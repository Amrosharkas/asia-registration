<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Coin extends Model
{
	protected $guarded = [];
    public function getClient(){
        
        return $this->belongsTo('App\User', 'user_id', 'id');

    }
}
