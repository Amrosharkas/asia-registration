<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Consultation extends Model
{
	protected $guarded = [];
    public function getClient(){
    	return $this->belongsTo('App\User', 'client_id', 'id');
    }
}
