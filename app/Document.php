<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Document extends Model
{
    public function getFile(){
    	return $this->belongsTo("App\File","file_id");
    }
}
