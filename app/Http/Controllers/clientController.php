<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Schedule;
use App\Course;
use App\Consultation;
use DB;
use App\Users_schedule;

use App\Http\Requests;
use Auth;

class clientController extends Controller
{
    public function __construct(){
		$this->logged_user = Auth::user();

	}
    public function index() {
        $clients = User::where('admin_show',1)->where('role_id',4)->get();
        $schedules = Schedule::where('admin_show',1)->get();
        $courses = Course::where('admin_show',1)->get();
        $data = [];
        $data['partialView'] = 'clients.list';
        $data['clients'] = $clients; 
        $data['schedules'] = $schedules; 
        $data['courses'] = $courses; 
        return view('clients.base', $data);
    }

    public function init(){
    	$client = new User();
    	$client->save();
    	return redirect('/admin/clients/'.$client->id.'/edit');
    }

    public function edit($id){
    	$client =  User::find($id);
    	$clients = User::where('admin_show',1)->where('role_id',4)->where('id','!=',$id)->get();
        $mothers = User::where('admin_show',1)->where('role_id',4)->where('gender','Female')->where('id','!=',$id)->get();
        $fathers = User::where('admin_show',1)->where('role_id',4)->where('gender','Male')->where('id','!=',$id)->get();
        $data = [];
        $data['clients'] = $clients;
        $data['client'] = $client;
        $data['mothers'] = $mothers; 
        $data['fathers'] = $fathers;
        $data['partialView'] = 'clients.form';
        return view('clients.base', $data);
    }

    public function save(Request $request){
        $data = $request->input();
        //Validations
        $check_email = User::where('email',$data['email'])->where('id','!=',$data['id'])->count();
        if($check_email !=0){
            $data = [];
            $data['status'] = 'error';
            
            $data['msg'] = "Email already exists";
            return response()->json(
                        $data
            );
        }
        $client = User::find($data['id']);
        $data['admin_show'] =1;
        $data['role_id'] =4;
        unset($data['password2']);
        if($data['date_of_birth'] != ""){
            $data['date_of_birth'] =date("Y-m-d",strtotime($data['date_of_birth']));
        }else{
            $data['date_of_birth'] = Null;    
        }
        if($data['joining_date'] != ""){
            $data['joining_date'] =date("Y-m-d",strtotime($data['joining_date']));
        }else{
            $data['joining_date'] = Null;    
        }
        

        if($client->update($data)){
            $data = [];
            $data['status'] = 'success';
            $data['page'] = '/admin/clients';
            $data['msg'] = "Saved Successfully";
            return response()->json(
                        $data
            );  
        }else{
        	$data = [];
            $data['status'] = 'error';
            $data['page'] = 'none';
            $data['msg'] = "There was an error";
            return response()->json(
                        $data
            );  

        }
    }

    public function profile($id){
        $client =  User::find($id);
        $children =  User::where('mother_id',$id)->orWhere('father_id',$id)->get();
        $consultations = Consultation::where('client_id',$id)->get();
        $clients = 
            Users_schedule::join('users', 'users.id', '=', 'users_schedules.user_id')
            ->join('schedules', 'users_schedules.schedule_id', '=', 'schedules.id')
            ->leftJoin('courses', 'schedules.course_id', '=', 'courses.id')
            ->leftJoin('places', 'schedules.place_id', '=', 'places.id')
            
            ->leftJoin('transactions', function($join)
                         {
                             $join->on( 'transactions.user_id', '=', 'users.id');
                             $join->on('transactions.schedule_id', '=', 'schedules.id');
                             
                         })

            ->where('users.id',$id)
            
            ->groupBy('transactions.schedule_id')
            ->select('users_schedules.*', 'courses.name', 'places.name as place','schedules.start_date', 'users.email','users.phone','users.points_balance' ,
                DB::raw('sum(amount) as payment')
                )
            ->get();
        $data = [];
        $data['clients'] = $clients;
        $data['client'] = $client;
        $data['consultations'] = $consultations;
        $data['children'] = $children;
        $data['partialView'] = 'clients.profile';
        return view('clients.base', $data);
    }
        

    
    public function delete($id){
       $delete = User::find($id)->delete();
    }
}
