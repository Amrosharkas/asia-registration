<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Coin;

use App\Http\Requests;
use Auth;

class coinController extends Controller
{
    public function __construct(){
		$this->logged_user = Auth::user();

	}
    public function index() {
        $clients = User::where('admin_show',1)->where('role_id',4)->get();
        $coins = Coin::where('admin_show',1)->get();
        
        $data = [];
        $data['partialView'] = 'coins.list';
        $data['clients'] = $clients; 
        $data['coins'] = $coins; 
        return view('coins.base', $data);
    }

    public function init(){
    	$coin = new Coin();
    	$coin->admin_show =0;
    	$coin->save();
    	return redirect('/admin/coins/'.$coin->id.'/edit');
    }

    public function edit($id){
    	$coin =  Coin::find($id);
    	$clients = User::where('admin_show',1)->where('role_id',4)->get();
        
        $data = [];
        
        $data['coin'] = $coin;
        $data['clients'] = $clients; 
        
        $data['partialView'] = 'coins.form';
        return view('coins.base', $data);
    }

    public function save(Request $request){
        $data = $request->input();
        //Validations
        
        if($data['user_id'] == ""){
            $data = [];
            $data['status'] = 'error';
            
            $data['msg'] = "Please select a client";
            return response()->json(
                        $data
            );
        }

        $coin = Coin::find($data['id']);
        $data['admin_show'] =1;
        
        $data['date'] =date("Y-m-d",strtotime($data['date']));

        
        

        if($coin->update($data)){
            $data = [];
            $data['status'] = 'success';
            $data['page'] = '/admin/coins';
            $data['msg'] = "Saved Successfully";
            return response()->json(
                        $data
            );  
        }else{
        	$data = [];
            $data['status'] = 'error';
            $data['page'] = 'none';
            $data['msg'] = "There was an error";
            return response()->json(
                        $data
            );  

        }
    }

    
        

    
    public function delete($id){
       $delete = Coin::find($id)->delete();
    }
}
