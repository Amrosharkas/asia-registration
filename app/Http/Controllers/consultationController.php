<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Helpers\Helpers;

use App\User;
use App\Consultation;
use App\Consultation_pricing;
use App\Transaction;
use App\Coin;

use App\Http\Requests;
use Auth;
use Illuminate\Support\Facades\Input;

class consultationController extends Controller
{
    public function __construct(){
		$this->logged_user = Auth::user();

	}
    public function index() {
        $clients = User::where('admin_show',1)->where('role_id',4)->get();
        $consultations = Consultation::get();
        
        $data = [];
        $data['partialView'] = 'consultations.list';
        $data['consultations'] = $consultations;
        $data['clients'] = $clients; 
         
        return view('consultations.base', $data);
    }

    public function init(Request $request){
    	$data = $request->input();

        //Validate a client is selected
        if($data['client_id'] == ""){
            return redirect('/admin/consultations?error=Please select a client first');
        }
    	$consultation = new Consultation();
    	$consultation->client_id = $data['client_id'];
    	$consultation->save();
    	
        return redirect('/admin/consultations/'.$consultation->id.'/edit');
    	
    }

    public function edit($id){
    	$consultation =  Consultation::find($id);
        $data = [];
        $data['consultation'] = $consultation;
        $data['partialView'] = 'consultations.form';
        return view('consultations.base', $data);
    }
    public function pricing(){
        $pricing =  Consultation_pricing::find(1);
        $data = [];
        $data['pricing'] = $pricing;
        $data['partialView'] = 'consultations.pricing';
        return view('consultations.base', $data);
    }

    public function save(Request $request){
        $data = $request->input();
        //Validations
        
        $consultation = Consultation::find($data['id']);
        

        $data['start_time'] = date("Y-m-d H:i:s",strtotime($data['date']." ".$data['time']));
        unset($data['date']);
        unset($data['time']);
        // if heba is updating the consultation
        if(Auth::user()->role_id == 1){
            if($data['next_contact']!=""){
                $data['next_contact'] =date("Y-m-d",strtotime($data['next_contact']));
            }

            // Set the price 
            $data['price'] = Helpers::getConsultationPrice($data['duration']);
            // $prices = Consultation_pricing::find(1);
            // $hours = explode(":", $data['duration'])[0];
            // $minutes = explode(":", $data['duration'])[1];

            // $total_minutes = $hours*60 + $minutes;
            // if($total_minutes>45){
            //     $data['price'] = $prices->pricing_1hour;
            // }else{
            //     $data['price'] = $prices->pricing_45;    
            // }
        }

        

        if($consultation->update($data)){
            $data = [];
            $data['status'] = 'success';
            $data['page'] = '/admin/consultations';
            $data['msg'] = "Saved Successfully";
            return response()->json(
                        $data
            );  
        }else{
        	$data = [];
            $data['status'] = 'error';
            $data['page'] = 'none';
            $data['msg'] = "There was an error";
            return response()->json(
                        $data
            );  

        }
    }

    public function save_audio(Request $request){
    	$data = $request->input();
        $consultation = Consultation::find($data['id']);
    	$dirPath = public_path('uploads');
    	Input::file('file')->move($dirPath, $data['id'].".wav");
        $consultation->audio_file = $data['id'].".wav";
        $consultation->save();

    }
    public function save_pricing(Request $request){
        $data = $request->input();
        //Validations
        
        $pricing = Consultation_pricing::find(1);
        

        

        if($pricing->update($data)){
            $data = [];
            $data['status'] = 'success';
            $data['page'] = '/admin/consultations';
            $data['msg'] = "Saved Successfully";
            return response()->json(
                        $data
            );  
        }else{
            $data = [];
            $data['status'] = 'error';
            $data['page'] = 'none';
            $data['msg'] = "There was an error";
            return response()->json(
                        $data
            );  

        }
    }
    public function add_reply(Request $request){
        $data = $request->input();
        $consultation= Consultation::find($data['id']);
        $consultation->call_reply = $data['reply'];
        $consultation->reply_date = date("Y-m-d H:i:s");
        $consultation->save();

        $data['status'] = 'success';
            $data['page'] = 'none';
            $data['msg'] = "Saved Successfully";
            return response()->json(
                        $data
        ); 
    }

    public function add_payment(Request $request){
        $data = $request->input();
        $pricing = Consultation_pricing::find(1);
        // Validate all fields are inserted 
        if(!is_numeric($data['amount'])){
            $data['status'] = 'error';
            $data['page'] = 'none';
            $data['msg'] = "Please insert right amount
            ";
            return response()->json(
                        $data
            );

        }
        if($data['date'] == ""){
            $data['status'] = 'error';
            $data['page'] = 'none';
            $data['msg'] = "Please insert a date
            ";
            return response()->json(
                        $data
            );

        }

        $consultation = Consultation::find($data['consultation_id']);
        $client = User::find($data['user_id']);
        
        $previous_payments = Transaction::where('consultation_id',$consultation->id)
            ->where('user_id',$data['user_id'])
            ->sum('amount');
        
        $price = $consultation->price;

        //Validate payment exceeded the course price
        $remaining_to_pay  = $price - $previous_payments;
        if($data['amount'] > $remaining_to_pay){
            $data['status'] = 'error';
            $data['page'] = 'none';
            $data['msg'] = "Sorry You are trying to pay more than the consultation price
            ";
            return response()->json(
                        $data
            );

        }
        // if user is paying with coins 
        if($data['payment_type'] == "Coins"){
            //check if he has sufficient balance
            if($client->points_balance < $data['amount']){
                $data['status'] = 'error';
                $data['page'] = 'none';
                $data['msg'] = "Sorry This client doesn't have enough coins balance
                ";
                return response()->json(
                            $data
                );
            }
            

        }

       
        
        // do the transaction
        $transaction = new Transaction();
        $transaction->user_id = $data['user_id'];
        $transaction->consultation_id = $data['consultation_id'];
        $transaction->amount = $data['amount'];
        $transaction->type = $data['payment_type'];
        $transaction->transaction_date = date("Y-m-d",strtotime($data['date']));
        $transaction->save();



        // update payment percentage
        $previous_payments = Transaction::where('consultation_id',$consultation->id)
            ->where('user_id',$data['user_id'])
            ->sum('amount');
        $payment_percentage = Helpers::getConsultationPaymentPercentage($consultation,$data['user_id']);
        $consultation->payment_percentage =   $payment_percentage;
        $consultation->payment =   $previous_payments;
        $consultation->save(); 

        // check if percentage is 100% 
        if($payment_percentage == 100 ){
            //check if this client is referred client
            if($client->refered_by != Null){
                //check if its his first payment
                $referral_deserves_reward = Helpers::doesReferralDeserveReward($client);
                if($referral_deserves_reward){
                    //add coins to his referral
                    $referral_client = User::find($client->refered_by);
                    Helpers::addCoinsToUser($pricing->referral_points_gained,"Retained",$referral_client,$consultation->id,$data['date']);
                }
            }

            // Add points to the client
            Helpers::addCoinsToUser($pricing->points_gained,"Retained",$client,$consultation->id,$data['date']);
        } 
        $data = [];
            $data['status'] = 'success';
            $data['page'] = '/admin/consultations/';
            $data['msg'] = "Saved Successfully";
            return response()->json(
                        $data
            ); 


    }
        

    
    public function delete($id){
       $delete = Consultation::find($id)->delete();
       $transactions = Transaction::where('consultation_id',$id)->get();
       if($transactions){
        foreach($transactions as $transaction){
            $transaction->delete(); 
        }
       }
       $coins = Coin::where('consultation_id',$id)->delete();
       if($coins){
            foreach($coins as $coin){
            $coin->delete(); 
        } 
       }
    }
    public function endTransactions($consultation_id){
        $update_transactions = Transaction::where('consultation_id',$consultation_id)->update(['editable'=>0]);
        $consultation = Consultation::find($consultation_id);
        $consultation->closed = 1;
        $consultation->save();
    }
    public function reOpenTransactions($consultation_id){
        $update_transactions = Transaction::where('consultation_id',$consultation_id)->update(['editable'=>1]);
        $consultation = Consultation::find($consultation_id);
        $consultation->closed = 0;
        $consultation->save();
    }
}
