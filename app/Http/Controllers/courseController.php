<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Course;
use Auth;

class courseController extends Controller
{
    public function __construct(){
		$this->logged_user = Auth::user();

	}
    public function index() {
        $courses = Course::where('admin_show',1)->get();
        $data = [];
        $data['partialView'] = 'courses.list';
        $data['courses'] = $courses; 
        return view('courses.base', $data);
    }

    public function init(){
    	$course = new Course();
    	$course->save();
    	return redirect('/admin/courses/'.$course->id.'/edit');
    }

    public function edit($id){
    	$course =  Course::find($id);
        $data = [];
        $data['course'] = $course;
        $data['partialView'] = 'courses.form';
        return view('courses.base', $data);
    }

    public function save(Request $request){
        $data = $request->input();
        
        $course = Course::find($data['id']);
        $data['admin_show'] =1;
        
        

        

        if($course->update($data)){
            $data = [];
            $data['status'] = 'success';
            $data['page'] = '/admin/courses';
            $data['msg'] = "Saved Successfully";
            return response()->json(
                        $data
            );  
        }else{
        	$data = [];
            $data['status'] = 'error';
            $data['page'] = 'none';
            $data['msg'] = "There was an error";
            return response()->json(
                        $data
            );  

        }
    }
        

    
    public function delete($id){
       $delete = Course::find($id)->delete();
    }
}
