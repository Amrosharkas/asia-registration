<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Schedule;
use App\Course;
use App\Consultation;
use DB;
use App\Users_schedule;

use App\Http\Requests;
use Auth;

class employeeController extends Controller
{
    public function __construct(){
		$this->logged_user = Auth::user();

	}
    public function index() {
        $employees = User::where('admin_show',1)->whereIn('role_id',array(1,2))->get();
        $data = [];
        $data['partialView'] = 'employees.list';
        $data['employees'] = $employees; 
        return view('employees.base', $data);
    }

    public function init(){
    	$employee = new User();
    	$employee->save();
    	return redirect('/admin/employees/'.$employee->id.'/edit');
    }

    public function edit($id){
    	$employee =  User::find($id);
        $data = [];
        $data['employee'] = $employee;
        $data['partialView'] = 'employees.form';
        return view('employees.base', $data);
    }

    public function save(Request $request){
        $data = $request->input();
        //Validations
        $check_email = User::where('email',$data['email'])->where('id','!=',$data['id'])->count();
        if($check_email !=0){
            $data = [];
            $data['status'] = 'error';
            
            $data['msg'] = "Email already exists";
            return response()->json(
                        $data
            );
        }
        $employee = User::find($data['id']);
        $data['admin_show'] =1;
        
        unset($data['password2']);
        if($data['date_of_birth'] != ""){
            $data['date_of_birth'] =date("Y-m-d",strtotime($data['date_of_birth']));
        }else{
            $data['date_of_birth'] = Null;    
        }
        if($data['joining_date'] != ""){
            $data['joining_date'] =date("Y-m-d",strtotime($data['joining_date']));
        }else{
            $data['joining_date'] = Null;    
        }
    	$data['password'] = bcrypt($data['password']);

        

        if($employee->update($data)){
            $data = [];
            $data['status'] = 'success';
            $data['page'] = '/admin/employees';
            $data['msg'] = "Saved Successfully";
            return response()->json(
                        $data
            );  
        }else{
        	$data = [];
            $data['status'] = 'error';
            $data['page'] = 'none';
            $data['msg'] = "There was an error";
            return response()->json(
                        $data
            );  

        }
    }

        

    
    public function delete($id){
       $delete = User::find($id)->delete();
    }
}
