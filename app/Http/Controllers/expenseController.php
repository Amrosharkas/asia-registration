<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Expense;
use App\Schedule;
use App\User;
use Auth;

class expenseController extends Controller
{
    public function __construct(){
		$this->logged_user = Auth::user();

	}
    public function index() {
        // super user can see all expenses
        if($this->logged_user->role_id ==1){
            $expenses = Expense::where('admin_show',1)->get();
        }
        // other employees can only see public expenses
        else{
            $expenses = Expense::where('admin_show',1)->where('view_type','public')->get();

        }
        $data = [];
        $data['partialView'] = 'expenses.list';
        $data['expenses'] = $expenses; 
        return view('expenses.base', $data);
    }

    public function init(){
    	$expense = new Expense();
    	$expense->save();
    	return redirect('/admin/expenses/'.$expense->id.'/edit');
    }

    public function edit($id){
    	$expense =  Expense::find($id);
    	$schedules = Schedule::where('admin_show',1)->get();
    	$trainers = User::where('admin_show',1)->where('role_id',3)->get();
    	$types = Expense::select('type')
    	->groupBy('type')
    	->whereNotIn('type',array('Trainers & Consultants Payment','Training Expenses','Others'))
    	->get();
        $data = [];
        $data['expense'] = $expense;
        $data['schedules'] = $schedules;
        $data['trainers'] = $trainers;
        $data['types'] = $types;
        $data['partialView'] = 'expenses.form';
        return view('expenses.base', $data);
    }

    public function save(Request $request){
        $data = $request->input();
        if($data['type'] == "" && $data['new_type'] == ""){
        	$data = [];
            $data['status'] = 'error';
            $data['page'] = 'none';
            $data['msg'] = "Please select a type";
            return response()->json(
                        $data
            );

        }
        if($data['new_type'] !=""){
        	$data['type'] = $data['new_type'];
        }
        unset($data['new_type']);
        
        $expense = Expense::find($data['id']);
        $data['admin_show'] =1;
        if($data['expense_date']!=""){
        	$data['expense_date'] = date("Y-m-d",strtotime($data['expense_date']));
        }
        
        

        

        if($expense->update($data)){
            $data = [];
            $data['status'] = 'success';
            $data['page'] = '/admin/expenses';
            $data['msg'] = "Saved Successfully";
            return response()->json(
                        $data
            );  
        }else{
        	$data = [];
            $data['status'] = 'error';
            $data['page'] = 'none';
            $data['msg'] = "There was an error";
            return response()->json(
                        $data
            );  

        }
    }
        

    
    public function delete($id){
       $delete = Expense::find($id)->delete();
    }
}
