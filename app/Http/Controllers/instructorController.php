<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Document;
use App\File;

use App\Http\Requests;
use Auth;
use DB;

class instructorController extends Controller
{
    public function __construct(){
		$this->logged_user = Auth::user();

	}
    public function index() {
        $instructors = User::where('admin_show',1)->where('role_id',3)->get();
        $now = date("Y-m-d");
        $expiring_documents = Document::
        where("expire_date","!=","0000-00-00")->
        where(DB::raw("DATE_SUB(expire_date, INTERVAL 30 DAY)"),"<=",$now)
        ->join('users', 'users.id', '=', 'documents.user_id')
        ->join('files', 'files.id', '=', 'documents.file_id')
        ->get()
        ;
        
        $data = [];
        $data['partialView'] = 'instructors.list';
        $data['instructors'] = $instructors; 
        $data['expiring_documents'] = $expiring_documents; 
        return view('instructors.base', $data);
    }

    public function init(){
    	$instructor = new User();
    	$instructor->save();
    	return redirect('/admin/instructors/'.$instructor->id.'/edit');
    }

    public function init_document(){
        $document = new Document();
        $document->save();
        return $document->id;
    }

    public function edit($id){
    	$instructor =  User::find($id);
        $specs = 
        User::select('speciality')->where('admin_show',1)->where('role_id',3)
        
        ->groupBy('speciality')
        ->whereNotNull('speciality')
        
        ->get();
        $documents = Document::where("user_id",$id)->whereNotNull('file_id')->get();
        $data = [];
        $data['instructor'] = $instructor;
        $data['specs'] = $specs;
        $data['documents'] = $documents;

        $data['partialView'] = 'instructors.form';
        return view('instructors.base', $data);
    }

    public function save(Request $request){
        $data = $request->input();
        if(isset($data['document_name'])){
            $file_count = count($data['document_name']);
            for($i=0;$i<$file_count;$i++){
                
                
                // name is inserted
                if ($data['document_name'][$i] == "") {
                    $data = [];
                    $data['status'] = 'error';
                    $data['page'] = 'none';
                    $data['msg'] = "Please Insert a valid document name";
                    return response()->json(
                                $data
                    ); 
                }
                
                $file =  Document::find($data['document_id'][$i]);
                
                if ($file->file_id == "") {
                    $data = [];
                    $data['status'] = 'error';
                    $data['page'] = 'none';
                    $data['msg'] = "Please upload a file first";
                    return response()->json(
                                $data
                    ); 
                }
                  
                
                
                
                
            }
            //$delete_files = Document::where('user_id',$data['id'])->delete();
            // Files Insertion
            //dd($data['document_name']);
            for($i=0;$i<$file_count;$i++){
                if($data['expire_date'][$i] != ""){
                    $expire_date = date("Y-m-d",strtotime($data['expire_date'][$i]));
                }else{
                    $expire_date="";
                }
                $document =  Document::find($data['document_id'][$i]);
                $document->document_name = $data['document_name'][$i];
                
                $document->expire_date = $expire_date;
                
                $document->save();
            }
        }
        //Validations
        $check_email = User::where('email',$data['email'])->where('id','!=',$data['id'])->count();
        if($check_email !=0){
            $data = [];
            $data['status'] = 'error';
            
            $data['msg'] = "Email already exists";
            return response()->json(
                        $data
            );
        }
        if($data['new_spec'] !=""){
            $data['speciality'] = $data['new_spec'];
        }
        unset($data['new_spec']);
        $instructor = User::find($data['id']);
        $data['admin_show'] =1;
        $data['role_id'] =3;
        unset($data['password2']);
        $data['date_of_birth'] =date("Y-m-d",strtotime($data['date_of_birth']));
        $data['joining_date'] =date("Y-m-d",strtotime($data['joining_date']));
        if(isset($data['document_name'])){
            unset($data['document_name']);
            unset($data['expire_date']);
            unset($data['document_id']);
            unset($data['qqfile']);
        }
        

        if($instructor->update($data)){
            $data = [];
            $data['status'] = 'success';
            $data['page'] = '/admin/instructors';
            $data['msg'] = "Saved Successfully";
            return response()->json(
                        $data
            );  
        }else{
        	$data = [];
            $data['status'] = 'error';
            $data['page'] = 'none';
            $data['msg'] = "There was an error";
            return response()->json(
                        $data
            );  

        }
    }
        

    
    public function delete($id){
       $delete = User::find($id)->delete();
    }

    public function deleteDocument($id){
       $document = Document::find($id);
       if(isset($document->getFile->hash)){
           $filename = public_path("uploads/".$document->getFile->hash."/".$document->getFile->file);
           
           if (file_exists($filename)) {
            unlink($filename);
            $delete_file = File::find($document->getFile->id)->delete();
           }
        }
       
       $document->delete();
    }
}
