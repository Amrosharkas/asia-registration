<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Place;
use Auth;

class placeController extends Controller
{
    public function __construct(){
		$this->logged_user = Auth::user();

	}
    public function index() {
        $places = Place::where('admin_show',1)->get();
        $data = [];
        $data['partialView'] = 'places.list';
        $data['places'] = $places; 
        return view('places.base', $data);
    }

    public function init(){
    	$place = new Place();
    	$place->save();
    	return redirect('/admin/places/'.$place->id.'/edit');
    }

    public function edit($id){
    	$place =  Place::find($id);
        $data = [];
        $data['place'] = $place;
        $data['partialView'] = 'places.form';
        return view('places.base', $data);
    }

    public function save(Request $request){
        $data = $request->input();
        
        $place = Place::find($data['id']);
        $data['admin_show'] =1;
        
        

        

        if($place->update($data)){
            $data = [];
            $data['status'] = 'success';
            $data['page'] = '/admin/places';
            $data['msg'] = "Saved Successfully";
            return response()->json(
                        $data
            );  
        }else{
        	$data = [];
            $data['status'] = 'error';
            $data['page'] = 'none';
            $data['msg'] = "There was an error";
            return response()->json(
                        $data
            );  

        }
    }
        

    
    public function delete($id){
       $delete = Place::find($id)->delete();
    }}
