<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Helpers\Helpers;

use App\Http\Requests;
use App\Schedule;
use App\Schedule_day;
use App\Course;
use App\User;
use App\Users_schedule;
use App\Users_course;
use App\Transaction;
use App\Place;
use DB;

use Auth;

class reportController extends Controller
{
    public function __construct(){
		$this->logged_user = Auth::user();

	}
    public function index() {
        $date1 = "1900-1-1";
        $date2 = "2900-1-1";
        if(isset($_GET['date1']) && $_GET['date1']!=""){
            $date1 = date("Y-m-d",strtotime($_GET['date1']));
        }
        if(isset($_GET['date2']) && $_GET['date2']!=""){
            $date2 = date("Y-m-d",strtotime($_GET['date2']));
        }
        

       $income_events= DB::select("
        	select MONTH(transaction_date) as month, Year(transaction_date) as year,sum(amount) as amount
        	From transactions
        	Where course_id is not null 
            And type = 'Cash'
            AND transaction_date between '".$date1."' and '".$date2."'
        	Group By MONTH(transaction_date)
        	, Year(transaction_date)
            Order By transaction_date
        	");
        $income_consultations = 
        DB::select("
        	select MONTH(transaction_date) as month,Year(transaction_date) as year,sum(amount) as amount
        	From transactions

        	Where consultation_id is not null 
            And type = 'Cash'
            AND transaction_date between '".$date1."' and '".$date2."'
        	Group By MONTH(transaction_date)
        	, Year(transaction_date)
            Order By transaction_date
        	");
        $expenses_query = 
        DB::select("
            select MONTH(expense_date) as month,Year(expense_date) as year,sum(amount) as amount,type
            From expenses

            Where admin_show = 1
            AND expense_date between '".$date1."' and '".$date2."'
            Group By MONTH(expense_date)
            , Year(expense_date)
            , type
            Order By expense_date
            ");
        $expenses_breakdown = 
        DB::select("
            select sum(amount) as amount,type
            From expenses

            Where admin_show = 1
            AND expense_date between '".$date1."' and '".$date2."'
            Group By  type
            
            ");
        $income_breakdown = 
        DB::select("
            select sum(amount)  'amount', 'Training' as type
            From transactions

            Where  transaction_date between '".$date1."' and '".$date2."'
            and course_id is not null

            Union

            select sum(amount)  'amount', 'Consultations' 
            From transactions

            Where  transaction_date between '".$date1."' and '".$date2."'
            and consultation_id is not null
            
            ");
        
        $get_months = 
        DB::select("
            select distinct MONTH(expense_date)  'month',Year(expense_date)  'year',
            expense_date 'order_date'
            From expenses

            Where admin_show = 1
            AND expense_date between '".$date1."' and '".$date2."'
            
            

            UNION

            select distinct MONTH(transaction_date)  ,Year(transaction_date) ,transaction_date 
            From transactions

            
            Where transaction_date between '".$date1."' and '".$date2."'
            And type = 'Cash'
            
            Order By order_date


            ");
        
        

        $dates = array();
        $consultations = array();
        $events = array();
        $expenses = array();
        $types = array();
        foreach ($expenses_query as $expense) {
            
            if (!in_array($expense->type, $types)) {
                array_push($types,$expense->type);
            }
            $expenses[$expense->month][$expense->year][$expense->type] = $expense->amount;
        }
        foreach ($income_events as $event) {
        	
        	$events[$event->month][$event->year] = $event->amount;
        }
        foreach ($income_consultations as $consultation) {
        	
        	$consultations[$consultation->month][$consultation->year] = $consultation->amount;
        }

        foreach ($get_months as $month) {
            if (!in_array($month->month."-".$month->year, $dates)) {
                array_push($dates,$month->month."-".$month->year);
            }
            
        }

        $data = [];
        $data['partialView'] = 'reports.index';
        $data['dates'] = $dates; 
        $data['events'] = $events;
        $data['expenses'] = $expenses; 
        $data['types'] = $types;  
        $data['consultations'] = $consultations;
        $data['expenses_breakdown'] = $expenses_breakdown; 
        $data['income_breakdown'] = $income_breakdown; 
        return view('reports.base', $data);
    }
}
