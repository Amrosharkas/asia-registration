<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Helpers\Helpers;

use App\Http\Requests;
use App\Schedule;
use App\Schedule_day;
use App\Course;
use App\User;
use App\Users_schedule;
use App\Users_course;
use App\Transaction;
use App\Place;
use DB;

use Auth;

class scheduleController extends Controller
{
    public function __construct(){
		$this->logged_user = Auth::user();

	}
    public function index() {
        $schedules = Schedule::where('admin_show',1)->get();
        $data = [];
        $data['partialView'] = 'schedules.list';
        $data['schedules'] = $schedules; 
        return view('schedules.base', $data);
    }

    public function init(){
    	$schedule = new Schedule();
    	$schedule->save();
    	return redirect('/admin/schedules/'.$schedule->id.'/edit');
    }

    public function edit($id){
    	$schedule =  Schedule::find($id);
        $schedule_days =  Schedule_day::where('schedule_id',$id)->orderBy('from')->get();
    	$courses = Course::where('admin_show',1)->get();
        $places = Place::where('admin_show',1)->get();
    	$instructors = User::where('role_id',3)->get();
        $data = [];
        $data['schedule'] = $schedule;
        $data['schedule_days'] = $schedule_days;
        $data['courses'] = $courses;
        $data['instructors'] = $instructors;
        $data['places'] = $places;
        $data['partialView'] = 'schedules.form';
        return view('schedules.base', $data);
    }

    public function save(Request $request){
        $data = $request->input();
        if($data['place_id'] == ""){
            $data = [];
            $data['status'] = 'error';
            $data['page'] = 'none';
            $data['msg'] = "Please select a place";
            return response()->json(
                        $data
            );
        }
        
        $froms = $data['from'];
        $tos = $data['to'];
        $days = $data['day'];

        unset($data['day']);
        unset($data['from']);
        unset($data['to']);
        
        $day_count = count($froms);

        //instructors
        if(isset($data['instructor_id'])){
            $data['instructor_id'] = implode (",", $data['instructor_id']);
        }else{
            $data['instructor_id']= "";
        }
        if($data['instructor_id'] == ""){
            $data = [];
            $data['status'] = 'error';
            $data['page'] = 'none';
            $data['msg'] = "Please select at least one instructor";
            return response()->json(
                        $data
            );
        }

        // Days Validation 

        for($i=0;$i<$day_count;$i++){
            $from = date("Y-m-d H:i:s",strtotime($days[$i]." ".$froms[$i]));
            $to = date("Y-m-d H:i:s",strtotime($days[$i]." ".$tos[$i]));
            // date is inserted
            if ($days[$i] == "") {
                $data = [];
                $data['status'] = 'error';
                $data['page'] = 'none';
                $data['msg'] = "Please Insert a valid date";
                return response()->json(
                            $data
                ); 
            }
            // start time and end time
            if(strtotime($days[$i]." ".$froms[$i]) > strtotime($days[$i]." ".$tos[$i]) ){
                $data = [];
                $data['status'] = 'error';
                $data['page'] = 'none';
                $data['msg'] = "The start time is greater than the end time";
                return response()->json(
                            $data
                ); 

            } 
            // check intersections 
            
            /*$intersecting_days = 
            Schedule_day::
            where("to",'>=',$from)->where("from",'<=',$to)
            ->where('schedule_id',$data['id'])
            
            ->count();
            
            if($intersecting_days != 0){
                $data = [];
                $data['status'] = 'error';
                $data['page'] = 'none';
                $data['msg'] = "Some days are intersecting with each other";
                return response()->json(
                            $data
                ); 

            } */
              
            
            
            
            
        }
        $delete_days = Schedule_day::where('schedule_id',$data['id'])->delete();
        // Days Insertion
        for($i=0;$i<$day_count;$i++){
            $day = new Schedule_day();
            $day->schedule_id = $data['id'];
            
            $day->from = date("Y-m-d H:i:s",strtotime($days[$i]." ".$froms[$i]));
            $day->to = date("Y-m-d H:i:s",strtotime($days[$i]." ".$tos[$i]));
            $day->save();
        }

        
        
        $schedule = Schedule::find($data['id']);
        $data['admin_show'] =1;
        $data['start_date'] = date("Y-m-d",strtotime($data['start_date']));




        

        if($schedule->update($data)){
            $data = [];
            $data['status'] = 'success';
            $data['page'] = '/admin/schedules';
            $data['msg'] = "Saved Successfully";
            return response()->json(
                        $data
            );  
        }else{
        	$data = [];
            $data['status'] = 'error';
            $data['page'] = 'none';
            $data['msg'] = "There was an error";
            return response()->json(
                        $data
            );  

        }
    }
    public function recommend(Request $request){
        $data = $request->input();
        // Event Recommendation
        if(isset($data['event'])){
        // validations
        if(!isset($data['clients']) || $data['schedule_id'] == ""){
            $data = [];
            $data['status'] = 'error';
            $data['page'] = 'none';
            $data['msg'] = "You must choose a client and event first";
            return response()->json(
                        $data
            );  

        }

        $accepted_users = 0;
        $rejected_users = 0;
        foreach ($data['clients'] as $client_id) {
            $schedule = Schedule::find($data['schedule_id']);
            $check_schedule_existing_count = 
            Users_schedule::where('user_id',$client_id)->
                            where('schedule_id',$data['schedule_id'])->
                            count();
            if($check_schedule_existing_count == 0){
                $user_schedule = new Users_schedule();
                $user_schedule->user_id = $client_id;
                $user_schedule->schedule_id =$data['schedule_id'];
                $user_schedule->course_id = $schedule->getCourse->id;
                $user_schedule->save();
                $accepted_users++;
            }else{
                $rejected_users++;

            }
        }
        }
        // Course recommendations
        else{
            // validations
        if(!isset($data['clients']) || $data['course_id'] == ""){
            $data = [];
            $data['status'] = 'error';
            $data['page'] = 'none';
            $data['msg'] = "You must choose a client and course first";
            return response()->json(
                        $data
            );  

        }

        $accepted_users = 0;
        $rejected_users = 0;
        foreach ($data['clients'] as $client_id) {
            
            $check_course_existing_count = 
            Users_course::where('user_id',$client_id)->
                            where('course_id',$data['course_id'])->
                            count();
            if($check_course_existing_count == 0){
                $user_course = new Users_course();
                $user_course->user_id = $client_id;
                $user_course->course_id =$data['course_id'];
                $user_course->save();
                $accepted_users++;
            }else{
                $rejected_users++;

            }
        }


        }


        // 
        $data = [];
            $data['status'] = 'success';
            $data['page'] = 'none';
            $data['msg'] = "
            
            Added clients : ".$accepted_users."</br>
            Already Exists : ".$rejected_users."
            ";
            return response()->json(
                        $data
            );
    }
    
    public function manage($schedule_id){
        $interest_status="";
        if(isset($_GET['interest_status'])){
            $interest_status = $_GET['interest_status'];    
        }
        $payment_status="";
        if(isset($_GET['payment_status'])){
            $payment_status = $_GET['payment_status'];    
        }
        $clients = 
            Users_schedule::join('users', 'users.id', '=', 'users_schedules.user_id')
            ->join('schedules', 'users_schedules.schedule_id', '=', 'schedules.id')
            
            ->leftJoin('transactions', function($join)
                         {
                             $join->on( 'transactions.user_id', '=', 'users.id');
                             $join->on('transactions.schedule_id', '=', 'schedules.id');
                             
                         })

            ->where('schedules.id',$schedule_id)
            ->where(function ($query) use ($interest_status,$payment_status) {
                if($interest_status!=""){
                    $query->where('users_schedules.interest_status',$interest_status);    
                }
                if($payment_status=="No"){
                    $query->whereNull('users_schedules.payment_percentage');    
                }
                if($payment_status=="Half"){
                    $query
                    ->where('users_schedules.payment_percentage','>',0)
                    ->where('users_schedules.payment_percentage','<',100);    
                }
                if($payment_status=="Full"){
                    $query
                    ->where('users_schedules.payment_percentage',100);    
                }
                
            })
            ->groupBy('users.id')
            ->select('users_schedules.*', 'users.name', 'users.email','users.phone','users.points_balance' ,
                DB::raw('sum(amount) as payment')
                ,DB::raw('users_schedules.id as user_schedule_id'
                    )
                )
            ->get();
            //dd($clients);

        $schedule = Schedule::find($schedule_id);
        $data = [];
        $data['partialView'] = 'schedules.manage';
        $data['clients'] = $clients;
        $data['schedule'] = $schedule; 
        $data['interest_status'] = $interest_status; 
        $data['payment_status'] = $payment_status; 
        return view('schedules.base', $data);

    }

    public function update_interested_status(Request $request){
        $data = $request->input();
        $user_schedule = Users_schedule::find($data['id']);
        $user_schedule->interest_status = $data['status'];
        $user_schedule->save();
    }

    public function add_payment(Request $request){
        $data = $request->input();
        // Validate all fields are inserted 
        if(!is_numeric($data['amount'])){
            $data['status'] = 'error';
            $data['page'] = 'none';
            $data['msg'] = "Please insert right amount
            ";
            return response()->json(
                        $data
            );

        }
        if($data['date'] == ""){
            $data['status'] = 'error';
            $data['page'] = 'none';
            $data['msg'] = "Please insert a date
            ";
            return response()->json(
                        $data
            );

        }

        $schedule = Schedule::find($data['schedule_id']);
        $client = User::find($data['user_id']);
        $user_schedule = Users_schedule::where('schedule_id',$data['schedule_id'])->where('user_id',$data['user_id'])->first();
        $previous_payments = Transaction::where('schedule_id',$schedule->id)
            ->where('user_id',$data['user_id'])
            ->sum('amount');
        $last_discount_date = Helpers::getScheduleLastDiscountDate($schedule);
        
        // if it's the first time the client will pay
        if(!$previous_payments ){
            // if he is paying before the discount cuttoff time set the price to the discounted price
            if(strtotime($data['date']) <= strtotime($last_discount_date)){
                $price = $schedule->disc_price_cash;
            }
            // if he is paying after the discount cuttoff time set the price to the normal price
            else{
                $price = $schedule->price_cash;
            }
            $user_schedule->price = $price;
            $user_schedule->save();
        }else{
            $price = $user_schedule->price;
        }

        //Validate payment exceeded the course price
        $remaining_to_pay  = $price - $previous_payments;
        if($data['amount'] > $remaining_to_pay){
            $data['status'] = 'error';
            $data['page'] = 'none';
            $data['msg'] = "Sorry You are trying to pay more than the event price
            ";
            return response()->json(
                        $data
            );

        }
        // if user is paying wiht coins 
        if($data['payment_type'] == "Coins"){
            //check if he has sufficient balance
            if($client->points_balance < $data['amount']){
                $data['status'] = 'error';
                $data['page'] = 'none';
                $data['msg'] = "Sorry This client doesn't have enough coins balance
                ";
                return response()->json(
                            $data
                );
            }
            

        }

       
        
        // do the transaction
        $transaction = new Transaction();
        $transaction->user_id = $data['user_id'];
        $transaction->schedule_id = $data['schedule_id'];
        $transaction->course_id = $schedule->getCourse->id;
        $transaction->amount = $data['amount'];
        $transaction->type = $data['payment_type'];
        $transaction->transaction_date = date("Y-m-d",strtotime($data['date']));
        $transaction->save();
        // update payment percentage
        $payment_percentage = Helpers::getPaymentPercentage($schedule,$data['user_id']);
        $user_schedule->payment_percentage =   $payment_percentage;
        $user_schedule->save(); 

        // check if percentage is 100% 
        if($payment_percentage == 100 ){
            //check if this client is referred client
            if($client->refered_by != Null){
                //check if its his first payment
                $referral_deserves_reward = Helpers::doesReferralDeserveReward($client);
                if($referral_deserves_reward){
                    //add coins to his referral
                    $referral_client = User::find($client->refered_by);
                    Helpers::addCoinsToUser($schedule->referral_points_gained,"Retained",$referral_client,$schedule->id,$data['date']);
                }
            }

            // Add points to the client
            Helpers::addCoinsToUser($schedule->points_gained,"Retained",$client,$schedule->id,$data['date']);
        } 
        $data = [];
            $data['status'] = 'success';
            $data['page'] = '/admin/schedules/'.$schedule->id.'/manage';
            $data['msg'] = "Saved Successfully";
            return response()->json(
                        $data
            ); 


    }
    public function payment_history($schedule_id,$user_id){
        $transactions = Transaction::where('user_id',$user_id)->where('schedule_id',$schedule_id)->get();
        $data = [];
        $data['partialView'] = 'schedules.payment_history';
        $data['transactions'] = $transactions;
          
        return view('schedules.base', $data);
    }

    public function cancel($user_schedule_id){
        $user_schedule = Users_schedule::find($user_schedule_id);
        
        $schedule = Schedule::find($user_schedule->schedule_id);
        $cuttoff_dates = Helpers::getScheduleCancellationCuttoffDates($schedule);
        $now = date("Y-m-d");
        $client = User::find($user_schedule->user_id);
        $previous_payments = Transaction::where('schedule_id',$schedule->id)
            ->where('user_id',$user_schedule->user_id)
            ->sum('amount');
        if($previous_payments){
            $previous_payments = $previous_payments;
        }else{
            $previous_payments = 0;
        }
        // return 100%
        if(strtotime($now) <= strtotime($cuttoff_dates['100'])){

            // Add points to the client
            Helpers::addCoinsToUser($previous_payments,"Retained",$client,$schedule->id,$now);

        }
        // return 50%
        if(strtotime($now) > strtotime($cuttoff_dates['100']) && strtotime($now) <= strtotime($cuttoff_dates['50'])){

            // Add points to the client
            Helpers::addCoinsToUser(($previous_payments/2),"Retained",$client,$schedule->id,$now);

        }

        // Delete all previous payments

        Transaction::where('schedule_id',$schedule->id)
            ->where('user_id',$user_schedule->user_id)
            ->delete();
        // Delete this user from this schedule
        $user_schedule->delete();

        return redirect('/admin/schedules/'.$schedule->id.'/manage');





    }

    
    public function delete($id){
       $delete = Schedule::find($id)->delete();
       $schedule_days = Schedule_day::where('schedule_id',$id)->delete();
       $user_schedule = Users_schedule::where('schedule_id',$id)->delete();
       $transactions = Transaction::where('schedule_id',$id)->delete();
       $coins = Coin::where('schedule_id',$id)->delete();
    }
    public function deleteDay($id){
       $delete = Schedule_day::find($id)->delete();
    }

    public function endTransactions($schedule_id){
        $update_transactions = Transaction::where('schedule_id',$schedule_id)->update(['editable'=>0]);
        $schedule = Schedule::find($schedule_id);
        $schedule->closed = 1;
        $schedule->save();
    }

    public function reOpenTransactions($schedule_id){
        $update_transactions = Transaction::where('schedule_id',$schedule_id)->update(['editable'=>1]);
        $schedule = Schedule::find($schedule_id);
        $schedule->closed = 0;
        $schedule->save();
    }
    
}
