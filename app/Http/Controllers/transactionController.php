<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Helpers\Helpers;

use App\Http\Requests;
use App\Schedule;
use App\Users_schedule;
use App\Transaction;
use App\Consultation;



class transactionController extends Controller
{
    public function payment_history($stuff_id,$stuff_identifier,$user_id){

        $transactions = 
        Transaction::
        where('user_id',$user_id)
        ->where($stuff_identifier,$stuff_id)
        ->get();
        $data = [];
        $data['partialView'] = 'transactions.payment_history';
        $data['transactions'] = $transactions;
          
        return view('transactions.base', $data);
    }

    public function delete($id){
	   $transaction = Transaction::find($id);
       $delete = Transaction::find($id)->delete();

       // updating payment percentages
       // if the transaction was for a schedule
       if($transaction->schedule_id != ""){
	       $schedule = Schedule::find($transaction->schedule_id);
	       $user_schedule = Users_schedule::where('user_id',$transaction->user_id)->where('schedule_id',$transaction->schedule_id)->first();
	       $payment_percentage = Helpers::getPaymentPercentage($schedule,$transaction->user_id);
	       $user_schedule->payment_percentage =   $payment_percentage;
	       $user_schedule->save(); 
    	}
    	// if the transaction was for a consultation
    	elseif($transaction->consultation_id != ""){
    		$consultation = Consultation::find($transaction->consultation_id);
	        $previous_payments = Transaction::where('consultation_id',$transaction->consultation_id)
	            ->where('user_id',$transaction->user_id)
	            ->sum('amount');
	        $payment_percentage = Helpers::getConsultationPaymentPercentage($consultation,$transaction->user_id);
	        $consultation->payment_percentage =   $payment_percentage;
	        $consultation->payment =   $previous_payments;
	        $consultation->save(); 
    	}
    }
}
