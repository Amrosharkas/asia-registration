<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Auth;

class userController extends Controller
{
    public function index(){
    	if(Auth::user()){
	    	if (Auth::user()->user_type == "Admin"){
	    		return redirect('/admin/companies');
	    	}elseif(Auth::user()->role_id == 1){
	    		return redirect('/admin/clients');
	    	}elseif(Auth::user()->role_id == 2){
	    		return redirect('/admin/clients');
	    	}elseif(Auth::user()->role_id == 3){
	    		$partialView = "employees.list";
	    		return view('employees.base',compact('partialView'));
	    	}elseif(Auth::user()->role_id == 4){
	    		return redirect('/admin/requests/Permissions');
	    	}
	    }else{
	    	return redirect('/login');
	    }
    }

    
}
