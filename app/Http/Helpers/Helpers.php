<?php

namespace App\Http\Helpers;


use App\User;
use App\Transaction;
use App\Users_schedule;
use App\Coin;
use App\Consultation;
use App\Consultation_pricing;
use Auth;

use DB;
use Mail;




 class Helpers
{	
    
    public static function getSchedulePrice($schedule,$payment_type,$date){
        $discount=0;
        $discount_last_day = Helpers::getScheduleLastDiscountDate($schedule);
        // check if there is discount 
        if(strtotime($date) > strtotime($discount_last_day)){
            $discount = 1;
        }

        // Cash payment
        if($payment_type == "Cash"){
            if($discount == 0){
                $price = $schedule->price_cash;
            }else{
                $price = $schedule->disc_price_cash;

            }
        }
        // Coins payment
        else{
            if($discount == 0){
                $price = $schedule->price_coins;
            }else{
                $price = $schedule->disc_price_coins;

            }

        }
        return $price;
    	
    }

    public static function getScheduleLastDiscountDate($schedule){
        $discount_last_day = date("Y-m-d",strtotime($schedule->start_date." -".$schedule->disc_cuttoff_time." days"));

        return $discount_last_day;

    }
    public static function getReaminingPayment($schedule,$user_id){
        $previous_payments = Transaction::where('schedule_id',$schedule->id)
            ->where('user_id',$user_id)
            ->sum('amount');
        $user_schedule = Users_schedule::where('schedule_id',$schedule->id)
        ->where('user_id',$data['user_id'])->first();

        $remaining_payment = $user_schedule->price - $previous_payments;

        return $remaining_payment;

    }
    public static function getPaymentPercentage($schedule,$user_id){
        $previous_payments = Transaction::where('schedule_id',$schedule->id)
            ->where('user_id',$user_id)
            ->sum('amount');
        $user_schedule = Users_schedule::where('schedule_id',$schedule->id)->where('user_id',$user_id)->first();
        $payment_percentage = $previous_payments/$user_schedule->price*100;
        return $payment_percentage;



    }
    public static function getConsultationPaymentPercentage($consultation,$user_id){
        $previous_payments = Transaction::where('consultation_id',$consultation->id)
            ->where('user_id',$user_id)
            ->sum('amount');
        
        $payment_percentage = $previous_payments/$consultation->price*100;
        return $payment_percentage;



    }
    public static function addCoinsToUser($amount,$type,$user,$schedule_id="",$date,$expire_after=30){
        $coins = new Coin();
        $coins->user_id = $user->id;
        $coins->schedule_id=$schedule_id;
        $coins->date = $date;
        $coins->amount= $amount;
        $coins->type= $type;
        if($type == "Expirable"){
            $coins->expire_after= $expire_after;
        }else{
            $coins->expire_after= 0;
        }
        $coins->save();

        $user->points_balance = $user->points_balance + $amount;
        $user->save();
    }

    public static function getScheduleCancellationCuttoffDates($schedule){
        $return_50 = date("Y-m-d",strtotime($schedule->start_date." -".$schedule->canc_50_cuttoff_time." days"));
        $return_100 = date("Y-m-d",strtotime($schedule->start_date." -".$schedule->canc_100_cuttoff_time." days"));

        $response = array();
        $response['50'] = $return_50;
        $response['100'] = $return_100;
        return $response;

    }

    public static function doesReferralDeserveReward($client){
        $get_previous_schedules = Users_schedule::where('user_id',$client->id)->count();
        $get_previous_consultations = consultation::where('client_id',$client->id)->count();
        if($get_previous_consultations == 0 && $get_previous_schedules==0){
            return true;
        }else{
            return false;
        }
        

    }

    public static function getConsultationPrice($duration){
        // Set the price 
        $prices = Consultation_pricing::find(1);
        $hours = explode(":", $duration)[0];
        $minutes = explode(":", $duration)[1];
        $total_minutes = $hours*60 + $minutes;


        $one_hour_count = floor($total_minutes/60);
        $reminder = ($total_minutes/60) - floor($total_minutes/60);
        $reminder_price = 0;

        if($reminder <= 0.5 && $reminder > 0){
            if($one_hour_count > 0){
                $one_hour_count--;    
            }
            
            $reminder_price = $prices->pricing_45*2;

        }
        else{
            if($reminder <= 0.75 && $reminder > 0 ){
                $reminder_price = $prices->pricing_45; 
            }
            elseif($reminder > 0){
                $reminder_price = $prices->pricing_1hour;

            }
        }
        
        $total_price = ($one_hour_count*$prices->pricing_1hour) + $reminder_price;

        return $total_price;
    }

    public static function getProfileDayEndTime($user_id,$profile_type){
        $user = User::find($user_id);
        if(isset($user->getProfile)){
            if($user->getProfile->sign_in_req == 1){
                $profile = Helpers::getProfile($user_id,$profile_type);
                $end_of_day = $profile->end_of_day;

                return $end_of_day;
            }else{
                return "24:00:00";
            }  
        }else{
            return "00:00:00";

        } 
        
    }
    public static function getWeekends($user_id){
        $user = User::find($user_id);
        $weekends = explode(",",$user->getProfile->weekends);
        return $weekends;   
    }
    public static function getHolidays($user_id){
        $user = User::find($user_id);
        $profile = Helpers::getProfile($user->id,"normal");

        $getHolidays = $profile->getHolidayProfile->getHolidays;   
        $holidays = array();
        foreach($getHolidays as $holiday){
            array_push($holidays,$holiday->getHoliday);
        }

        return $holidays;

    }
    

    public static function checkNextDayLimits($dateTime,$user_id,$profile_type= "normal"){

        $day_start_time = Helpers::getProfileDayStartTime($user_id,$profile_type,"Without permissiblity");
        $day_end_time = Helpers::getProfileDayEndTime($user_id,$profile_type);

        $next_start_of_day = Helpers::moveUntill($dateTime,"24:00:00",$day_start_time);
        $next_end_of_day = Helpers::moveUntill($dateTime,"24:00:00",$day_end_time);
       

        $dayLimits = array();
        $dayLimits['start_of_day'] = $next_start_of_day;
        $dayLimits['end_of_day'] = $next_end_of_day;

        return $dayLimits;
    }
    public static function isWithinWorkDay($dateTime,$user_id,$profile_type= "normal"){
        $response = array();
        $dayLimits = Helpers::checkNextDayLimits($dateTime,$user_id,$profile_type= "normal");
        $next_end_of_day = $dayLimits['end_of_day'];

        $next_start_of_day = $dayLimits['start_of_day'];
        
        $profile = Helpers::getProfile($user_id,$profile_type);
        $permissiblity = Helpers::timeToSeconds($profile->time_allowed_before_sign_in);
        $next_start_of_day_in_seconds = strtotime($next_start_of_day);
        $start_of_day_after_adding_permissiblility = $next_start_of_day_in_seconds- $permissiblity;
        
        
        if(strtotime($next_end_of_day) <= strtotime($next_start_of_day)  ){

            $response['status'] = true;
            $response['case'] = 1;
            if(strtotime($dateTime) > $start_of_day_after_adding_permissiblility && strtotime($dateTime)<$next_start_of_day_in_seconds){
                
                $response['case'] = 2;
                return $response;
            }

            return $response;
        }else{

            
            if(strtotime($dateTime) > $start_of_day_after_adding_permissiblility && strtotime($dateTime)<$next_start_of_day_in_seconds){
                $response['status'] = true;
                $response['case'] = 2;
                return $response;
            }
            $response['status'] = false;
            return $response;
        }
        

    }
    public static function getDayLimits($dateTime,$user_id,$profile_type= "normal"){
        $profile = Helpers::getProfile($user_id,$profile_type);
        
    	$within_worday = Helpers::isWithinWorkDay($dateTime,$user_id,$profile_type= "normal");
        
        if($within_worday['status']){
            if($within_worday['case'] ==1){
            	$nextDayLimits = Helpers::checkNextDayLimits($dateTime,$user_id,$profile_type= "normal");
                $end_of_day = $nextDayLimits['end_of_day'];
                $start_of_day = date("Y-m-d H:i:s",strtotime($nextDayLimits['start_of_day']." -1 day"));
            }else{
                $nextDayLimits = Helpers::checkNextDayLimits($dateTime,$user_id,$profile_type= "normal");
                $end_of_day = $nextDayLimits['end_of_day'];
                $start_of_day = $nextDayLimits['start_of_day'];

            }

            $realDayLimits = array();
            $realDayLimits['start_of_day'] = $start_of_day;
            $realDayLimits['end_of_day'] = $end_of_day;
            
            return $realDayLimits;
        }else{
            $nextDayLimits = Helpers::checkNextDayLimits($dateTime,$user_id,$profile_type= "normal");
            $start_of_day = date("Y-m-d H:i:s",strtotime($nextDayLimits['start_of_day']." -1 day"));
            $end_of_day = Helpers::moveUntill($start_of_day,"24:00:00",$profile->end_of_day);
            $realDayLimits = array();
            $realDayLimits['start_of_day'] = $start_of_day;
            $realDayLimits['end_of_day'] = $end_of_day;
            return $realDayLimits;

        }
	
    }
    public static function isWithin($dateTime,$dateTime1,$dateTime2){
        if(strtotime($dateTime) >= strtotime($dateTime1) && strtotime($dateTime) <= strtotime($dateTime2)){
            return true;
        }else{
            return false;
        }
    }
    public static function getFullDayData($dateTime,$user_id,$profile_type= "normal"){
        $profile = Helpers::getProfile($user_id,$profile_type);
        if (strpos($dateTime, ':') !== false) {

        }else{
            $dateTime = $dateTime." ".$profile->sign_in_start_time;
        }
        
        $dayLimits = Helpers::getDayLimits($dateTime,$user_id,$profile_type= "normal");
        $sign_in_start_datetime = $dayLimits['start_of_day'];
        $start_of_day_with_permissibility = Helpers::addIntervalToDate($dayLimits['start_of_day'],$profile->time_allowed_before_sign_in,"Subtract");
        $sign_in_end_datetime  = Helpers::timesToDateTimes($profile->sign_in_start_time,$profile->sign_in_end_time,date('Y-m-d'),strtotime($dayLimits['start_of_day']));
        $sign_in_end_datetime = $sign_in_end_datetime['datetime2'];

        $minor_tardiness_datetime = Helpers::addIntervalToDate($sign_in_end_datetime,$profile->minor_tardiness_range,"Add");
        $major_tardiness_datetime = Helpers::addIntervalToDate($minor_tardiness_datetime,$profile->major_tardiness_range,"Add");

        $response = array();
        $response['start_of_day_w_permiss'] = $start_of_day_with_permissibility;
        $response['start_of_day'] = $dayLimits['start_of_day'];
        $response['sign_in_end_datetime'] = $sign_in_end_datetime;
        $response['minor_tardiness'] = $minor_tardiness_datetime;
        $response['major_tardiness'] = $major_tardiness_datetime;
        $response['end_of_day'] = $dayLimits['end_of_day'];
        $response['date'] = date("Y-m-d",strtotime($dayLimits['start_of_day']));
        return $response;




    }
    public static function alreadySignedIn($dateTime,$user_id,$profile_type= "normal"){
    	$user = User::find($user_id);
    	$dayLimits = Helpers::getDayLimits($dateTime,$user->id,$profile_type);
        $date = date("Y-m-d",strtotime($dayLimits['start_of_day']));
    	$check_signed_in = Sign_in_out::where('date',$date)->where('emp_id',$user->id)->count();	
    	if($check_signed_in == 0){
    		return false;
    	}else{
    		return true;
    	}
    }
    public static function thisDayIn($date,$setOfDays){
        if(!is_array($setOfDays)){
            $setOfDays = explode(",", $setOfDays);
        }
        $day = date("l",strtotime($date));
            if(in_array($day, $setOfDays)){
                return true;
            }else{
                return false;
            }
    }
    public static function isThisDate($date,$status,$type,$user_id,$add_info=""){
        $date = date("Y-m-d",strtotime($date));
        if($type == "Weekend"){
            $weekends = Helpers::getWeekends($user_id);
            $isWeekend = Helpers::thisDayIn($date,$weekends);
            return $isWeekend;

        }
        if($type == "Holiday"){
            $holidays = Helpers::getHolidays($user_id);
            $isHoliday = false;
            foreach($holidays as $holiday){
                $start_date = strtotime($holiday->start_date);
                $end_date = strtotime($holiday->end_date);
                $check_date = strtotime($date);
                if($check_date >=$start_date && $check_date <=$end_date ){
                    $isHoliday = true;
                }
               
            }
            return $isHoliday;

        }

        $status_names = array('Rejected','Accepted','Pending');
        $status_ids = array(0,1,2);
        $statuses = str_replace($status_names,$status_ids,$status); 
        $statuses = explode(',',$statuses);


        if($type == "Leave"){
            if($add_info==""){
                    $request_count = Req::where('type', 'LIKE', '%'.$type.'%')->where('emp_id',$user_id)->where('time_from','<=',$date)->where('time_to','>=',$date)->whereIn('status',$statuses)->count();       
                }else{
                    $request_count = Req::where('type', 'LIKE', '%'.$add_info.'%')->where('emp_id',$user_id)->where('time_from','<=',$date)->where('time_to','>=',$date)->whereIn('status',$statuses)->count();       
                }
            if($request_count == 0){
                return false;
            }else{
                return true;
            }
        }
            


        if($add_info==""){
            $request_count = Req::where('type',$type)->where('emp_id',$user_id)->where('req_day',$date)->whereIn('status',$statuses)->count();
        }else{
            $request_count = Req::where('type',$type)->where('emp_id',$user_id)->where('req_day',$date)->whereIn('status',$statuses)->where("add_info",$add_info)->count();
        }
        if($request_count == 0){
            return false;
        }else{
            return true;
        }
        

    }
    public static function datetimeToDate($dateTime){
        $date = date("Y-m-d",strtotime($dateTime));
        return $date;

    }
    public static function toTime($date){
        $time = date("H:i:s",strtotime($date));
        return $time;

    }
    
    

    public static function timeDifference($time1,$time2){
    	$time1 = new DateTime($time1);
		$time2 = new DateTime($time2);
		$interval = $time1->diff($time2);

		return $interval->format("%H:%i:%s");
    }
    public static function overtimeDifference($time1,$time2){
        $time1 = Helpers::timeToSeconds($time1);
        $time2 = Helpers::timeToSeconds($time2);
        $interval = $time1-$time2;

        return Helpers::overtimeSecondsToTime($interval);
    }
    public static function subtractDates($date1,$date2){
        $current_time_zone = date_default_timezone_get();
        date_default_timezone_set('UTC');
        $result = strtotime($date1) - strtotime($date2);
        $result = date("H:i:s",$result);
        date_default_timezone_set($current_time_zone);
        return $result;

    }
    public static function addIntervalToTime($time,$interval,$operation){
        $current_time_zone = date_default_timezone_get();
        
        date_default_timezone_set('UTC');
        $time = strtotime("1989-06-18 ".$time);
        $interval = strtotime("1989-06-18 ".$interval);
        if($operation == "Add"){
            $diff = $time + $interval ;
        }else{
            $diff = $time - $interval ;
        }
        $result = date("H:i:s",$diff);
        date_default_timezone_set($current_time_zone);
        return $result;
    }
    public static function addIntervalToDate($date,$interval,$operation){
        $current_time_zone = date_default_timezone_get();
        date_default_timezone_set('UTC');
        $date = strtotime($date);

        $interval = Helpers::timeToSeconds($interval);

        if($operation == "Add"){
            $diff = $date + $interval ;
        }else{
            $diff = $date - $interval ;
        }
        $result = date("Y-m-d H:i:s",$diff);
        date_default_timezone_set($current_time_zone);
        return $result;
    }

    public static function timeToSeconds($time){
    	$parts = explode(":",$time);
        if(isset($parts[2])){
    	   $seconds = ($parts[0]*60*60)+($parts[1]*60)+($parts[2]);
        }else{
            $seconds = ($parts[0]*60*60)+($parts[1]*60);    
        }
    	return $seconds;
    }
    public static function timeToMins($time){
        $parts = explode(":",$time);
        $seconds = ($parts[0]*60)+($parts[1])+($parts[2]/60);
        return $seconds;
    }

    public static function timeToHours($time){
    	$parts = explode(":",$time);
    	$seconds = ($parts[0])+($parts[1]/60)+($parts[2]/60/60);
    	return $seconds;
    }

    public static function moveUntill($dateTime,$interval,$time,$direction = "Forward"){
        $dateTime_mins = strtotime($dateTime)/60 +1;
        $interval_in_mins = Helpers::timeToMins($interval);
        if($direction == "Forward"){
            for($i=$dateTime_mins;$i<=$dateTime_mins+$interval_in_mins;$i++){
                $loop_seconds = $i*60;
                $currTime = date("H:i",$loop_seconds).":00";
                if($currTime == $time ){
                    $endDateTime = date("Y-m-d H:i",$loop_seconds);
                    return $endDateTime;
                }
            }
        }else{
            for($i=$dateTime_mins;$i<=$dateTime_mins+$interval_in_mins;$i--){
                $loop_seconds = $i*60;
                $currTime = date("H:i",$loop_seconds).":00";
                if($currTime == $time ){
                    $endDateTime = date("Y-m-d H:i",$loop_seconds);
                    return $endDateTime;
                }
            }

        }
    }

    public static function moveUntillDayName($date,$day_name){
        
        for($i=0; $i<=7 ; $i++){
            $date1 = date("Y-m-d",strtotime($date." +".$i." days"));
            $date_day_name = date("l",strtotime($date1));
            
            if($date_day_name == $day_name){
                return date("Y-m-d",strtotime($date1));
            }

        }
    }

    public static function getDayType($datetime,$user_id,$profile_type="normal"){
        if(!Helpers::isWithinWorkDay($datetime,$user_id,$profile_type)){
            return Null;
        }

    }


    public static function canWorkFromHomeOnThisDay($date,$user_id){
        $user = User::find($user_id);
        $check_from_requests = Helpers::isThisDate($date,"Accepted","Home",$user->id);
        // User canT work from home
        if($user->can_work_home == 0){
            return false;
        }

        if($user->flexible_home == 1){
            
            return $check_from_requests;
        }else{
            // user canT exchange days
            if($user->can_ex_days == 0){
                if(Helpers::thisDayIn($date,$user->home_days)){
                    return true;
                }else{
                    return false;
                }
            //User can exchange days
            }else{
                // User exchanged day
                if($check_from_requests){
                    return true;
                // user didnT exchange
                }else{
                    // check if this day is a homeday
                    if(Helpers::thisDayIn($date,$user->home_days)){
                        $weekLimits = Helpers::getWeekOfThis($date,$user->id);
                        $today_name = date("l",strtotime($date));
                        $week_start_date = $weekLimits['start_date'];
                        for($i=0;$i<=6;$i++){
                            $current_date = date("Y-m-d",strtotime($week_start_date." +".$i." days"));
                            $check_from_requests = Helpers::isThisDate($current_date,"Accepted","Home",$user->id,$today_name);
                            // check if this homeday was exchanged before or not
                            if($check_from_requests){
                                return false;
                            }
                        }
                        return true;
                    }else{
                        return false;
                    }


                }
            }
        }

    }
    public static function addDates($date1,$date2,$operation){
        $date1 = strtotime($date1);
        $date2 = strtotime($date2);

        if($operation == "Add"){
            $result = $date1 + $date2;    
        }else{
            $result = $date1 - $date2;      
        }
        return Helpers::secondsToTime($result);
    }
    public static function secondsToTime($seconds) {
    $dtF = new \DateTime('@0');
    $dtT = new \DateTime("@$seconds");
    $hours = $dtF->diff($dtT)->format('%h');
    $mins = $dtF->diff($dtT)->format('%i');
    return $hours.":".$mins;
    }
    public static function overtimeSecondsToTime($seconds) {
        $hours = $seconds/60/60;
        $fraction = $hours - floor($hours);
        $hours = floor($hours);
        $mins = $fraction*60;
        $fraction = $mins - floor($mins);
        $mins = floor($mins);
        $secs = round($fraction*60);
        return $hours.":".$mins.":".$secs;

    }
    public static function getWeekOfThis($date,$user_id){
        $profile = Helpers::getProfile($user_id,"normal");
        $start_of_week = $profile->week_start_day;
        $req_day_order = date("N",strtotime($date));
        $req_day_order=$req_day_order+2;
        if($req_day_order >7){
            $req_day_order = $req_day_order -7; 
        }
        $req_day_week_offset = $req_day_order - $start_of_week;
        if($req_day_week_offset<0){
            $req_day_week_offset = $req_day_week_offset +7;
        }
        $week_start_date = date("Y-m-d",strtotime($date."- ".$req_day_week_offset." days"));
        $week_end_date = date("Y-m-d",strtotime($week_start_date."+ 6 days"));

        $weekLimits = array();
        $weekLimits['start_date'] = $week_start_date;
        $weekLimits['end_date'] = $week_end_date;
        return $weekLimits;

    }

    public static function timeToDateTimeWithDate($date,$time){
        return $date." ".$time;
    }

    public static function areTimesSequential($time1,$time2){
        $datetime1 = Helpers::timeToDateTimeWithDate("1989-06-18",$time1);
        $datetime2 = Helpers::timeToDateTimeWithDate("1989-06-18",$time2);
        
        if(strtotime($datetime1) <= strtotime($datetime2)){
            return true;
        }else{
            return false;
        }
    }

    public static function timeToDateTime($time1,$time2,$date){
        $response =array();
        if(Helpers::areTimesSequential($time1,$time2)){

            $dateTime1 = Helpers::timeToDateTimeWithDate($date,$time1);
            $dateTime2 = Helpers::timeToDateTimeWithDate($date,$time2);
        }else{
            $dateTime1 = Helpers::timeToDateTimeWithDate($date,$time1);
            $date = date("Y-m-d",strtotime($date." +1 day"));
            $dateTime2 = Helpers::timeToDateTimeWithDate($date,$time2);
        }
        $response['datetime1']=$dateTime1;
        $response['datetime2']=$dateTime2;
        //return $response;
        return $dateTime2;

    }

    public static function timesToDateTimes($time1,$time2,$date){
        $response =array();
        if(Helpers::areTimesSequential($time1,$time2)){

            $dateTime1 = Helpers::timeToDateTimeWithDate($date,$time1);
            $dateTime2 = Helpers::timeToDateTimeWithDate($date,$time2);
        }else{
            $dateTime1 = Helpers::timeToDateTimeWithDate($date,$time1);
            $date = date("Y-m-d",strtotime($date." +1 day"));
            $dateTime2 = Helpers::timeToDateTimeWithDate($date,$time2);
        }
        $response['datetime1']=$dateTime1;
        $response['datetime2']=$dateTime2;
        return $response;
        

    }



    public static function getRemainingLeaves($user_id,$year,$type,$profile_type="normal"){

        $user = User::find($user_id);
        $profile = Helpers::getProfile($user->id,$profile_type);
        $penalties = 0;
        $accepted_requests = 0;
        if($type == "Normal"){
            $annual_leaves = $profile->allowed_normal_leaves;
            $penalties = Penalty::where('emp_id',$user->id)->where('deducted_from','Normal Leaves')->whereYear('date', '=', $year)->sum('quantity');
            $accepted_requests = Req::where('emp_id',$user->id)->where('type','Normal Leaves')->whereYear('time_from','=',$year)->whereIn('status',array(1,2))->sum('interval_in_days');
            $balance_added = Balance::where('year',$year)->where('emp_id',$user->id)->where('type','Normal Leaves')->sum('quantity');
        }else{
            $annual_leaves = $profile->allowed_emergency_leaves;
            $accepted_requests = Req::where('emp_id',$user->id)->where('type','Emergency Leaves')->whereYear('time_from','=',$year)->whereIn('status',array(1,2))->sum('interval_in_days');
            $balance_added = Balance::where('year',$year)->where('emp_id',$user->id)->where('type','Emergency Leaves')->sum('quantity');
        }

        return $annual_leaves - $accepted_requests - $penalties + $balance_added;
    }

    public static function deductFrom($user_id,$year,$type,$profile_type,$quantity){
        $user = User::find($user_id);
        $profile = Helpers::getProfile($user->id,$profile_type);
        $response = array();
        $response['leaves'] = 0;
        $response['salary'] = 0;
        if($type == "minor"){
            if($profile->minor_tardiness_penalty_type == "Leaves"){
                $remaining_leaves = Helpers::getRemainingLeaves($user_id,$year,"Normal",$profile_type="normal");
                if($remaining_leaves < $quantity){
                    $difference = $quantity - $remaining_leaves;
                    $response['leaves'] = $remaining_leaves;
                    $response['salary'] = $difference;
                }else{
                    $response['leaves'] = $quantity;
                    $response['salary'] = 0;
                }
            }else{
                $response['leaves'] = 0;
                $response['salary'] = $quantity;    
            }
        //Major
        }elseif($type == "major"){
            if($profile->major_tardiness_penalty_type == "Leaves"){
                $remaining_leaves = Helpers::getRemainingLeaves($user_id,$year,"Normal",$profile_type);
                if($remaining_leaves < $quantity){
                    $difference = $quantity - $remaining_leaves;
                    $response['leaves'] = $remaining_leaves;
                    $response['salary'] = $difference;
                }else{
                    $response['leaves'] = $quantity;
                    $response['salary'] = 0;
                }
            }else{
                $response['leaves'] = 0;
                $response['salary'] = $quantity;    
            }
        }elseif($type == "early signout"){
            if($profile->early_signout_deduction_type == "Leaves"){
                $remaining_leaves = Helpers::getRemainingLeaves($user_id,$year,"Normal",$profile_type);
                if($remaining_leaves < $quantity){
                    $difference = $quantity - $remaining_leaves;
                    $response['leaves'] = $remaining_leaves;
                    $response['salary'] = $difference;
                }else{
                    $response['leaves'] = $quantity;
                    $response['salary'] = 0;
                }
            }else{
                $response['leaves'] = 0;
                $response['salary'] = $quantity;    
            }

        }elseif($type == "absent"){
            if($profile->emergency_penalty_type == "Leaves"){
                $remaining_leaves = Helpers::getRemainingLeaves($user_id,$year,"Normal",$profile_type);
                if($remaining_leaves < $quantity){
                    $difference = $quantity - $remaining_leaves;
                    $response['leaves'] = $remaining_leaves;
                    $response['salary'] = $difference;
                }else{
                    $response['leaves'] = $quantity;
                    $response['salary'] = 0;
                }
            }else{
                $response['leaves'] = 0;
                $response['salary'] = $quantity;    
            }

                   
            

        }
        return $response;

    }

    public static function getSignInOut($date,$user_id){
        $sign_in_out = Sign_in_out::where('emp_id',$user_id)->where('date',$date)->first();
        return $sign_in_out;
    }

    public static function penalise($user_id,$date,$type,$profile_type){
        $sign_in_out = Helpers::getSignInOut(date("Y-m-d",strtotime($date)),$user_id);
        if($sign_in_out){
            $sign_in_id = $sign_in_out->id;
        }else{
            $sign_in_id = 0;    
        }
        $user = User::find($user_id);
        date_default_timezone_set($user->getOffice->time_zone);
        $now = date("Y-m-d H:i:s");
        $profile = Helpers::getProfile($user->id,$profile_type);
        if($type == "minor" || $type == "major"){
            $field_name = $type."_tardiness_penalty";    
        }elseif($type == "early signout"){

            $field_name = "early_signout_deduction_days";    
        }elseif( $type == "absent"){
            $field_name = "emergency_penalty";   
        }
        
        $deduct_from = Helpers::deductFrom($user->id,$date,$type,$profile_type,$profile->$field_name);
        
        if($deduct_from['leaves'] != 0){
            $penalty = new Penalty();
            $penalty->sign_in_id = $sign_in_id;
            $penalty->date = $date;
            $penalty->date = $date;
            $penalty->company_id = $user->company_id;
            $penalty->office_id = $user->office_id;
            $penalty->emp_id = $user->id;
            $penalty->quantity = $deduct_from['leaves'];
            $penalty->deducted_from = 'Normal Leaves';
            $penalty->penalty_reason = $type.' penalty';
            $penalty->penalty_created_at = $now;
            $penalty->save();
            Helpers::sendPenaltyEmail($penalty);


        }
        if($deduct_from['salary'] != 0){
            $penalty = new Penalty();
            $penalty->sign_in_id = $sign_in_id;
            $penalty->date = $date;
            $penalty->company_id = $user->company_id;
            $penalty->office_id = $user->office_id;
            $penalty->emp_id = $user->id;
            $penalty->quantity = $deduct_from['salary'];
            $penalty->deducted_from = 'Salary';
            $penalty->penalty_reason = $type.' penalty';
            $penalty->penalty_created_at = $now;
            $penalty->save();
            Helpers::sendPenaltyEmail($penalty);

        }
        
        // check if user is absent this day
        $absent_penalty = Penalty::where('emp_id',$user_id)->where('date',$date)->where('penalty_reason', 'LIKE', '%absent%')->first();
        // if user is absent remove other penalties and leave only the absence penalty
        if($absent_penalty){
            $delete_other_penalties = Penalty::where('emp_id',$user_id)->where('date',$date)->where('id', '!=', $absent_penalty->id)->delete();
        }
    }

    public static function signInOutButtonStatus($user_id,$date){
        $dayLimits = Helpers::getDayLimits($date,$user_id,"normal");
        $date = date("Y-m-d",strtotime($dayLimits['start_of_day']));
        $today_sign_in = Sign_in_out::where('emp_id',$user_id)->where('date',$date)->orderBy('id','desc')->whereNull("sign_out_time")->count();
        $response = array();
        if($today_sign_in > 0){
            
            $response['show'] = "sign_out";
            $response['hide'] = "sign_in";
            
        }else{
            $response['show'] = "sign_in";
            $response['hide'] = "sign_out";
        }
        return $response;
        
    }

    public static function getRemainingOvertime($user_id,$date){
        $month = date("m",strtotime($date));
        $year = date("Y",strtotime($date));
        $user = User::find($user_id);
        $profile = Helpers::getProfile($user->id,"normal");
        $taken_overtime = 0;
        $monthly_overtime = $profile->max_overtime_per_month;
        $previous_overtime = 
                    DB::table('requests')
                     ->select(DB::raw('SEC_TO_TIME(SUM(TIME_TO_SEC(interval_in_time))) as previous_overtime_interval'))
                     ->where('emp_id', $user->id)
                     ->whereMonth('req_day','=',$month)
                     ->whereYear('req_day','=',$year)
                     ->where('type','Overtime')
                     ->whereIn('status',array(1,2))
                     ->first();
        $remaining_interval = Helpers::overtimeDifference($profile->max_overtime_per_month,$previous_overtime->previous_overtime_interval);
        
        return $remaining_interval;
    }

    public static function validateIntersectingIntervalsRequests($user_id,$time_from,$time_to,$request_type){
        $time_from = date("Y-m-d H:i:s",strtotime($time_from));
        $time_to = date("Y-m-d H:i:s",strtotime($time_to));
       
        $response = true;
        if($request_type == "Permission"){
            $array = array('Permissions','Emergency Leaves','Normal Leaves');
            $total_requests = 
            Req::
            where("time_to",'>',$time_from)->where("actual_time_from",'<',$time_to)
            ->where('emp_id',$user_id)
            ->whereIn('status',array(1,2))
            ->whereIn('type',$array)
            ->count();


        }
        else{

            $array = array('Permissions','Emergency Leaves','Normal Leaves', 'Half Day');
            $intersecting_accepted_or_pending_request1 = 
            Req::
            where('req_day','>=',$time_from)->where('req_day','<=',$time_to)
            ->where('emp_id',$user_id)
            ->whereIn('status',array(1,2))
            ->whereIn('type',$array)
            ->count();

            $intersecting_accepted_or_pending_request2 = 
            Req::
            where("time_to",'>=',$time_from)->where("actual_time_from",'<=',$time_to)
            ->where('emp_id',$user_id)
            ->whereIn('status',array(1,2))
            ->whereIn('type',$array)
            ->count();

            $total_requests = $intersecting_accepted_or_pending_request2 + $intersecting_accepted_or_pending_request1;
        }
        if($total_requests>0){
            $response = false;
        }
        return $response;
        

        
    }
    public static function validateIntersectingDayRequests($user_id,$date,$request_type){
        $date = date("Y-m-d H:i:s",strtotime($date));
        if($request_type == "Home"){
            $array = array('Emergency Leaves','Normal Leaves', 'Home');

            $intersecting_accepted_or_pending_request1 = 
            Req::
            where('time_from','<=',$date)->where('time_to','>=',$date)
            ->where('emp_id',$user_id)
            ->whereIn('status',array(1,2))
            ->whereIn('type',$array)
            ->count();

            $intersecting_accepted_or_pending_request2 = 
            Req::
            where('req_day',$date)
            ->where('emp_id',$user_id)
            ->whereIn('status',array(1,2))
            ->whereIn('type',$array)
            ->count();

            $total_requests = $intersecting_accepted_or_pending_request2 + $intersecting_accepted_or_pending_request1;

        }else{
            $array = array('Emergency Leaves','Normal Leaves', 'Half Day');
            $intersecting_accepted_or_pending_request1 = 
            Req::
            where('time_from','<=',$date)->where('time_to','>=',$date)
            ->where('emp_id',$user_id)
            ->whereIn('status',array(1,2))
            ->whereIn('type',$array)
            ->count();

            $intersecting_accepted_or_pending_request2 = 
            Req::
            where('req_day',$date)
            ->where('emp_id',$user_id)
            ->whereIn('status',array(1,2))
            ->whereIn('type',$array)
            ->count();

            $total_requests = $intersecting_accepted_or_pending_request2 + $intersecting_accepted_or_pending_request1;


        }

        if($total_requests >0 ){
            return false;
        }else{
            return true;
        }
        
    }

    public static function getWeekendsHolidaysWithin($time_from,$time_to,$user_id){
        
        $count = 0;
        $user = User::find($user_id);
        $profile = Helpers::getProfile($user->id,"normal");

        $datePeriod = Helpers::getDatesBetweenDateTimes($time_from, $time_to);
        foreach($datePeriod as $date) {
            $i = 0;
            $date = $date->format('Y-m-d H:i:s');
            
            $weekend = Helpers::isThisDate($date,"","Weekend",$user->id);
            if($weekend){
                $i = 1;
                $count = $count +1;
            }else{
                $holiday = Helpers::isThisDate($date,"","Holiday",$user->id);
                if($holiday){
                    $i = 2;
                    $count = $count +1;    
                }
            }
            
        }

    return $count;
    }

    public static function getCountOfDaysBetween($time_from,$time_to){
        $count = 0;
        $datePeriod = Helpers::getDatesBetweenDateTimes($time_from, $time_to);
        foreach($datePeriod as $date) {
            $count = $count +1;
        }

    return $count;
    }
    



    public static function getDatesBetweenDateTimes($fromdate, $todate) {
        $fromdate = date("d/m/Y",strtotime($fromdate));
        $todate = date("d/m/Y",strtotime($todate));
        $fromdate = \DateTime::createFromFormat('d/m/Y', $fromdate);
        $todate = \DateTime::createFromFormat('d/m/Y', $todate);
        return new \DatePeriod(
            $fromdate,
            new \DateInterval('P1D'),
            $todate->modify('+1 day')
        );
    }


    public static function getRemainingPermissions($user_id,$dateTime,$type){
        $within_worday = Helpers::isWithinWorkDay($dateTime,$user_id,$profile_type= "normal");
        if(!$within_worday){
            return Null;

        }
        $user = User::find($user_id);
        $profile = Helpers::getProfile($user->id,'normal');
        $dayLimits = Helpers::getDayLimits($dateTime,$user->id,"normal");
        $start_of_day = date("Y-m-d",strtotime($dayLimits['start_of_day']));
        $day = date("d",strtotime($start_of_day));
        $month = date("m",strtotime($start_of_day));
        $year = date("Y",strtotime($start_of_day));
        if($type == "day"){
            $previous_interval = DB::table('requests')
             ->select(DB::raw('SEC_TO_TIME(SUM(TIME_TO_SEC(interval_in_time))) as previous_interval'))
             ->where('emp_id', $user->id)
             ->where('req_day',$start_of_day)
             ->where('type','Permissions')
             ->whereIn('status',array(1,2))
             ->first();

             $balance_added = 0;
         }else{
            $previous_interval = DB::table('requests')
             ->select(DB::raw('SEC_TO_TIME(SUM(TIME_TO_SEC(interval_in_time))) as previous_interval'))
             ->where('emp_id', $user->id)
             ->whereMonth('req_day','=',$month)
             ->whereYear('req_day','=',$year)
             ->where('type','Permissions')
             ->whereIn('status',array(1,2))
             ->first();

             $month_year = date("m-Y",strtotime($year."-".$month."-1"));
             $balance_added = Balance::where('emp_id',$user->id)->where('month',$month_year)->where('type','Permissions')->sum('quantity');

         }
        $field_name = "max_units_per_".$type;

        if(!$previous_interval->previous_interval){
            $previous_interval->previous_interval = "00:00:00";
        }
        $previous_interval_in_mins = Helpers::timeToMins($previous_interval->previous_interval);

        $max_interval = (Helpers::timeToMins($profile->min_permission_unit)) * $profile->$field_name;

        return $max_interval - $previous_interval_in_mins + $balance_added;
    }

    public static function isThisRequestHolidayWeekend($user_id,$date){
         
        if (strpos($date, ':') !== false) {
            
          $dayLimits = Helpers::getDayLimits($date,$user_id,"normal");
          $date = date("Y-m-d",strtotime($dayLimits['start_of_day']));
        }

        $holiday = Helpers::isThisDate($date,"","Holiday",$user_id);
        $weekend = Helpers::isThisDate($date,"","Weekend",$user_id); 

        if($holiday || $weekend){
            return true;
        } 
        return false;
          

    }

    public static function rollbackSignInOut($id){
        $delete_sign_in = Sign_in_out::find($id)->delete();
        $delete_penalties = Penalty::where('sign_in_id',$id)->delete();
        $delete_overtime_requests = Req::where('sign_in_id',$id)->delete();
    }

    public static function applyRequest($req_id){
        $now = date("Y-m-d H:i:s");
        $req = Req::find($req_id);
        $todayLimits = Helpers::getDayLimits($now,$req->emp_id,"normal");
        $today = $todayLimits['start_of_day'];

        if($req->type == "Permissions"){
            $sign_in_out = Sign_in_out::where('date',$req->req_day)->where('emp_id',$req->emp_id)->first();

            if($sign_in_out){
                
                $dayLimits = Helpers::getDayLimits($sign_in_out->sign_in_time,$req->emp_id,$sign_in_out->day_type);
                $profile = Helpers::getProfile($sign_in_out->emp_id,$sign_in_out->day_type);
                // check if the permission start time is inside the sign in range
                $start_of_day_with_permissibility = Helpers::addIntervalToDate($dayLimits['start_of_day'],$profile->time_allowed_before_sign_in,"Subtract");
                $sign_in_end_datetime  = Helpers::timesToDateTimes($profile->sign_in_start_time,$profile->sign_in_end_time,date('Y-m-d'),strtotime($dayLimits['start_of_day']));
                $sign_in_end_datetime = $sign_in_end_datetime['datetime2'];
                if(strtotime($req->time_from) >= strtotime($start_of_day_with_permissibility) && strtotime($req->time_from) <= strtotime($sign_in_end_datetime)){
                    // subtract the permission interval from the sign in time
                    $new_sign_in_time =  Helpers::addIntervalToDate($sign_in_out->sign_in_time,$req->interval_in_time,"Subtract");

                    if(strtotime($new_sign_in_time) < strtotime($dayLimits['start_of_day'])){
                        $new_sign_in_time =  $dayLimits['start_of_day'];   
                    }
                    // override the sign in
                   
                    SignInOut::overRideSignInOut($sign_in_out->id,$new_sign_in_time,$sign_in_out->sign_out_time);


                }
                // check if the permission intersects with the calculated sign out range
                $calculated_sign_out_time = $sign_in_out->calculated_sign_out_time;
                if(strtotime($calculated_sign_out_time) >= $req->time_from && strtotime($calculated_sign_out_time) <= $req->time_to){
                    // add the permission interval from the sign out time
                    $new_sign_out_time =  Helpers::addIntervalToDate($sign_in_out->sign_out_time,$req->interval_in_time,"Add");
                    if(strtotime($new_sign_out_time) > strtotime($calculated_sign_out_time)){
                        $new_sign_out_time =  $calculated_sign_out_time;   
                    }
                    // override the sign in
                    SignInOut::overRideSignInOut($sign_in_out->id,$sign_in_out->sign_in_time,$new_sign_out_time);

                }

            }
        }
        if($req->type == "Emergency Leaves" || $req->type == "Normal Leaves"){
            
            
            // if the request end time is before now
            if( strtotime($req->actual_time_to) <= strtotime($today)){
                $absent_sign_ins = Sign_in_out::where('emp_id',$req->emp_id)->where('status','absent')->where('date','>=',$req->time_from)->where('date','<=',$req->time_to)->get();

                foreach($absent_sign_ins as $sign_in){
                    
                    Helpers::rollbackSignInOut($sign_in->id);
                    $sign_in_out = new Sign_in_out();
                    $sign_in_out->emp_id = $sign_in->emp_id;
                    $sign_in_out->status ="Leave";
                    $sign_in_out->date = $sign_in->date;
                    $sign_in_out->attended = 0;
                    $sign_in_out->save();
                }

                
            }
        }
        if(strtotime($today) >= strtotime($req->actual_time_to)){
            $req->taken =1;
            $req->save();
        }
    }

    public static function sendReqEmail($req){

        $req = Req::find($req->id);
        
        // if the request is pending send to manager
        if($req->status ==2){
            // send to manager
            $manager = user::find($req->getEmployee->manager_id);
            Mail::send('emails.request', ['req' => $req  ], function ($m) use ($manager) {
                $m->from('notifications@arabiclocalizer.info', 'Arabic Localizer');

                $m->to($manager->email, $manager->name)->subject(' Arabic Localizer | New Request Submitted');
                
            });
            // send to copied managers
            foreach($req->getEmployee->getCopiedManagers as $manager){
                Mail::send('emails.request', ['req' => $req  ], function ($m) use ($manager) {
                    $m->from('notifications@arabiclocalizer.info', 'Arabic Localizer');

                    $m->to($manager->email, $manager->name)->subject(' Arabic Localizer | New Request Submitted');
                
                });    
            }

        }
        // if the manager replied to the request
        else{
            $employee = $req->getEmployee;
            Mail::send('emails.request', ['req' => $req  ], function ($m) use ($employee) {
                $m->from('notifications@arabiclocalizer.info', 'Arabic Localizer');

                $m->to($employee->email, $employee->name)->subject(' Arabic Localizer | Request Response');
                
            });


        }
    }

    public static function sendPenaltyEmail($penalty){
        // send to employee
        $employee = $penalty->getEmployee;
        Mail::send('emails.penalty', ['penalty' => $penalty  ], function ($m) use ($employee) {
            $m->from('notifications@arabiclocalizer.info', 'Arabic Localizer');

            $m->to($employee->email, $employee->name)->subject(' Arabic Localizer | New Penalty Added');
            
        });
        // send to manager
        $manager = user::find($penalty->getEmployee->manager_id);
        Mail::send('emails.penalty', ['penalty' => $penalty  ], function ($m) use ($manager) {
            $m->from('notifications@arabiclocalizer.info', 'Arabic Localizer');

            $m->to($manager->email, $manager->name)->subject(' Arabic Localizer | Employee Penalised');
            
        });
        // send to copied managers
        foreach($penalty->getEmployee->getCopiedManagers as $manager){
            Mail::send('emails.penalty', ['penalty' => $penalty  ], function ($m) use ($manager) {
                $m->from('notifications@arabiclocalizer.info', 'Arabic Localizer');

                $m->to($manager->email, $manager->name)->subject(' Arabic Localizer | Employee Penalised');
            
            });    
        }

        
        
    }
    public static function sendNoSignOutEmail($sign_in_out){
        
        // send to manager
        $manager = user::find($sign_in_out->getEmployee->manager_id);
        Mail::send('emails.no_sign_out', ['sign_in_out' => $sign_in_out  ], function ($m) use ($manager) {
            $m->from('notifications@arabiclocalizer.info', 'Arabic Localizer');

            $m->to($manager->email, $manager->name)->subject(' Arabic Localizer | Employee Did Not Sign Out');
            
        });
        // send to copied managers
        foreach($sign_in_out->getEmployee->getCopiedManagers as $manager){
            Mail::send('emails.no_sign_out', ['sign_in_out' => $sign_in_out  ], function ($m) use ($manager) {
                $m->from('notifications@arabiclocalizer.info', 'Arabic Localizer');

                $m->to($manager->email, $manager->name)->subject(' Arabic Localizer | Employee Did Not Sign Out');
            
            });    
        }

        
        
    }
    public static function sendEarlySignOutEmail($sign_in_out){
        
        // send to manager
        $manager = user::find($sign_in_out->getEmployee->manager_id);
        Mail::send('emails.early_sign_out', ['sign_in_out' => $sign_in_out  ], function ($m) use ($manager) {
            $m->from('notifications@arabiclocalizer.info', 'Arabic Localizer');

            $m->to($manager->email, $manager->name)->subject(' Arabic Localizer | Employee Did Not Sign Out');
            
        });
        // send to copied managers
        foreach($sign_in_out->getEmployee->getCopiedManagers as $manager){
            Mail::send('emails.early_sign_out', ['sign_in_out' => $sign_in_out  ], function ($m) use ($manager) {
                $m->from('notifications@arabiclocalizer.info', 'Arabic Localizer');

                $m->to($manager->email, $manager->name)->subject(' Arabic Localizer | Employee Did Not Sign Out');
            
            });    
        }

        
        
    }

}
