<?php

namespace App\Http\Helpers;

use App\Http\Requests;

use App\Http\Helpers\Helpers;
use App\Http\Helpers\SignInOut;
use App\Sign_in_out;
use App\User;
use Auth;
use DateTime;
use App\Req;
use App\Penalty;
use DB;
use App\Office;



 class Notification
{	
    public static function Notify($users,){
        $Penalty = new Penalty();
        $Penalty-save();
        // handling not taken requests
        // get untaken requests with their corresponding office timezone
        $not_taken_requests = DB::table('requests')
        ->join('users', 'users.id', '=', 'requests.emp_id')
        ->join('offices', 'users.office_id', '=', 'offices.id')
        ->where('requests.taken',0)
        ->where('requests.status',1)
        ->select('requests.*','offices.time_zone')
        ->get();
        // loop through untaken requests
        foreach($not_taken_requests as $request){
            // set the default timezone to the office timezone
            date_default_timezone_set($request->time_zone);
            $now = date("Y-m-d H:i:s");
            if(strtotime($now) > strtotime($request->actual_time_to)){
                Helpers::applyRequest($request->id);
            }
        }



        
    }

    public static function pastDays(){
        // Handling past days
        $all_offices = Office::where('admin_show',1)->get();

        foreach($all_offices as $office){
            date_default_timezone_set($office->time_zone);
            
            $employees = DB::table('users')
            ->join('att_profiles', 'users.att_profile_id', '=', 'att_profiles.id')
            ->where('users.office_id',$office->id)
            
            ->where('att_profiles.sign_in_req',1)
            ->where('users.admin_show',1)
            ->select('users.*')
            ->get();
            foreach($employees as $employee){
                
                
                $now = date("Y-m-d H:i:s");
                $profile = Helpers::getProfile($employee->id,"normal");
                // get the last ended date
                $last_ended_datetime = Helpers::moveUntill($now,"24:00:00",$profile->end_of_day,"Backward");
                $last_ended_datetime = date("Y-m-d H:i:s",strtotime($last_ended_datetime." -2 seconds"));
                $dayLimits = Helpers::getDayLimits($last_ended_datetime,$employee->id,"normal");
                $last_ended_day = date("Y-m-d",strtotime($dayLimits['start_of_day']));

                // The suggested dates that shoud have been filled for this user
                $get_suggested_days_for_this_employees = Helpers::getDatesBetweenDateTimes($employee->starts_at,$last_ended_day);
                $suggested_day_to_array  = array();
                foreach($get_suggested_days_for_this_employees as $date) {
                    
                    array_push($suggested_day_to_array,$date->format('Y-m-d'));
                }
                $get_filled_dates_array = array();
                $get_filled_dates = Sign_in_out::where('emp_id',$employee->id)->select("date")->get();
                foreach( $get_filled_dates as $date){
                    array_push($get_filled_dates_array, $date->date);
                }
                $unlisted_dates = array_diff($suggested_day_to_array, $get_filled_dates_array);
                

                foreach($unlisted_dates as $date){
                    $holiday = Helpers::isThisDate($date,"","Holiday",$employee->id);
                    $weekend = Helpers::isThisDate($date,"","Weekend",$employee->id);
                    $leave = Helpers::isThisDate($date,"Accepted","Leave",$employee->id);
                    $first_half_day = Helpers::isThisDate($date,"Accepted","Half Day",$employee->id,"First");
                    $second_half_day = Helpers::isThisDate($date,"Accepted","Half Day",$employee->id,"Second");
                    if($first_half_day){
                        $profile = "1st_half";
                    }
                    elseif($second_half_day){
                        $profile = "2nd_half";
                    }else{
                        $profile = "normal";
                    }


                    if($holiday) {
                        $status = "Holiday";
                    }
                    elseif($weekend){
                        $status = "Weekend";
                    }
                    elseif($leave){
                        $status = "Leave";
                    }
                    else{
                        $status = "Absent";
                        Helpers::penalise($employee->id,$date,"absent",$profile);
                    }
                    $sign_in_out = new Sign_in_out();
                    $sign_in_out->emp_id = $employee->id;
                    $sign_in_out->status = $status;
                    $sign_in_out->date = $date;
                    $sign_in_out->attended = 0;
                    $sign_in_out->save();
                }


            }
        }
    }

}
