<?php

namespace App\Http\Helpers;
use App\Http\Helpers\Helpers;

use App\Http\Requests;
use App\Sign_in_out;
use Auth;
use App\Penalty;
use App\Req;
use DB;
use App\User;


 class SignInOut
{	
    public static function sign_in($dateTime="now",$user_id,$home="",$day_type=""){
    $user = User::find($user_id);
    
    $day_data = Helpers::getFullDayData($dateTime,$user_id);
        if( $dateTime=="now"){
            date_default_timezone_set($user->getOffice->time_zone);
            $dateTime = date("Y-m-d H:i:s");
        }
        
        
        
        $from_home = 0;
        $profile_type = "normal";

        // validate if the user starts at is not after this datetime
        if(strtotime($user->starts_at) > strtotime($dateTime)){
            $data = [];
                $data['status'] = 'error';
                $data['page'] = 'none';
                $data['msg'] = "You can not sign in before ".$user->starts_at.", Please check with your HR";
                return $data;    
        }
        // Validate Holiday or not
        $holiday = Helpers::isThisDate($day_data['start_of_day'],"","Holiday",$user_id);
        
        if($holiday){
            // Validate can work on this weekend or not
            $can_work = "Query";
            if(!$can_work){
                $profile_type = "holiday";
            }else{
                $data = [];
                $data['status'] = 'error';
                $data['page'] = 'none';
                $data['msg'] = "You cant work on this day (Holiday)";
                return $data;
                
                
            }
        }
        // Validate Weekend or not
        $weekend = Helpers::isThisDate($day_data['start_of_day'],"","Weekend",$user_id);
        
        if($weekend){
            // Validate can work on this weekend or not
            $can_work = "Query";
            if(!$can_work){
                $profile_type = "holiday";
            }else{
                $data = [];
                $data['status'] = 'error';
                $data['page'] = 'none';
                $data['msg'] = "You cant work on this day (Weekend)";
                return $data;
                
                
            }
        }

        //Validate if this day is a half day
        $half_day = Helpers::isThisDate($day_data['start_of_day'],"Accepted","Half Day",$user_id);
        
        if($half_day){
            $first_half = Helpers::isThisDate($day_data['start_of_day'],"Accepted","Half Day",$user_id,"First");

            if($first_half){
                //First half
                
                $profile_type="1st_half";
            }else{
                //Second Half
                $profile_type="2nd_half";
            }
        }
        // overwrite the profile type if this login is edited from the hr not a normal login
        if($day_type != ""){
            $profile_type=$day_type;    
        }


        // Validate Within Workday Hours
        $within_workday = Helpers::isWithinWorkDay($dateTime,$user_id,$profile_type);

        if(!$within_workday['status']){
            $data = [];
            $data['status'] = 'error';
            $data['page'] = 'none';
            $data['msg'] = "You are out of the workday hours";
            return  $data;
            
            
        }
        // Validate Already signed in this day
        if(Helpers::alreadySignedIn($dateTime,$user_id)){
            $data = [];
            $data['status'] = 'error';
            $data['page'] = 'none';
            $data['msg'] = "You signed in before in this day";
            return  $data;
           
            
        }

        

        //Validate if this day is Work From Home Day
        $Can_Work_From_Home = Helpers::canWorkFromHomeOnThisDay($day_data['start_of_day'],$user_id);
        if($Can_Work_From_Home){
            $from_home = 1;
        }
        // overwrite the work from home status if this login is edited from the hr not a normal login
        if($home != ""){
            $from_home = $home;    
        }


        
        $profile = Helpers::getProfile($user_id,$profile_type);

        $day_limits = Helpers::getDayLimits($dateTime ,$user_id,$profile_type);
        $start_of_day = Helpers::addIntervalToDate($day_limits['start_of_day'],$profile->time_allowed_before_sign_in,"subtract");
        $day_limitsDate = date("Y-m-d",strtotime($day_limits['start_of_day']));
        $day_limitsTime = date("H:i:s",strtotime($day_limits['start_of_day']));
       



        



        // get absolute times without dates
        $sign_in_start_time = $profile->sign_in_start_time;
        $sign_in_end_time = $profile->sign_in_end_time;
        $sign_in_end_permissibility_time = Helpers::addIntervalToTime($profile->sign_in_end_time,$profile->max_lateness_permissibility,"Add");
        $minor_tardiness_end_time = Helpers::addIntervalToTime($profile->sign_in_end_time,$profile->minor_tardiness_range,"Add");
        $major_tardiness_end_time = Helpers::addIntervalToTime($minor_tardiness_end_time,$profile->major_tardiness_range,"Add");



        // adding dates to times
        $sign_in_start_time = Helpers::timeToDateTime($day_limitsTime,$sign_in_start_time,$day_limitsDate);
        $sign_in_end_time = Helpers::timeToDateTime($day_limitsTime,$sign_in_end_time,$day_limitsDate);
        $sign_in_end_permissibility_time = Helpers::timeToDateTime($day_limitsTime,$sign_in_end_permissibility_time,$day_limitsDate);
        $minor_tardiness_end_time = Helpers::timeToDateTime($day_limitsTime,$minor_tardiness_end_time,$day_limitsDate);
        $major_tardiness_end_time = Helpers::timeToDateTime($day_limitsTime,$major_tardiness_end_time,$day_limitsDate);
        
        /*dd($sign_in_start_time. " ----- ".
            $sign_in_end_time. " ----- ".
            $sign_in_end_permissibility_time. " ----- ".
            $minor_tardiness_end_time. " ----- ".
            $major_tardiness_end_time. " ----- "
        );*/
        // check if this date is an accepted  leave
        $accepted_leave = Helpers::isThisDate($sign_in_start_time,"Accepted","Leave",$user_id,"");
        if($accepted_leave){
            $data = [];
            $data['status'] = 'error';
            $data['page'] = 'none';
            $data['msg'] = "This day is an accepted leave";
            return $data;
            
            
        }

        // -------------------- User is allowed to sign in

        $sign_in = new Sign_in_out();
        $sign_in->emp_id = $user_id;
        $sign_in->date = $sign_in_start_time;
        $sign_in->save();
        $sign_in_recorded_time = $dateTime;
        if(strtotime($dateTime) >= strtotime($start_of_day)  && strtotime($dateTime) <= strtotime($sign_in_start_time)){
            // sign in abl el ma3ad el   maqbool
            $status = "Ontime";
            $early_late ="Before";
            $early_late_interval = Helpers::timeDifference($dateTime,$sign_in_start_time); // lazem nerga3 lel 7etta de
            $sign_in_recorded_time = $sign_in_start_time;
            $penalties = Null;
        }

        if(strtotime($dateTime) >= strtotime($sign_in_start_time)  && strtotime($dateTime) <= strtotime($sign_in_end_time)){
            // within interval
            $status = "Ontime";
            $early_late =Null;
            $early_late_interval = Null; 
            $penalties = Null;
        }
        if(strtotime($dateTime) >= strtotime($sign_in_end_time)  && strtotime($dateTime) <= strtotime($sign_in_end_permissibility_time)){
            // within permissibility
            $status = "Ontime";
            $early_late ="After";
            $early_late_interval =  Helpers::timeDifference($dateTime,$sign_in_end_time);// lazem nerga3 lel 7etta de
            $penalties = Null;
        }
        if(strtotime($dateTime) >= strtotime($sign_in_end_permissibility_time)  && strtotime($dateTime) <= strtotime($minor_tardiness_end_time)){
            // within Minor tardiness
            $status = "Minor Tardiness";
            $early_late ="After";
            $early_late_interval = Helpers::timeDifference($dateTime,$sign_in_end_time); // lazem nerga3 lel 7etta de
            //Penalty handling
            Helpers::penalise($user_id,$sign_in_start_time,"minor",$profile_type);
            

            
        }
        if(strtotime($dateTime) >= strtotime($minor_tardiness_end_time)  && strtotime($dateTime) <= strtotime($major_tardiness_end_time)){
            // within major tardiness
            $status = "Major Tardiness";
            $early_late ="After";
            $early_late_interval = Helpers::timeDifference($dateTime,$sign_in_end_time); 
            //Penalty handling
            Helpers::penalise($user_id,$sign_in_start_time,"major",$profile_type);
        }
        if(strtotime($dateTime) >= strtotime($major_tardiness_end_time)  && strtotime($dateTime) <= strtotime($day_limits['end_of_day'])){
            //  outside sign in interval
            $status = "Absent";
            $early_late ="After";
            $early_late_interval = Helpers::timeDifference($dateTime,$sign_in_end_time); 
            //Penalty handling
            Helpers::penalise($user_id,$sign_in_start_time,"absent",$profile_type);
            
        }

        if($early_late =="After"){
            //$sign_out_dateTime = Helpers::timeToDateTimeWithDate($day_data['start_of_day'],$profile->sign_in_end_time);
            
            $calculated_sign_out_time = Helpers::addIntervalToDate($day_data['sign_in_end_datetime'],$profile->work_hours,"Add");

        }else{
            $calculated_sign_out_time = Helpers::addIntervalToDate($sign_in_recorded_time,$profile->work_hours,"Add");
        }
        
        $sign_in->emp_id = $user_id;
        $sign_in->date = $sign_in_start_time;
        $sign_in->sign_in_time = $sign_in_recorded_time;
        $sign_in->status = $status;
        $sign_in->early_late = $early_late;
        $sign_in->early_late_time = $early_late_interval;
        $sign_in->day_type = $profile_type;
        $sign_in->from_home = $from_home;
        $sign_in->calculated_min_sign_out_time = $calculated_sign_out_time;
        $sign_in->save();

        $data = [];
        $data['status'] = 'success';
        $data['page'] = 'none';
        $data['confirm'] = $from_home;
        $data['confirm_msg'] = 'Are you working from home?';
        $data['route'] = "/admin/sign_in_out/dismissWorkFromHome";

        $data['msg'] = "Done";
        return $data;
        
        



        

        









        /*$day_limitsTime = Helpers::getProfileDayStartTime($user_id);
        $dayLimits = Helpers::getDayLimits($dateTime,$user_id);
        $holidays = Helpers::getHolidays($user_id);
        $check_date = Helpers::isThisDate($day_data['start_of_day'],"Accepted,Pending,Rejected","Leave",$user_id,"Emergency");*/
        
    }

    public static function  sign_out($dateTime,$user_id){
        $user = User::find($user_id);
        date_default_timezone_set($user->getOffice->time_zone);
        
        $sign_in = Sign_in_out::where('emp_id',$user_id)->whereNull('sign_out_time')->orderBy('id','desc')->first();
        
        $day_data = Helpers::getFullDayData($dateTime,$user_id,$sign_in->day_type);

        if( $dateTime=="now"){
            
            $dateTime = date("Y-m-d H:i:s");
            
        }
        $confirm = 0;
        $attended = 1;
        
        $profile = Helpers::getProfile($user_id,$sign_in->day_type);


        // validate if the user is signing out before the calculated sign out time
        if(strtotime($sign_in->calculated_min_sign_out_time) > strtotime($dateTime)){
            $confirm = 1;    
        }
        // Check if the user will be considered absent or not
        $considered_absent_before = Helpers::addIntervalToDate($sign_in->calculated_min_sign_out_time,$profile->sign_out_cuttoff_time,"Subtract");
        // user signing out before the cuttoff time and will be considered absent
        if(strtotime($dateTime) < strtotime($considered_absent_before) ){
            $attended = 0;  
            Helpers::penalise($user_id,$sign_in->date,"absent",$sign_in->day_type);  
        }

        if(strtotime($dateTime) > strtotime($considered_absent_before) && strtotime($dateTime) < strtotime($sign_in->calculated_min_sign_out_time)){
            Helpers::penalise($user_id,$sign_in->date,"early signout",$sign_in->day_type);

        }

        $dayLimits = Helpers::getDayLimits($sign_in->sign_in_time,$user_id,$sign_in->day_type);
        $end_of_day_dateTime = $dayLimits['end_of_day'];
        if(strtotime($dateTime) >= strtotime($sign_in->calculated_min_sign_out_time) &&  strtotime($dateTime) <= strtotime($end_of_day_dateTime)){

            // This user is allowed to have overtime
            if($profile->overtime_permissible == 1){
                $overtime = Helpers::subtractDates($dateTime,$sign_in->calculated_min_sign_out_time);

                $dont_calculate_overtime_before = Helpers::addIntervalToDate($sign_in->calculated_min_sign_out_time,$profile->min_overtime_cuttoff,"Add");

                // if the user exceeded the start time of starting calculating overtime
                if(strtotime($dateTime) >= strtotime($dont_calculate_overtime_before)){

                    $overtime_has_limit = $profile->overtime_limit;
                    // if there is a limit to the overtime
                    if($overtime_has_limit == 1){

                        $daily_limit_in_seconds = Helpers::timeToDateTimeWithDate("1989-06-18",$profile->max_overtime_per_day);

                        $over_time_in_seconds = Helpers::timeToDateTimeWithDate("1989-06-18",$overtime);

                        if(strtotime($daily_limit_in_seconds) < strtotime($over_time_in_seconds)){
                            $overtime = Helpers::overtimeSecondsToTime($daily_limit_in_seconds);

                        }  

                        $over_time_in_seconds = Helpers::timeToSeconds($overtime);

                        $remaining_monthly_overtime = Helpers::getRemainingOvertime($user_id,date("Y-m-d",strtotime($sign_in->date)));
                        $remaining_monthly_overtime_in_seconds = Helpers::timeToSeconds($remaining_monthly_overtime);

                        if(strtotime($over_time_in_seconds) > strtotime($remaining_monthly_overtime_in_seconds)){
                            $overtime = Helpers::overtimeSecondsToTime($remaining_monthly_overtime_in_seconds);
                        } 
                    }
                    $overtime_interval = $overtime;
                    $overtime = new Req();
                    $overtime->sign_in_id = $sign_in->id;
                    $overtime->req_day = $sign_in->date;
                    $overtime->emp_id = $user_id;
                    $overtime->interval_in_time = $overtime_interval;
                    $overtime->type = "Overtime";
                    $overtime->save();
                    Helpers::sendReqEmail($overtime);
                    
                }
            } 
            
        }
        if(  strtotime($dateTime) > strtotime($end_of_day_dateTime)){
            $data = [];
        $data['status'] = 'error';
        $data['page'] = 'none';
        $data['confirm'] = 0;
        //$data['confirm_msg'] = "You are signing out before the assumed end of you workday, Are you sure you want to continue?";
        //$data['route'] = "/admin/sign_in_out/revertSignOut";
        $data['msg'] = "You can not sign out outside the permissible workday";
        return $data;
        
        }
        $sign_in->sign_out_time = $dateTime;
        $sign_in->attended = $attended;
        $sign_in->save();

        $data = [];
        $data['status'] = 'success';
        $data['page'] = 'none';
        $data['confirm'] = $confirm;
        $data['confirm_msg'] = "You are signing out before the assumed end of you workday, Are you sure you want to continue?";
        $data['route'] = "/admin/sign_in_out/revertSignOut";
        $data['msg'] = "Done";
        return $data;
        


    }

    public static function overRideSignInOut($sign_in_id,$sign_in_time,$sign_out_time,$home="",$day_type=""){
        $sign_in = Sign_in_out::find($sign_in_id);
        Helpers::rollbackSignInOut($sign_in_id);
        SignInOut::sign_in($sign_in_time,$sign_in->emp_id,$home,$day_type);
        
        if($sign_out_time!= Null){
            SignInOut::sign_out($sign_out_time,$sign_in->emp_id);
        }

        return true;

        

    }

}
