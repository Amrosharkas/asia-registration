<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'userController@index')->name('index');



Route::group(['middleware' => ['auth'], 'prefix' => "admin/clients", 'as' => "admin.clients."], function () {
    Route::get('/', 'clientController@index')->name('index');
    Route::delete('{id}/delete', 'clientController@delete')->name('delete')->middleware('can:super_user');
    Route::get('{id}/edit', 'clientController@edit')->name('edit');
    Route::get('{id}/profile', 'clientController@profile')->name('profile');
    Route::get('init', 'clientController@init')->name('init');
    Route::post('save', 'clientController@save')->name('save');

});

Route::group(['middleware' => ['auth','can:super_user'], 'prefix' => "admin/employees", 'as' => "admin.employees."], function () {
    Route::get('/', 'employeeController@index')->name('index');
    Route::delete('{id}/delete', 'employeeController@delete')->name('delete');
    Route::get('{id}/edit', 'employeeController@edit')->name('edit');
    Route::get('{id}/profile', 'employeeController@profile')->name('profile');
    Route::get('init', 'employeeController@init')->name('init');
    Route::post('save', 'employeeController@save')->name('save');

});
Route::group(['middleware' => ['auth','can:super_user'], 'prefix' => "admin/coins", 'as' => "admin.coins."], function () {
    Route::get('/', 'coinController@index')->name('index');
    Route::delete('{id}/delete', 'coinController@delete')->name('delete');
    Route::get('{id}/edit', 'coinController@edit')->name('edit');
    Route::get('init', 'coinController@init')->name('init');
    Route::post('save', 'coinController@save')->name('save');

});

Route::group(['middleware' => ['auth'], 'prefix' => "admin/instructors", 'as' => "admin.instructors."], function () {
    Route::get('/', 'instructorController@index')->name('index');
    Route::delete('{id}/delete', 'instructorController@delete')->name('delete');
    Route::delete('{id}/delete_document', 'instructorController@deleteDocument')->name('delete_document');
    Route::get('{id}/edit', 'instructorController@edit')->name('edit');
    Route::get('init', 'instructorController@init')->name('init');
    Route::get('init_document', 'instructorController@init_document')->name('init_document');
    Route::post('save', 'instructorController@save')->name('save');

});

Route::group(['middleware' => ['auth'], 'prefix' => "admin/courses", 'as' => "admin.courses."], function () {
    Route::get('/', 'courseController@index')->name('index');
    Route::delete('{id}/delete', 'courseController@delete')->name('delete');
    Route::get('{id}/edit', 'courseController@edit')->name('edit');
    Route::get('init', 'courseController@init')->name('init');
    Route::post('save', 'courseController@save')->name('save');

});

Route::group(['middleware' => ['auth'], 'prefix' => "admin/places", 'as' => "admin.places."], function () {
    Route::get('/', 'placeController@index')->name('index');
    Route::delete('{id}/delete', 'placeController@delete')->name('delete');
    Route::get('{id}/edit', 'placeController@edit')->name('edit');
    Route::get('init', 'placeController@init')->name('init');
    Route::post('save', 'placeController@save')->name('save');

});

Route::group(['middleware' => ['auth'], 'prefix' => "admin/schedules", 'as' => "admin.schedules."], function () {
    Route::get('/', 'scheduleController@index')->name('index');
    Route::post('/recommend', 'scheduleController@recommend')->name('recommend');
    Route::delete('{id}/delete', 'scheduleController@delete')->name('delete');
    Route::delete('schedule_day/{id}/delete', 'scheduleController@deleteDay')->name('delete');
    
    Route::get('{id}/edit', 'scheduleController@edit')->name('edit');
    Route::get('init', 'scheduleController@init')->name('init');
    Route::post('save', 'scheduleController@save')->name('save');
    Route::get('/{id}/manage', 'scheduleController@manage')->name('manage');
    Route::post('update_interested_status', 'scheduleController@update_interested_status')->name('update_interested_status');
    Route::post('add_payment', 'scheduleController@add_payment')->name('add_payment');
    Route::get('/{schedule_id}/{user_id}/payment_history', 'scheduleController@payment_history')->name('payment_history');
    Route::get('/{user_id}/cancel', 'scheduleController@cancel')->name('cancel')->middleware('can:super_user');
    Route::get('/{schedule_id}/end_transactions', 'scheduleController@endTransactions')->name('end_transactions')->middleware('can:super_user');
    Route::get('/{schedule_id}/reopen_transactions', 'scheduleController@reOpenTransactions')->name('reopen_transactions')->middleware('can:super_user');
    
    
    

});


Route::group(['middleware' => ['auth'], 'prefix' => "admin/consultations", 'as' => "admin.consultations."], function () {
    Route::get('/', 'consultationController@index')->name('index');
    Route::delete('{id}/delete', 'consultationController@delete')->name('delete');
    Route::get('{id}/edit', 'consultationController@edit')->name('edit');
    Route::get('pricing', 'consultationController@pricing')->name('pricing')->middleware('can:super_user');
    Route::post('init', 'consultationController@init')->name('init');
    Route::post('save', 'consultationController@save')->name('save');
    Route::post('save_pricing', 'consultationController@save_pricing')->name('save_pricing')->middleware('can:super_user');
    Route::post('save_audio', 'consultationController@save_audio')->name('save_audio');
    Route::post('add_payment', 'consultationController@add_payment')->name('add_payment');
    Route::post('add_reply', 'consultationController@add_reply')->name('add_reply');
    Route::get('/{consultation_id}/end_transactions', 'consultationController@endTransactions')->name('end_transactions')->middleware('can:super_user');
    Route::get('/{consultation_id}/reopen_transactions', 'consultationController@reOpenTransactions')->name('reopen_transactions')->middleware('can:super_user');

});

Route::group(['middleware' => ['auth'], 'prefix' => "admin/expenses", 'as' => "admin.expenses."], function () {
    Route::get('/', 'expenseController@index')->name('index');
    Route::delete('{id}/delete', 'expenseController@delete')->name('delete');
    Route::get('{id}/edit', 'expenseController@edit')->name('edit');
    Route::get('init', 'expenseController@init')->name('init');
    Route::post('save', 'expenseController@save')->name('save');

});
Route::group(['middleware' => ['auth'], 'prefix' => "admin/reports", 'as' => "admin.reports."], function () {
    Route::get('/', 'reportController@index')->name('index');
    

});
Route::group(['middleware' => ['auth'], 'prefix' => "admin/transactions", 'as' => "admin.transactions."], function () {
    Route::delete('{id}/delete', 'transactionController@delete')->name('delete');
    
    Route::get('/{stuff_id}/{stuff_identifier}/{user_id}/payment_history', 'transactionController@payment_history')->name('payment_history');
    
    

});
Route::group( ['middleware' => ['auth'],'prefix' => "files", 'as' => "files."], function () {
                    Route::get('/add', 'memoryController@add')->name('add');
                    Route::post('/endpoint', 'fileController@handle')->name('endpoint');
                    Route::post('/save_file', 'fileController@saveFile')->name('save_file');
                    Route::delete('/endpoint/{file}', 'fileController@handle')->name('delete_endpoint');
                    Route::post('/get_file_info', 'fileController@getFileInfo')->name('get_file_info');
                    
    
//--routes@@files

});
Route::auth();

