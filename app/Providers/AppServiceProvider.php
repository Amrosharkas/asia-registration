<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\User;
use View;
use Session;
use App\Course;
use Auth;
use DateTimeZone;
use DateTime;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        date_default_timezone_set('Asia/Dubai');
        
        
        
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {


    }
}
