<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Schedule extends Model
{
	protected $guarded = [];

    public function getCourse(){
        
        return $this->belongsTo('App\Course', 'course_id', 'id');

    }
    public function getPlace(){
        
        return $this->belongsTo('App\Place', 'place_id', 'id');

    }
}
