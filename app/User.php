<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Company;
use App\Http\Requests;
class User extends Authenticatable
{
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function getFather(){
        
        return $this->belongsTo('App\User', 'father_id', 'id');

    }
    public function getMother(){
        
        return $this->belongsTo('App\User', 'mother_id', 'id');

    }
    public function getProfile(){
        
        return $this->belongsTo('App\Att_profile', 'att_profile_id', 'id');

    }
    public function getWorkHolidayProfile(){
        
        return $this->belongsTo('App\Att_profile', 'work_on_holiday_profile_id', 'id');

    }
    public function getFirstHalfProfile(){
        
        return $this->belongsTo('App\Att_profile', 'first_half_day_profile_id', 'id');

    }
    public function getSecondHalfProfile(){
        
        return $this->belongsTo('App\Att_profile', 'second_half_day_profile_id', 'id');

    }
    

    public function getOffice(){
        
        return $this->belongsTo('App\Office', 'office_id', 'id');

    }
    public function getRole(){
        
        return $this->belongsTo('App\Employee_role', 'role_id', 'id');

    }
    public function getPosition(){
        
        return $this->belongsTo('App\Position', 'position_id', 'id');

    }
    public function getDepartment(){
        
        return $this->belongsTo('App\Department', 'department_id', 'id');

    }
    public function getManager(){
        
        return $this->belongsTo('App\User', 'manager_id', 'id');

    }

    public function getCopiedManagers(){
        return $this->belongsToMany('App\User', 'managers_employees','employee_id','manager_id');    
    }

    

    

   
}
