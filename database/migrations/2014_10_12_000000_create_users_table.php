<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('password')->nullable();
            $table->string('dec_password')->nullable();
            $table->integer('admin_show')->default(0);
            $table->string('phone')->nullable();
            $table->string('email')->nullable();
            $table->string('gender')->nullable();
            $table->date('date_of_birth')->nullable();
            $table->string('fax')->nullable();
            $table->integer('mother_id')->nullable();
            $table->integer('father_id')->nullable();
            $table->integer('refered_by')->nullable();
            $table->float('points_balance')->nullable();
            $table->float('cash_balance')->nullable();
            $table->date('joining_date')->nullable();
            
            $table->integer('role_id')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });

        Schema::create('user_roles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->timestamps();
        });

        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
        Schema::drop('user_roles');
        
    }
}
