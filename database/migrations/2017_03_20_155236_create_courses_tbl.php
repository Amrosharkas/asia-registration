<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoursesTbl extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('courses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('description')->nullable();
            $table->integer('admin_show')->default(0);
            $table->timestamps();
        });

        Schema::create('schedules', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('course_id')->nullable();
            $table->date('start_date')->nullable();
            $table->float('price_coins')->nullable();
            $table->float('price_cash')->nullable();
            $table->float('disc_price_coins')->nullable();
            $table->float('disc_price_cash')->nullable();
            $table->integer('disc_cuttoff_time')->nullable();
            $table->string('instructor_id')->nullable();
            $table->float('instructor_payment')->nullable();// cash
            $table->integer('place_id')->nullable();// cash
            $table->integer('admin_show')->default(0);
            // Cancelation policy
            $table->integer('canc_100_cuttoff_time')->nullable();
            $table->integer('canc_50_cuttoff_time')->nullable();
            
            // Point rewards
            $table->float('points_gained')->nullable();
            $table->float('referral_points_gained')->nullable();

            $table->timestamps();


        });

        Schema::create('schedule_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('course_id')->nullable();
            $table->integer('schedule_id')->nullable();
            $table->datetime('from')->nullable();
            $table->datetime('to')->nullable();
            $table->integer('admin_show')->default(0);
            $table->timestamps();
        });

        Schema::create('users_schedules', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('course_id')->nullable();
            $table->integer('schedule_id')->nullable();
            $table->integer('user_id')->nullable();
            $table->string('interest_status')->nullable();
            $table->float('price')->nullable();
            $table->float('payment_percentage')->nullable();
            $table->string('payment_status')->nullable();
            $table->string('attended')->default(0);
            $table->integer('has_notice')->default(0);
            $table->string('comment')->nullable();
            $table->integer('admin_show')->default(0);
            $table->timestamps();
        });
        Schema::create('places', function (Blueprint $table) {
            $table->increments('id');
            $table->string('country')->nullable();
            $table->string('city')->nullable();
            $table->string('name')->nullable();
            $table->integer('admin_show')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('courses');
        Schema::drop('schedules');
        Schema::drop('schedule_details');
        Schema::drop('users_schedules');
        Schema::drop('places');
    }
}
