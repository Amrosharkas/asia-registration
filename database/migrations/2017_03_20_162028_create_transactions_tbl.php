<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionsTbl extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->nullable();
            $table->integer('course_id')->nullable();
            $table->integer('schedule_id')->nullable();
            $table->integer('consultation_id')->nullable();
            $table->float('amount')->nullable();
            $table->string('type')->nullable();
            $table->date('transaction_date')->nullable();
            $table->timestamps();
        });
        Schema::create('coins', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->nullable();
            $table->integer('schedule_id')->nullable();
            $table->integer('consultation_id')->nullable();
            $table->float('amount')->nullable();
            $table->string('date')->nullable();
            $table->string('type')->nullable(); // Retained , Expirable
            $table->integer('expire_after')->nullable(); // days
            $table->integer('admin_show')->default(1);
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('transactions');
        Schema::drop('coins');
    }
}
