<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConsultationsTbl extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('consultations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('client_id')->nullable();
            $table->datetime('start_time')->nullable();
            $table->datetime('end_time')->nullable();
            $table->time('duration')->nullable();
            $table->text('comment')->nullable();
            $table->text('attachment')->nullable();
            $table->text('audio_file')->nullable();
            $table->date('next_contact')->nullable();
            $table->text('call_reply')->nullable();
            $table->datetime('reply_date')->nullable();
            $table->float('price')->nullable();
            $table->float('payment')->nullable();
            $table->float('payment_percentage')->nullable();
            $table->string('status')->nullable(); // before start, in process, Finished
            $table->integer('mail_sent')->default(0);
            $table->integer('admin_show')->default(0);
            $table->timestamps();
        });

        Schema::create('consultation_pricings', function (Blueprint $table) {
            $table->increments('id');
            $table->float('pricing_45')->default(0);
            $table->float('pricing_1hour')->default(0);
            $table->float('points_gained')->default(0);
            $table->float('referral_points_gained')->default(0);
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('consultations');
        Schema::drop('consultation_pricings');
    }
}
