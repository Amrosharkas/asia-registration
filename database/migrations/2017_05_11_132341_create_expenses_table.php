<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExpensesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('expenses', function (Blueprint $table) {
            $table->increments('id');
            $table->float('amount')->nullable();
            $table->date('expense_date')->nullable();
            $table->integer('asset')->nullable();
            $table->string('type')->nullable();
            $table->integer('trainer_id')->nullable();
            $table->integer('schedule_id')->nullable();
            $table->string('schedule_expense_type')->nullable();
            $table->text('description')->nullable();
            $table->integer('admin_show')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('expenses');
    }
}
