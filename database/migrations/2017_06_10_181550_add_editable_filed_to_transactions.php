<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEditableFiledToTransactions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('transactions', function ($table) {
            $table->integer('editable')->default(1);
        });
        Schema::table('schedules', function ($table) {
            $table->integer('closed')->default(0);
        });
        Schema::table('consultations', function ($table) {
            $table->integer('closed')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transactions', function($table) {
            $table->dropColumn('editable');
        });
        Schema::table('schedules', function($table) {
            $table->dropColumn('closed');
        });
        Schema::table('consultations', function($table) {
            $table->dropColumn('closed');
        });
    }
}
