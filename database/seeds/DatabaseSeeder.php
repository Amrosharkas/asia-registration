<?php

use Illuminate\Database\Seeder;
use App\User;
use App\User_role;
use App\Consultation_pricing;

use Faker\Factory as Faker;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Some fake Clients
        $faker = Faker::create();
        foreach (range(1,20) as $index) {
            DB::table('users')->insert([
                'name' => $faker->name,
                'email' => $faker->email,
                'phone' => $faker->phoneNumber,
                'password' => bcrypt('123456'),
                'role_id' => 4,
                'gender' => 'Male',
                'admin_show' => 1
            ]);
        }
        

        $pricing = new Consultation_pricing();
        $pricing->save();
        // Owner
        $user = new User();
        $user->name = 'Heba Sharkas';
        $user->email = 'heba@asiacademy.net';
        $user->phone = '00971557074216';
        $user->gender = 'Female';
        $user->date_of_birth = '1975-11-26';
        $user->password = bcrypt('hebaPass123');
        $user->role_id = 1;
        $user->admin_show =1;
        $user->save();

        // Registration Manager
        $user = new User();
        $user->name = 'Ahmed Fathi';
        $user->email = 'Ahmed@asiacademy.net';
        $user->phone = '';
        $user->gender = 'Male';
        $user->date_of_birth = '';
        $user->password = bcrypt('ahmedPass123');
        $user->role_id = 2;
        $user->admin_show =1;
        $user->save();

        // Registration Manager
        $user = new User();
        $user->name = 'Maisam';
        $user->email = 'Maisam@asiacademy.net';
        $user->phone = '';
        $user->gender = 'Male';
        $user->date_of_birth = '';
        $user->password = bcrypt('maisamPass123');
        $user->role_id = 2;
        $user->admin_show =1;
        $user->save();
        // Create Roles
        $role = new User_role();
        $role->name = "Owner";
        $role->save();
        $role = new User_role();
        $role->name = "Registration Manager";
        $role->save();
        $role = new User_role();
        $role->name = "Instructor";
        $role->save();
        $role = new User_role();
        $role->name = "Client";
        $role->save();


        
        
        
    }
}
