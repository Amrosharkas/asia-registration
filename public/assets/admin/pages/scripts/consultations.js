$(document).on('ready pjax:success', function () {
    
   
    $("select[name='in_status']").on('change', function() {
        var current_status =  this.value ;
        var schedule_id = $(this).closest("tr").attr("data-id");

        appBlockUI();
        $.ajax(
            {
                url: "/admin/schedules/update_interested_status", 
                method:"post",
                data:{
                    id:schedule_id,
                    status:current_status
                },
                success: function(result){

                    appUnBlockUI();
                }});


    })

    $(document).on('click', '.add_payment', function () {
        $info_div = $(this).closest('.payment_info');
        $payment_form = $info_div.next('form');
        $info_div.hide();
        $payment_form.show();


    });
    $(document).on('click', '.cancel_payment', function () {
        $payment_form = $(this).closest('form');
        $info_div = $payment_form.prev('div');

        $info_div.show();
        $payment_form.hide();


    });
    
    $('.call_reply').bind("enterKey",function(e){
        $(this).closest("form").submit();
    });
    $('.call_reply').keyup(function(e){
        if(e.keyCode == 13)
        {
            $(this).trigger("enterKey");
        }
    });
});

