$(document).on('ready pjax:success', function () {

    $filepickers = $("#files .filepicker");
    $filepickers.each(function(){
        var user_id = $("input.user_id").val();
        $file = $(this).closest(".file");
        data = $file.attr("data-id");
        runUploader("filePicker_"+$file.attr("data-id"),false,5000000,0,0,"Document","user_id",user_id,data);
        
    });

    
    
   
    $("select[name='speciality']").on('change', function() {
        var type =  this.value ;
        if(type == "New Speciality"){
            $(this).hide(100);
            $(".new_spec").show(100);
            $(".new_spec").focus();
           
           }

    });
    
    
    

    
});
$(document).on('click', '#files .btn-sm', function () {
        var id = $(this).closest('.file').attr('data-id');
        var $file = $(this).closest('.file');
        swal({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes!',
        cancelButtonText: 'No, cancel!',
        confirmButtonClass: 'btn btn-danger',
        cancelButtonClass: 'btn btn-info',
        buttonsStyling: false
    }).then(function () {
        
        
            $.ajax({
                method: 'delete',
                url: '/admin/instructors/' + id + '/delete_document'
            });

         
        $file.remove();

    }, function (dismiss) {
        // dismiss can be 'cancel', 'overlay',
        // 'close', and 'timer'

    })
        

    });
$(document).on('click', '.add_new_file', function () {
        $.ajax({
            method: 'get',
            url: '/admin/instructors/init_document',
            success: function (data) {
                var user_id = $("input.user_id").val();
                var $file = $('.clonable').clone();
                $file.attr("data-id",data);
                console.log($file);
                $count = parseInt($('#files .file').length);
                $count = $count + 1;
                $file.removeClass('clonable');


                $file.find('.caption').html('<i class="fa fa-gift"></i>File ' + $count);
                $('#files').append($file);
                ComponentsDateTimePickers.init();
                

                $(".datePicker").datepicker({
                    dateFormat: 'yy-mm-dd',
                    prevText: '<i class="fa fa-caret-left"></i>',
                    nextText: '<i class="fa fa-caret-right"></i>'

                });

                $('.datePicker,.timepicker,.dateTimePicker').keypress(function (event) {
                    event.preventDefault();
                    return false;
                });
                //file uploader 

                $file.find(".filepicker").attr("id","filePicker_"+data);
                $file.find(".document_id").val(data);
                runUploader("filePicker_"+data,false,5000000,0,0,"Document","user_id",user_id,data);
                
            }
        });

        


    });

