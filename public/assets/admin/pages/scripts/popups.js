$(document).ready(function () {
    $('.popup').magnificPopup({
        type: 'iframe',
        callbacks: {
            open: function () {
                // Will fire when this exact popup is opened
                // this - is Magnific Popup object
            },
            close: function () {
                    var url = document.URL;
                    var goTo = url.replace("#", "");
                    pjaxPage(goTo);
                    
                }
                // e.t.c.
        }

    });
});

$(document).on('ready pjax:success', function () {
    $('.popup').magnificPopup({
        type: 'iframe',
        callbacks: {
            open: function () {
                // Will fire when this exact popup is opened
                // this - is Magnific Popup object
            },
            close: function () {
                    var url = document.URL;
                    var goTo = url.replace("#", ""); 
                    pjaxPage(goTo);
                }
                // e.t.c.
        }

    });
});
