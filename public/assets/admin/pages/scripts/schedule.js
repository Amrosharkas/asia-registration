
$(document).on('ready pjax:success', function () {
    $(document).on('click', '.add_new_day', function () {
        var $day = $('.clonable').clone();
        console.log($day);
        $count = parseInt($('#days .day').length);
        $count = $count + 1;
        $day.removeClass('clonable');

        $day.find('.caption').html('<i class="fa fa-gift"></i>Day ' + $count);
        $('#days').append($day);
        ComponentsDateTimePickers.init();
        $('.dateTimePicker').datetimepicker({
            format: 'yy-mm-dd hh:ii',
            weekStart: 1,
            todayBtn: 0,
            autoclose: 1,
            todayHighlight: 0,
            startView: 1,
            minView: 0,
            maxView: 1,
            forceParse: 0,
            showMeridian: true
        });

        $(".datePicker").datepicker({
            dateFormat: 'yy-mm-dd',
            prevText: '<i class="fa fa-caret-left"></i>',
            nextText: '<i class="fa fa-caret-right"></i>'

        });

        $('.datePicker,.timepicker,.dateTimePicker').keypress(function (event) {
            event.preventDefault();
            return false;
        });


    });
    
    $(document).on('click', '#days .btn-sm', function () {
        $id = $(this).closest('.day').attr('data-id');
        if (isNaN) {
            $.ajax({
                method: 'delete',
                url: '/admin/schedules/schedule_day/' + $id + '/delete'
            });

        } 
        $(this).closest('.day').remove();

    });

    $("select[name='in_status']").on('change', function() {
        var current_status =  this.value ;
        var schedule_id = $(this).closest("tr").attr("data-id");

        appBlockUI();
        $.ajax(
            {
                url: "/admin/schedules/update_interested_status", 
                method:"post",
                data:{
                    id:schedule_id,
                    status:current_status
                },
                success: function(result){

                    appUnBlockUI();
                }});


    })

    $(document).on('click', '.add_payment', function () {
        $info_div = $(this).closest('.payment_info');
        $payment_form = $info_div.next('form');
        $info_div.hide();
        $payment_form.show();


    });
    $(document).on('click', '.cancel_payment', function () {
        $payment_form = $(this).closest('form');
        $info_div = $payment_form.prev('div');

        $info_div.show();
        $payment_form.hide();


    });
});

