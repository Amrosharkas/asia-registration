// prepare the form when the DOM is ready 
$(document).on('ready pjax:success', function () {


    //Hidden elements Dependencies
    $dependent_emements = $(".dependent");


    $dependent_emements.each(function () {
        var depending_on_name = $(this).attr("data-depending-on");
        var depending_on_value = $(this).attr("data-depending-value");
        var depending_on_values = depending_on_value.split(",");
        

        $depending_on_element = $("[name=" + depending_on_name + "]");
        $depending_on_element.addClass("master_dependency");
        var curr_val =$depending_on_element.val();
        
        var check = depending_on_values.indexOf(curr_val);
        
        if (check < 0) {

            $(this).find("input,select").val("");
            $(this).find("input,select").change();
            $(this).find("input,select").hide();
            $(this).hide(250);

        } else {
            //$(this).find(".timepicker").val("Choose Time");

            $(this).show(250);
        }

    });

    $(".master_dependency").change(function () {
        var element_name = $(this).attr("name");
        var element_value = $(this).val();

        $dependent_element = $('*[data-depending-on="' + element_name + '"]');
        $dependent_element.each(function () {
            var showValue = $(this).attr("data-depending-value");
            var showValues = showValue.split(",");
            var check = showValues.indexOf(element_value);
            
            if (check >= 0) {
                $(this).find("input,select").show();
                $(this).show(250);
                $(this).find(".timepicker").val("Choose Time");
            } else {
                $(this).hide(250);
                $(this).find("input,select").val("");
                $(this).find("input,select").hide();
                $(this).find("input,select").change();
            }
        });
    });

    // Hidden options dependencies

    $dependent_emements = $(".options_dependent");


    $dependent_emements.each(function () {
        var depending_on_name = $(this).attr("data-depending-on");
        //var depending_on_value = $(this).attr("data-depending-value");

        $depending_on_element = $("[name=" + depending_on_name + "]");
        $depending_on_element.addClass("master_options_dependency");
        var masterValue = $depending_on_element.val();
        $dependent_emements.find("option").each(function () {
            var iteratingOkVal = $(this).attr("data-depending-value");
            var selectedValue = $(this).parent('select').val();
            if (iteratingOkVal == masterValue || iteratingOkVal == "force_show" || $(this).attr("value") == "") {
                $(this).show();
                if ($(this).parent('select').hasClass("select2")) {
                    $(this).removeAttr("disabled");
                    $(this).parent('select').select2();
                }
            } else {
                $(this).hide();
                if ($(this).parent('select').hasClass("select2")) {
                    $(this).attr("disabled", "disabled");
                    $(this).parent('select').select2();

                }
                if (selectedValue == $(this).attr("value")) {
                    $(this).parent('select').val("");
                }
            }
        });


    });

    $(".master_options_dependency").change(function () {
        var element_name = $(this).attr("name");
        var element_value = $(this).val();

        $dependent_element = $('*[data-depending-on="' + element_name + '"]');
        $dependent_element.find("option").each(function () {

            var iteratingOkVal = $(this).attr("data-depending-value");

            if (iteratingOkVal == element_value || iteratingOkVal == "force_show" || $(this).attr("value") == "") {


                $(this).show();
                if ($(this).parent('select').hasClass("select2")) {
                    $(this).removeAttr("disabled");
                    $(this).parent('select').select2();
                }
            } else {
                $(this).hide();

                if ($(this).parent('select').hasClass("select2")) {
                    $(this).attr("disabled", "disabled");
                    $(this).parent('select').select2("val", "");
                    $(this).parent('select').select2();

                }
                $(this).parent('select').val("");

            }
        });




    });

    // hide error on changing
    $("input,select").change(function () {
        $(this).removeClass("errorValidation");
        $(this).next(".error_message").hide();
    });
    $(".select2").select2({
        placeholder: "",
        allowClear: true
    });
    var options = {

        beforeSubmit: beforeSubmit, // pre-submit callback 
        success: success, // post-submit callback 
        error: error


    };
    // bind form using 'ajaxForm' 
    $('.ajax_form').ajaxForm(options);

});

// pre-submit callback 
function beforeSubmit(formData, jqForm, options) {
    appBlockUI();
    // formData is an array; here we use $.param to convert it to a string to display it 
    // but the form plugin does this for you automatically when it submits the data 
    var queryString = $.param(formData);


    var formElement = jqForm[0];
    var pass = 1;
    $(".error_message").hide();
    for (var i = 0; i < formData.length; i++) {
        var elementName = formData[i].name.replace("[]", "");
        if (formData[i].type == "select-one") {
            $formElement = $('select[name=' + elementName + ']');
        } else {

            $formElement = $('input[name=' + elementName + ']');
        }

        $formElement.removeClass("errorValidation");

        validationRules = $formElement.attr('data-validation');
        validationName = $formElement.attr('data-name');
        var check_dependency = true;
        if ($formElement.closest('.dependent').length != 0) {
            if ($formElement.css('display').toLowerCase() == 'none') {
                check_dependency = false;
            }
        }
        if (typeof validationRules !== 'undefined' && check_dependency) {
            if (validationRules.includes("match")) {

                var split = validationRules.split(".");
                var field = split[1];
                var fieldValue = $('input[name=' + field + ']').val();
                if (formData[i].value !== fieldValue) {
                    appUnBlockUI();
                    //var $toast = toastr["error"]("Error", validationName+" is required");  
                    $formElement.addClass("errorValidation");
                    $formElement.next(".error_message").show().html(field + "s don't match");
                    pass = 0;
                }
            }
            if (formData[i].value == "Choose Time" && $formElement.hasClass("timepicker")) {
                appUnBlockUI();
                //var $toast = toastr["error"]("Error", " Invalid Email");
                $formElement.addClass("errorValidation");
                $formElement.next(".error_message").show().html("Invalid Time");
                pass = 0;
            }

            if (!validateEmail(formData[i].value) && validationRules.includes("email")) {
                appUnBlockUI();
                //var $toast = toastr["error"]("Error", " Invalid Email");
                $formElement.addClass("errorValidation");
                $formElement.next(".error_message").show().html("Invalid Email");
                pass = 0;
            }

            if (isNaN(formData[i].value) && validationRules.includes("number")) {
                appUnBlockUI();
                //var $toast = toastr["error"]("Error", validationName+" must be a number");
                $formElement.addClass("errorValidation");
                $formElement.next(".error_message").show().html(validationName + " must be a number");
                pass = 0;
            }
            if (formData[i].value == "" && validationRules.includes("required")) {
                appUnBlockUI();
                //var $toast = toastr["error"]("Error", validationName+" is required");  
                $formElement.addClass("errorValidation");
                $formElement.next(".error_message").show().html(validationName + " is required");
                pass = 0;
            }


        }
    }
    if (pass == 1) {
        return true;
    } else {

        return false;
    }
}

// post-submit callback 
function success(responseText, statusText, xhr, $form) {
    appUnBlockUI();
    var response = responseText;
    if (response.status == "error") {
        var $toast = toastr["error"](response.msg, "Sorry");
    } else {
        var $toast = toastr["success"](response.msg, "Success");
        if (response.page == "none") {
            $(".resetForm").click();
        } else {
           // pjaxPage(response.page);
        }
    }
}

function error(responseText, statusText, xhr, $form) {
    appUnBlockUI();
    var $toast = toastr["error"](responseText, statusText);

}


function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}
