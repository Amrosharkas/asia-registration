var table = null;

function pjaxPage(url) {
    var link = document.getElementById('pjax-goto-link');
    link.href = url;
    link.click();
}

function scrollToTop() {
    var aTag = $('#page-top');
    $('html,body').animate({
        scrollTop: aTag.offset().top
    }, 'slow');
}
// Blocking UI
function appBlockUI() {
    $.blockUI({
        message: '<h1><img src="/assets/global/img/ring-alt.svg" /> </h1>',
        overlayCSS: {
            backgroundColor: '#000',
            bgOpacity: 0.6,
            opacity: 0.8
        },
        css: {
            border: 'none',
            padding: '0',
            backgroundColor: 'none',

        }
    });
}
// unblocking UI
function appUnBlockUI() {
    $.unblockUI();
}

function customFileInput($elements) {
    $elements.each(function (index, el) {
        var data = {
            id: $(el).closest('.level').find('input[name=id]').val()
        };
        var url = $(el).data('delete');
        $(el).fileinput({
            uploadUrl: $(el).data('action'),
            showPreview: false,
            uploadExtraData: function (previewId, index) {
                return data;
            },
            allowedPreviewTypes: ['image'],
            initialPreviewConfig: [
                {
                    caption: 'desert.jpg',
                    width: '120px',
                    url: url, // server delete action 
                    key: 100,
                    extra: {
                        id: 100
                    }
                }
            ]
        });
        $(el).on('fileclear', function () {
            console.log("fileclear");
        });
        $(el).on('fileloaded', function (event, file, previewId, index, reader) {
            console.log("fileloaded");
        });
        $(el).on('filedeleted', function (event, key) {
            console.log('Key = ' + key);
        });

    });
}
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
$(document).on('ready pjax:success', function () {
    




    $('.datePicker,.timepicker,.dateTimePicker').keypress(function (event) {
        event.preventDefault();
        return false;
    });
    ComponentsDateTimePickers.init();
    $('.editable-select').editableSelect();

    $('.dateTimePicker').datetimepicker({
        format: 'yy-mm-dd hh:ii',
        weekStart: 1,
        todayBtn: 0,
        autoclose: 1,
        todayHighlight: 0,
        startView: 1,
        minView: 0,
        maxView: 1,
        forceParse: 0,
        showMeridian: true
    });

    $(".datePicker").datepicker({
        dateFormat: 'yy-mm-dd',
        prevText: '<i class="fa fa-caret-left"></i>',
        nextText: '<i class="fa fa-caret-right"></i>'

    });
    $(".monthPicker").datepicker({
        format: "mm-yyyy",
        viewMode: "months",
        minViewMode: "months"

    });

    if ($('.table-dt').length) {
        table = $('.table-dt').DataTable({
            columnDefs: [
                {
                    targets: 'no-sorting',
                    sortable: false,
                    searchable: false,
                    orderable: false
                },
                {
                    targets: 'table-checkbox-col',
                    width: 10
                }
            ]
        });
    }
    $('.no-sorting').removeClass('sorting sorting_asc sorting_desc');
});
$(document).on('change', '.group-checkable', function () {
    if ($(this).is(':checked')) {
        $('.table-checkbox').prop('checked', true);
    } else {
        $('.table-checkbox').prop('checked', false);
    }
    $('.table-checkbox').each(function () {
        if ($(this).is(':checked')) {
            $(this).parents('tr').addClass("active to-remove");
        } else {
            $(this).parents('tr').removeClass("active to-remove");
        }
    });
    var formId = $(this).closest('form').attr('id');
    var $button = $('[data-form=' + formId + ']');
    if ($('.table-checkbox:checked').length) {
        $button.prop('disabled', false);
    } else {
        $button.prop('disabled', true);
    }
});
$(document).on('change', '.table-checkbox', function () {
    var formId = $(this).closest('form').attr('id');
    var $button = $('[data-form=' + formId + ']');
    if ($(this).is(':checked')) {
        $(this).parents('tr').addClass("active to-remove");
    } else {
        $(this).parents('tr').removeClass("active to-remove");
    }
    if ($('.table-checkbox:checked').length) {
        $button.prop('disabled', false);
    } else {
        $('.group-checkable').prop('checked', false);
        $button.prop('disabled', true);
    }
});
$(document).on('click', '.no-sorting', function () {
    $(this).removeClass('sorting sorting_asc sorting_desc');
});
$(document).on('click', '.delete_single', function () {
    var url = $(this).data('action');
    var $tr = $(this).closest('tr');

    swal({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes!',
        cancelButtonText: 'No, cancel!',
        confirmButtonClass: 'btn btn-danger',
        cancelButtonClass: 'btn btn-info',
        buttonsStyling: false
    }).then(function () {
        $.ajax({
            url: url,
            method: 'delete',
            success: function (data) {
                table.row($tr).remove().draw();
                swal(
                    'Deleted!',
                    'Done.',
                    'success'
                )
            },
            error: function (data) {
                swal(
                    'Sorry!',
                    'There was an error',
                    'error'
                )
            }
        });

    }, function (dismiss) {
        // dismiss can be 'cancel', 'overlay',
        // 'close', and 'timer'

    })

})

$(document).on('click', '.confirm_action', function (e) {
    e.preventDefault();
    var url = $(this).data('action');


    swal({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes!',
        cancelButtonText: 'No, cancel!',
        confirmButtonClass: 'btn btn-danger',
        cancelButtonClass: 'btn btn-info',
        buttonsStyling: false
    }).then(function () {
        $.ajax({
            url: url,
            method: 'get',
            success: function (data) {
                //table.row($tr).remove().draw();
                var url = document.URL;
                var goTo = url.replace("#", "");
                pjaxPage(goTo);
                swal(
                    'Success!',
                    'Done.',
                    'success'
                )
            },
            error: function (data) {
                swal(
                    'Sorry!',
                    'There was an error',
                    'error'
                )
            }
        });

    }, function (dismiss) {
        // dismiss can be 'cancel', 'overlay',
        // 'close', and 'timer'

    })

})


$(document).on('click', '.delete_multiple', function () {
    var $form = $('#' + $(this).data('form'));
    var formData = $form.serialize();
    var url = $form.attr('action');
    swal({
        title: "Are you sure?",
        text: "",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete it!",
        closeOnConfirm: false
    }, function () {
        $.ajax({
            url: url,
            method: 'delete',
            data: formData,
            success: function (data) {
                table.rows('tr.to-remove').remove().draw();
                $('.group-checkable').prop('checked', false);
                swal("Done!", data.message, "success");
                return false;
            },
            error: function (data) {
                $('tr.to-remove').removeClass('to-remove');
            }
        });
    });
})

$(document).on('click', '.sign_in_out', function () {
    appBlockUI();
    var url = $(this).attr('data-action');

    $.ajax({
        url: url,
        method: 'get',
        success: function (data) {
            refreshSignInOut();
            appUnBlockUI();

            var response = data;
            if (response.status == "error") {
                var $toast = toastr["error"](response.msg, "Sorry");
            } else {
                if (response.confirm == 1) {

                    swal({
                        title: "",
                        text: response.confirm_msg,
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Yes',
                        cancelButtonText: 'No',
                        allowOutsideClick: false,
                        allowEscapeKey: false,
                        confirmButtonColor: "#79a121",
                        cancelButtonColor: "#e7505a"
                    }).then(function () {

                    }, function (dismiss) {
                        $.ajax({
                            url: response.route,
                            method: 'get',
                            success: function (data) {
                                refreshSignInOut();

                            }
                        });
                    })
                } else {
                    var $toast = toastr["success"](response.msg, "Success");
                }

                if (response.page == "none") {

                } else {
                    //pjaxPage(response.page);
                }
            }
        },
        error: function (data) {
            var $toast = toastr["error"](response.msg, "Sorry");
        }
    });

})

function refreshSignInOut() {
    $.ajax({
        url: "/admin/sign_in_out/refreshSignInOut",
        method: 'get',
        success: function (data) {
            var response = data;
            $("." + response.show).show();
            $("." + response.hide).hide();
        }
    });

}
