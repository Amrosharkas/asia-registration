-- phpMyAdmin SQL Dump
-- version 4.6.6
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: May 11, 2017 at 10:36 AM
-- Server version: 5.6.35
-- PHP Version: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `asia-registration`
--

--
-- Dumping data for table `coins`
--

INSERT INTO `coins` (`id`, `user_id`, `schedule_id`, `consultation_id`, `amount`, `date`, `type`, `expire_after`, `created_at`, `updated_at`) VALUES
(1, 3, 1, NULL, 100.00, '08/02/2017', 'Retained', NULL, '2017-05-11 10:30:15', '2017-05-11 10:30:15'),
(2, 3, 1, NULL, 0.00, '06/01/2017', 'Retained', NULL, '2017-05-11 10:30:58', '2017-05-11 10:30:58');

--
-- Dumping data for table `consultations`
--

INSERT INTO `consultations` (`id`, `client_id`, `start_time`, `end_time`, `duration`, `comment`, `attachment`, `audio_file`, `next_contact`, `call_reply`, `reply_date`, `price`, `payment`, `payment_percentage`, `status`, `admin_show`, `created_at`, `updated_at`) VALUES
(1, 3, NULL, NULL, '46:00:00', '', NULL, NULL, '2017-05-31', NULL, NULL, 500.00, 500.00, 100.00, NULL, 0, '2017-05-11 10:30:26', '2017-05-11 10:30:58');

--
-- Dumping data for table `consultation_pricings`
--

INSERT INTO `consultation_pricings` (`id`, `pricing_45`, `pricing_1hour`, `points_gained`, `referral_points_gained`, `created_at`, `updated_at`) VALUES
(1, 400.00, 500.00, 0.00, 0.00, '2017-05-11 10:06:20', '2017-05-11 10:27:27');

--
-- Dumping data for table `courses`
--

INSERT INTO `courses` (`id`, `name`, `description`, `admin_show`, `created_at`, `updated_at`) VALUES
(1, 'Harmony elites', NULL, 1, '2017-05-11 10:28:20', '2017-05-11 10:28:23');

--
-- Dumping data for table `places`
--

INSERT INTO `places` (`id`, `country`, `city`, `name`, `admin_show`, `created_at`, `updated_at`) VALUES
(1, 'United Arab Emirates', 'Abu Zaby (Abu Dhabi)', 'Asia Academy', 1, '2017-05-11 10:28:07', '2017-05-11 10:28:17');

--
-- Dumping data for table `schedules`
--

INSERT INTO `schedules` (`id`, `course_id`, `start_date`, `price_coins`, `price_cash`, `disc_price_coins`, `disc_price_cash`, `disc_cuttoff_time`, `instructor_id`, `instructor_payment`, `place_id`, `admin_show`, `canc_100_cuttoff_time`, `canc_50_cuttoff_time`, `points_gained`, `referral_points_gained`, `created_at`, `updated_at`) VALUES
(1, 1, '2017-05-01', 0.00, 3000.00, 0.00, 2500.00, 7, 'isis hawas', NULL, 1, 1, 7, 4, 100.00, 150.00, '2017-05-11 10:28:25', '2017-05-11 10:29:14');

--
-- Dumping data for table `schedule_details`
--

INSERT INTO `schedule_details` (`id`, `course_id`, `schedule_id`, `from`, `to`, `admin_show`, `created_at`, `updated_at`) VALUES
(1, NULL, 1, '2017-05-01 15:00:00', '2017-05-01 17:00:00', 0, '2017-05-11 10:29:14', '2017-05-11 10:29:14');

--
-- Dumping data for table `transactions`
--

INSERT INTO `transactions` (`id`, `user_id`, `course_id`, `schedule_id`, `consultation_id`, `amount`, `type`, `transaction_date`, `created_at`, `updated_at`) VALUES
(1, 3, 1, 1, NULL, 100.00, 'Cash', '05/02/2017', '2017-05-11 10:29:38', '2017-05-11 10:29:38'),
(2, 3, 1, 1, NULL, 100.00, 'Cash', '06/01/2017', '2017-05-11 10:29:51', '2017-05-11 10:29:51'),
(3, 3, 1, 1, NULL, 1000.00, 'Cash', '07/03/2017', '2017-05-11 10:30:04', '2017-05-11 10:30:04'),
(4, 3, 1, 1, NULL, 1300.00, 'Cash', '08/02/2017', '2017-05-11 10:30:15', '2017-05-11 10:30:15'),
(5, 3, NULL, NULL, 1, 400.00, 'Cash', '05/01/2017', '2017-05-11 10:30:51', '2017-05-11 10:30:51'),
(6, 3, NULL, NULL, 1, 100.00, 'Cash', '06/01/2017', '2017-05-11 10:30:58', '2017-05-11 10:30:58');

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `password`, `dec_password`, `admin_show`, `phone`, `email`, `gender`, `date_of_birth`, `fax`, `mother_id`, `father_id`, `refered_by`, `points_balance`, `cash_balance`, `joining_date`, `role_id`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Heba Sharkas', '$2y$10$1F0.rJ3Eagx0iT.Jd26khOZrQmtpNGRWe8yeeeEg/NLwSFAN/lEpu', NULL, 1, '00971557074216', 'heba@asiacademy.net', 'Female', '1975-11-26', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, '2017-05-11 10:06:20', '2017-05-11 10:06:20'),
(2, 'Ahmed Fathi', '$2y$10$JllBFGu0U2L0BGmfpv7rJee9h0e0fwzVr7eP.rHaVzXlJZAtS534a', NULL, 1, '', 'Ahmed@asiacademy.net', 'Male', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, '2017-05-11 10:06:20', '2017-05-11 10:06:20'),
(3, 'Amr Sharkas', '', NULL, 1, '0112421355', 'asharkas@arabiclocalizer.com', 'Male', '2017-05-01', '01112421335', 0, 0, 0, 100.00, NULL, '2017-05-01', 4, NULL, '2017-05-11 10:27:32', '2017-05-11 10:30:15'),
(4, 'isis hawas', '', NULL, 1, '0112421355', 'isis@gmail.com', 'Female', '2017-05-01', '01112421335', NULL, NULL, NULL, NULL, NULL, '2017-05-01', 3, NULL, '2017-05-11 10:27:50', '2017-05-11 10:28:04');

--
-- Dumping data for table `users_schedules`
--

INSERT INTO `users_schedules` (`id`, `course_id`, `schedule_id`, `user_id`, `interest_status`, `price`, `payment_percentage`, `payment_status`, `attended`, `has_notice`, `comment`, `admin_show`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 3, NULL, 2500.00, 100.00, NULL, '0', 0, NULL, 0, '2017-05-11 10:29:21', '2017-05-11 10:30:15');

--
-- Dumping data for table `user_roles`
--

INSERT INTO `user_roles` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Owner', '2017-05-11 10:06:20', '2017-05-11 10:06:20'),
(2, 'Registration Manager', '2017-05-11 10:06:20', '2017-05-11 10:06:20'),
(3, 'Instructor', '2017-05-11 10:06:20', '2017-05-11 10:06:20'),
(4, 'Client', '2017-05-11 10:06:20', '2017-05-11 10:06:20');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
