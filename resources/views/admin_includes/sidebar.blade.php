<div class="page-sidebar-wrapper">
		<!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
		<!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
		<div class="page-sidebar navbar-collapse collapse">
                    <!-- BEGIN SIDEBAR MENU -->
                    <!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
                    <!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
                    <!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
                    <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
                    <!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
                    <!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
            <div class="slimScrollDiv" style="position: relative; overflow: hidden;  ">

                <ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" >
                    <!-- DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element -->
                    <li class="sidebar-toggler-wrapper">
                        <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
                        <div class="sidebar-toggler">
                            <span></span>
                        </div>
                        <!-- END SIDEBAR TOGGLER BUTTON -->
                    </li>
                    <!-- DOC: To remove the search box from the sidebar you just need to completely remove the below "sidebar-search-wrapper" LI element -->
                    <li class="sidebar-search-wrapper">
                        <!-- BEGIN RESPONSIVE QUICK SEARCH FORM -->
                        <!-- DOC: Apply "sidebar-search-bordered" class the below search form to have bordered search box -->
                        <!-- DOC: Apply "sidebar-search-bordered sidebar-search-solid" class the below search form to have bordered & solid search box -->
                        <form class="sidebar-search hide" action="page_general_search_3.html" method="POST">
                            <a href="javascript:;" class="remove">
                                <i class="icon-close"></i>
                            </a>
                            <div class="input-group">
                                <input class="form-control" placeholder="Search..." type="text">
                                <span class="input-group-btn">
                                        <a href="javascript:;" class="btn submit">
                                            <i class="icon-magnifier"></i>
                                        </a>
                                    </span>
                            </div>
                        </form>
                        <!-- END RESPONSIVE QUICK SEARCH FORM -->
                    </li>
                    <h3></h3>
                    


                            </li>

                            
                            

                        

                        @if(Auth::user()->role_id == 1 || Auth::user()->role_id == 2 )
                            <li class="nav-item start <?php if($i=='clients'){?>active<?php ;}?>">
                                <a href="/admin/clients" class="nav-link nav-toggle">
                                    <i class="fa fa-user" aria-hidden="true"></i>
                                    <span class="title">Clients</span>
                                </a>

                            </li>
                            <li class="nav-item start <?php if($i=='instructors'){?>active<?php ;}?>">
                                <a href="/admin/instructors" class="nav-link nav-toggle">
                                    <i class="fa fa-users" aria-hidden="true"></i>
                                    <span class="title">Trainers & Consultants</span>
                                </a>

                            </li>
                            <li class="nav-item start <?php if($i=='places'){?>active<?php ;}?>">
                                <a href="/admin/places" class="nav-link nav-toggle">
                                    <i class="fa fa-hotel" aria-hidden="true"></i>
                                    <span class="title">Places</span>
                                </a>

                            </li>
                            <li class="nav-item start <?php if($i=='courses'){?>active<?php ;}?>">
                                <a href="/admin/courses" class="nav-link nav-toggle">
                                    <i class="fa fa-book" aria-hidden="true"></i>
                                    <span class="title">Courses</span>
                                </a>

                            </li>
                            
                            <li class="nav-item start <?php if($i=='schedules'){?>active<?php ;}?>">
                                <a href="/admin/schedules" class="nav-link nav-toggle">
                                    <i class="fa fa-clock-o"></i>
                                    <span class="title">Events</span>
                                </a>

                                


                            </li>
                            @can('super_user')
                            <li class="nav-item start <?php if($i=='pricing'){?>active<?php ;}?>">
                                <a href="/admin/consultations/pricing" class="nav-link nav-toggle">
                                    <i class="fa fa-money"></i>
                                    <span class="title">Consultation Pricing</span>
                                </a>

                                


                            </li>
                            @endcan
                            <li class="nav-item start <?php if($i=='consultations'){?>active<?php ;}?>">
                                <a href="/admin/consultations" class="nav-link nav-toggle">
                                    <i class="fa fa-users"></i>
                                    <span class="title">Consultations</span>
                                </a>

                                


                            </li>
                            
                            <li class="nav-item start <?php if($i=='expenses'){?>active<?php ;}?>">
                                <a href="/admin/expenses" class="nav-link nav-toggle">
                                    <i class="fa fa-money"></i>
                                    <span class="title">Expenses</span>
                                </a>

                                


                            </li>
                            @can('super_user')
                            <li class="nav-item start <?php if($i=='coins'){?>active<?php ;}?>">
                                <a href="/admin/coins" class="nav-link nav-toggle">
                                    <i class="fa fa-money"></i>
                                    <span class="title">Grant Coins</span>
                                </a>

                                


                            </li>
                            @endcan
                            <li class="nav-item start <?php if($i=='reports'){?>active<?php ;}?>">
                                <a href="/admin/reports" class="nav-link nav-toggle">
                                    <i class="fa fa-line-chart"></i>
                                    <span class="title">Financial Report</span>
                                </a>

                                


                            </li>
                            @can('super_user')
                            <li class="nav-item start <?php if($i=='employees'){?>active<?php ;}?>">
                                <a href="/admin/employees" class="nav-link nav-toggle">
                                    <i class="fa fa-users"></i>
                                    <span class="title">Employees</span>
                                </a>

                                


                            </li>
                            @endcan
                        @endif

                    





                </ul>
                <!-- END SIDEBAR MENU -->
                <!-- END SIDEBAR MENU -->
                <div class="slimScrollBar" style="background: rgb(187, 187, 187); width: 7px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 1100px;"></div>
            </div>

        </div>
	</div>