<form action="{{route('admin.clients.save')}}" method="post" class="ajax_form j-forms larajsval form" id="main-form" novalidate>
    <div class="header">
        <p>
             @if(empty($client->name))<i class="fa fa-user"></i> New client @else <i class="fa fa-user"></i> {{$client->name}} @endif
        </p>
    </div>

    <div class="content">
        
            
            
            
            
            
            <div class="unit">
                <label class="label">Gender</label>
                <label class="input select">
                    <select name="gender" data-validation="required" data-name="Gender" class="form-control" >
                        <option value="">Select Gender</option>
                        
                            <option  value="Male" <?php if( $client->gender == "Male"){?>selected<?php ;}?>>Male</option>
                            <option  value="Female" <?php if( $client->gender == "Female"){?>selected<?php ;}?>>Female</option>
                    </select>
                    <span class="error_message"></span>
                    <i></i>
                </label>
            </div>
            
            <div class="unit">
                <label class="label">Name</label>
                <div class="input">
                    <label class="icon-right">
                        <i class="fa fa-anchor"></i>
                    </label>
                    <input name="name" type="text" class="form-control" data-validation="required" data-name="Name" value="{{$client->name}}"  >
                    <span class="error_message"></span>
                </div>
            </div>
            <div class="unit">
                <label class="label">Email</label>
                <div class="input">
                    <label class="icon-right">
                        <i class="fa fa-envelope"></i>
                    </label>
                    <input name="email" type="text" data-validation="required,email" data-name="Email"  class="form-control" value="{{$client->email}}"  >
                    <span class="error_message"></span>
                </div>
            </div>
            <div class="unit">
                <label class="label">Phone</label>
                <div class="input">
                    <label class="icon-right">
                        <i class="fa fa-phone"></i>
                    </label>
                    <input name="phone" type="text" data-validation="required,number" data-name="Phone" class="form-control" value="{{$client->phone}}"  >
                    <span class="error_message"></span>
                </div>
            </div>
            <div class="unit">
                <label class="label">Fax</label>
                <div class="input">
                    <label class="icon-right">
                        <i class="fa fa-phone"></i>
                    </label>
                    <input name="fax" type="text" data-validation="number" data-name="Fax" class="form-control" value="{{$client->fax}}"  >
                    <span class="error_message"></span>
                </div>
            </div>
            <div class="unit">
                <label class="label">Date of birth</label>
                <div class="input">
                    <label class="icon-right">
                        <i class="fa fa-calendar"></i>
                    </label>
                    <input name="date_of_birth" type="text" data-validation="" data-name="Date of birth" class="form-control datePicker" value="<?php if($client->date_of_birth!= ""){echo date("m/d/Y",strtotime($client->date_of_birth)) ;}?>"  >
                    <span class="error_message"></span>
                </div>
            </div>
            <div class="unit">
                <label class="label">Joining Date</label>
                <div class="input">
                    <label class="icon-right">
                        <i class="fa fa-calendar"></i>
                    </label>
                    <input autocomplete="off" name="joining_date" type="text" data-validation="required" data-name="Joining Date" class="form-control datePicker" value="<?php if($client->joining_date!= ""){echo date("m/d/Y",strtotime($client->joining_date)) ;}?>"  >
                    <span class="error_message"></span>
                </div>
            </div>
            <div class="hide unit">
                <label class="label">password</label>
                <div class="input">
                    <label class="icon-right">
                        <i class="fa fa-lock"></i>
                    </label>
                    <input name="password" data-validation="" data-name="password" type="password" class="form-control" value="{{$client->dec_password}}"  >
                    <span class="error_message"></span>
                </div>
            </div>
            <div class="hide unit">
                <label class="label">Retype Password</label>
                <div class="input">
                    <label class="icon-right">
                        <i class="fa fa-lock"></i>
                    </label>
                    <input name="password2" data-validation="" data-name="password" type="password" class="form-control" value="{{$client->dec_password}}" >
                    <span class="error_message"></span>
                </div>
            </div>
            <div class="unit">
                <label class="label">Refered By</label>
                <label class="input select">
                    <select name="refered_by" data-validation="" data-name="Mother" class="form-control select2" >
                        <option value="" >Select Client</option>
                        @foreach($clients as $clientx)
                            <option  value="{{$clientx->id}}" <?php if($clientx->id == $client->refered_by){?>selected<?php ;}?>>{{$clientx->name}}</option>
                        @endforeach
                    </select>
                    <span class="error_message"></span>
                    <i></i>
                </label>
            </div>
            <div class="unit">
                <label class="label">Mother</label>
                <label class="input select">
                    <select name="mother_id" data-validation="" data-name="Mother" class="form-control select2" >
                        <option value="" >Select Mother</option>
                        @foreach($mothers as $clientx)
                            <option  value="{{$clientx->id}}" <?php if($clientx->id == $client->mother_id){?>selected<?php ;}?>>{{$clientx->name}}</option>
                        @endforeach
                    </select>
                    <span class="error_message"></span>
                    <i></i>
                </label>
            </div>
            
            <div class="unit">
                <label class="label">Father</label>
                <label class="input select">
                    <select name="father_id" data-validation="" data-name="Father" class="form-control select2" >
                        <option value="" >Select Father</option>
                        @foreach($fathers as $clientx)
                            <option  value="{{$clientx->id}}" <?php if($clientx->id == $client->father_id){?>selected<?php ;}?>>{{$clientx->name}}</option>
                        @endforeach
                    </select>
                    <span class="error_message"></span>
                    <i></i>
                </label>
            </div>
            
            
            
            
            
            
            <input type="hidden" name="id" id="id" value="{{$client->id}}"  data-validation="" data-name="id"  />
    </div>
    <div id="response"></div>
    <div class="footer">
        <button type="submit" class="btn btn-lg color" ><i class="fa fa-edit"></i> Save</button>

    </div>
    <input type="reset" class="hide resetForm" >

</form>


