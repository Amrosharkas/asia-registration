<div class="portlet light">
    <div class="portlet-title">
        <div class="caption font-color">
            <i class="fa fa-user font-color"></i>Clients
        </div>
        <div class="btn-group pull-right">
                    
                      
                    <a href="{{route('admin.clients.init')}}" class="pjax-link"  >
                        <button type="button" class="btn color" id="add_new" >
                            Add New <i class="fa fa-plus"></i>
                        </button>      
                      </a> 
                      
                                       
                    </div>
    </div>
    <div class="portlet-body">
    <form class="ajax_form" method="post" action="/admin/schedules/recommend">
      <div class="table-toolbar" >
            <div class="row" >
                <div class="col-md-5 hide" >
                	<div class="btn-group">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" >
                   <tr>
                     <td align="left" valign="middle"><select name="course_id" data-validation="Required" data-name="Course" class="form-control select2" style="display:inline;" >
                        <option value="" >Select Course</option>
                        @foreach($courses as $course)
                            <option  value="{{$course->id}}" >{{$course->name}} </option>
                        @endforeach
                    </select></td>
                     <td align="left" valign="middle"><input class="btn green"  type="submit" style="display:inline;" value="Recommend to course" name
                     ="course"    />
                 
 
                     </td>
                   </tr>
                 </table>
                    
                              
                      
                      
                    
                      
                                       
                  </div>    
                </div>
                <div class="col-md-5" >
                    <div class="btn-group">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                   <tr>
                     <td align="left" valign="middle"><select name="schedule_id" data-validation="Required" data-name="Course" class="form-control select2" style="display:inline;" >
                        <option value="" >Select Schedule</option>
                        @foreach($schedules as $schedule)
                            <option  value="{{$schedule->id}}" >
                            <?php if(isset($schedule->getCourse->name)){?>
                            {{$schedule->getCourse->name}} 
                            <?php ;}?>
                            ( {{ Carbon\Carbon::parse($schedule->start_date)->format('d M')    }} )
                            
                            </option>
                        @endforeach
                    </select></td>
                     <td align="left" valign="middle"><input class="btn green"  type="submit" style="display:inline;" value="Recommend to Event" name
                     ="event"    /></td>
                   </tr>
                 </table>
                    
                              
                      
                      
                    
                      
                                       
                  </div>
                </div>
                
            </div>
        </div>
        <table class="table table-striped table-bordered table-hover table-dt" id="table-dt" >
            <thead>
                <tr class="tr-head">
                  <th valign="middle"><input type="checkbox" class="group-checkable"></th>
                  <th valign="middle">
                      Name
                  </th>
                  <th valign="middle">Phone</th>
                  <th valign="middle">Email</th>
                    <th valign="middle">
                        Created at
                    </th>
                    <th valign="middle">
                        Action
                    </th>
                </tr>
            </thead>
            <tbody>
                @foreach($clients as $client)
                <tr class="odd gradeX" id="data-row-{{$client->id}}">
                  <td valign="middle">
                  <input type="checkbox" name="clients[]"  class="table-checkbox"  value="{{$client->id}}" >
                  </td>
                  <td valign="middle">
                      {{$client->name}}
                    </td>
                    <td valign="middle">{{$client->phone}}</td>
                  <td valign="middle">{{$client->email}}</td>
                    <td valign="middle">
                        {{$client->created_at}}
                    </td>
                    <td valign="middle">
                    <a href="{{route('admin.clients.profile',['id'=>$client->id])}}" class="btn color pjax-link" ><i class="fa fa-user"></i> Profile</a>
 						@if(Auth::user()->role_id <= 1)
                        <a href="#" data-action="{{route('admin.clients.delete',['id'=>$client->id])}}"  class="btn red delete_single" ><i class="fa fa-remove"></i> Delete</a> 
                        @endif
                        
                        <a href="{{route('admin.clients.edit',['id'=>$client->id])}}" class="btn green pjax-link" ><i class="fa fa-edit"></i> Edit</a> 
                        
                    </td>
                </tr>
                @endforeach

        </table>
        </form>
    </div>
</div>