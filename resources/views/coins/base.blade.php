<?php 
	$i ="coins" ; $j = "";
 ?>
@extends('admin.master')
@section('plugins_css')
<link href="/assets/admin/pages/css/profile.css" rel="stylesheet" type="text/css"/>

<link href="/assets/admin/pages/css/tasks.css" rel="stylesheet" type="text/css"/>

@stop

@section('plugins_js')

@stop

@section('page_js')
<script type="text/javascript" src="{{asset('assets/admin/pages/scripts/coins.js')}}"></script>

@endSection

@section('add_inits')

@stop

@section('title')
Grant Coins
@stop

@section('page_title')

@stop

@section('page_title_small')

@stop

@section('content')
@include($partialView)
@stop

