<form action="{{route('admin.coins.save')}}" method="post" class="ajax_form j-forms larajsval form" id="main-form" novalidate>
    <div class="header">
        <p>
             @if(empty($coin->name))<i class="fa fa-user"></i> New coin @else <i class="fa fa-user"></i> {{$coin->name}} @endif
        </p>
    </div>

    <div class="content">
        
            
            
            
            
            
            <div class="unit">
                <label class="label">Client</label>
                <label class="input select">
                    <select name="user_id" data-validation="" data-name="Client" class="form-control select2" >
                        <option value="" >Select client</option>
                        @foreach($clients as $coinx)
                            <option  value="{{$coinx->id}}" <?php if($coinx->id == $coin->user_id){?>selected<?php ;}?>>{{$coinx->name}}</option>
                        @endforeach
                    </select>
                    <span class="error_message"></span>
                    <i></i>
                </label>
            </div>
            
            <div class="unit">
                <label class="label">Amount</label>
                <div class="input">
                    <label class="icon-right">
                        <i class="fa fa-money"></i>
                    </label>
                    <input name="amount" type="text" class="form-control" data-validation="required" data-name="Amount" value="{{$coin->amount}}"  >
                    <span class="error_message"></span>
                </div>
            </div>
            <div class="unit">
                <label class="label">Date</label>
                <div class="input">
                    <label class="icon-right">
                        <i class="fa fa-calendar"></i>
                    </label>
                    <input name="date" type="text" data-validation="required" data-name="Date" class="form-control datePicker" value="<?php if($coin->date!= ""){echo date("m/d/Y",strtotime($coin->date)) ;}?>"  >
                    <span class="error_message"></span>
                </div>
            </div>
            <div class="unit">
                <label class="label">Expirable</label>
                <label class="input select">
                    <select name="type" data-validation="required" data-name="Gender" class="form-control" >
                        <option value="">Select Option</option>
                        
                            <option  value="Expirable" <?php if( $coin->type == "Expirable"){?>selected<?php ;}?>>Yes</option>
                            <option  value="Retained" <?php if( $coin->type == "Retained"){?>selected<?php ;}?>>No</option>
                    </select>
                    <span class="error_message"></span>
                    <i></i>
                </label>
            </div>
            <div class="unit dependent" data-depending-on= "type" data-depending-value="Expirable">
                <label class="label">Expire After (days)</label>
                <div class="input">
                    <label class="icon-right">
                        <i class="fa fa-calendar"></i>
                    </label>
                    <input name="expire_after" type="text" data-validation="required,number" data-name="Expiry days" class="form-control" value="{{$coin->expire_after}}"  >
                    <span class="error_message"></span>
                </div>
            </div>
            
           
            
            
            
            
            
            
            
            
            
            
            <input type="hidden" name="id" id="id" value="{{$coin->id}}"  data-validation="" data-name="id"  />
    </div>
    <div id="response"></div>
    <div class="footer">
        <button type="submit" class="btn btn-lg color" ><i class="fa fa-edit"></i> Save</button>

    </div>
    <input type="reset" class="hide resetForm" >

</form>


