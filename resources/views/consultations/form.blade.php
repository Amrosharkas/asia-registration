<form action="{{route('admin.consultations.save')}}" method="post" class="ajax_form j-forms larajsval form" id="main-form" novalidate>
    <div class="header">
        <p>
              <i class="fa fa-user"></i> {{$consultation->getClient->name}}
        </p>
    </div>

    <div class="content">
        
            <div class="divider-text gap-top-20 gap-bottom-45">
                <span>Date and time</span>
            </div>
           <div class="unit">
                <label class="label">Date and time</label>
                <div class="row">
                <div class="col-md-3">
                <div class="input">
                    <label class="icon-right">
                        <i class="fa fa-calendar"></i>
                    </label>
                    <input name="date" type="text" class="form-control datePicker" data-validation="required" data-name="Date" value="<?php if($consultation->start_time!=""){?><?php echo date("m/d/Y",strtotime($consultation->start_time));?><?php ;}?>"  >
                    <span class="error_message"></span>
                </div>
                </div>
                <div class="col-md-3">
                <div class="input">
                    <label class="icon-right">
                        <i class="fa fa-clock-o"></i>
                    </label>
                    <input name="time" type="text" class="form-control timepicker timepicker-24" data-validation="required" data-name="Time" value="<?php if($consultation->start_time!=""){?><?php echo date("H:i",strtotime($consultation->start_time));?><?php ;}?>"  >
                    <span class="error_message"></span>
                </div>
                </div>
                </div>
            </div>
            @can("super_user")
            <div class="divider-text gap-top-20 gap-bottom-45">
                <span>Duration</span>
            </div>
            <div class="unit">
                <label class="label"></label>
                <div class="input sw">
                    
<p class="stopwatch">
<?php if($consultation->duration ==""){?>
<span id="hours">00</span>:<span id="minutes">00</span>:<span id="seconds">00</span><span class="hide" id="tens">00</span>
<?php ;}else{
	$duration = explode(":",$consultation->duration);
	?>
<span id="hours">{{$duration[0]}}</span>:<span id="minutes">{{$duration[1]}}</span>:<span id="seconds">{{$duration[2]}}</span><span class="hide" id="tens">00</span>
<?php ;}?>
</p>
<button type="button" id="button-start" class="btn green pull-left"><i class="fa fa-play" aria-hidden="true"></i> Start</button>
<button type="button" id="button-stop" class="btn red pull-left"><i class="fa fa-stop" aria-hidden="true"></i> Stop</button>
<button type="button" id="button-reset" class="btn blue pull-left"><i class="fa fa-circle-o-notch" aria-hidden="true"></i> Reset</button>
 








                </div>
            </div>
            <div class="row"></div>
            </br>
            <div class="unit">
                <label class="label">Duration</label>
                <div class="input">
                    <label class="icon-right">
                        <i class="fa fa-clock-o"></i>
                    </label>
                    <input name="duration" type="text" class="form-control duration_picker" data-validation="required" data-name="duration" value="<?php if($consultation->duration != ""){?>{{$consultation->duration}}<?php ;}else{?>00:00:00<?php ;}?>"  >
                    <span class="error_message"></span>
                </div>
            </div>
            <div class="divider-text gap-top-20 gap-bottom-45">
                <span>Comments</span>
            </div>
            <div class="unit">
                <label class="label">Comment</label>
                <div class="input">
                    <label class="icon-right">
                        <i class="fa fa-edit"></i>
                    </label>
                    <textarea name="comment"  class="form-control" data-validation="required" data-name="duration"   >{{$consultation->comment}}</textarea>
                    
                </div>
            </div>
            
            <div class="unit">
                <label class="label">Voice Note</label>
                <section class="experiment recordrtc">
            
            <div style="display:none;">
                <select class="recording-media">
                    
                    <option value="record-audio">Microphone</option>
                    
                </select>

                <span style="font-size: 15px;">into</span>

                <select class="media-container-format">
                    
                    <option>pcm</option>
                    
                </select>
</div>
                

               

                <button id="btn-start-recording" type="button" class="btn green pull-left"><i class="fa fa-microphone" aria-hidden="true"></i> Start Recording</button>
                <button id="btn-pause-recording" type="button" class="btn blue pull-left">Pause</button>

                
                <div style="display:none;">
                <select class="media-resolutions">
                    <option value="default">Default resolutions</option>
                    <option value="1920x1080">1080p</option>
                    <option value="1280x720">720p</option>
                    <option value="3840x2160">4K Ultra HD (3840x2160)</option>
                </select>

                <select class="media-framerates">
                    <option value="default">Default framerates</option>
                    <option value="5">5 fps</option>
                    <option value="15">15 fps</option>
                    <option value="24">24 fps</option>
                    <option value="30">30 fps</option>
                    <option value="60">60 fps</option>
                </select>

                <select class="media-bitrates">
                    <option value="default">Default bitrates</option>
                    <option value="8000000000">1 GB bps</option>
                    <option value="800000000">100 MB bps</option>
                    <option value="8000000">1 MB bps</option>
                    <option value="800000">100 KB bps</option>
                    <option value="8000">1 KB bps</option>
                    <option value="800">100 Bytes bps</option>
                </select>
                <input type="checkbox" id="chk-MultiStreamRecorder" style="margin:0;width:auto;">
                <label for="chk-MultiStreamRecorder" style="font-size: 15px;margin:0;width: auto;cursor: pointer;-webkit-user-select:none;user-select:none;">All cameras?</label>
                </div>
            

            <div style="text-align: center; display: none;">
                <button id="save-to-disk" type="button" class="btn blue" >Save</button>
                <button id="upload-to-php" style="display:none;" >Upload to PHP</button>
                <button id="open-new-tab" style="display:none;" >Open New Tab</button>

                <div style="margin-top: 10px; display:none;">
                    <span id="signinButton" class="pre-sign-in">
                      <span
                        class="g-signin"
                        data-callback="signinCallback"
                        data-clientid="41556190767-7amutgh7ug0dfkkeb9fkrpt8j86rv5o3.apps.googleusercontent.com"
                        data-cookiepolicy="single_host_origin"
                        data-scope="https://www.googleapis.com/auth/youtube.upload https://www.googleapis.com/auth/youtube">
                      </span>
                    </span>

                    <button id="upload-to-youtube" style="vertical-align:top;">Upload to YouTube</button>
                </div>
            </div>

            <div style="margin-top: 10px;">
            	<?php if($consultation->audio_file!= ""){?><audio src="/uploads/{{$consultation->audio_file}}" controls=""></audio><?php ;}?>
                <video style="height:30px;" id="recording-player" controls muted></video>
            </div>
        </section>
            </div>
            <div class="divider-text gap-top-20 gap-bottom-45">
                <span>Next Contact</span>
            </div>
            
            <div class="unit">
                <label class="label">Next date to contact this client</label>
                <div class="input">
                    <label class="icon-right">
                        <i class="fa fa-calendar"></i>
                    </label>
                    <input name="next_contact" type="text" class="form-control datePicker" data-validation="" data-name="duration" value="<?php if($consultation->next_contact!=""){?><?php echo date("m/d/Y",strtotime($consultation->next_contact));?><?php ;}?>"  >
                    <span class="error_message"></span>
                </div>
            </div>
            @endcan
            
            
            
            
            
            
            
            <input class="cosultation_id" type="hidden" name="id" id="id" value="{{$consultation->id}}"  data-validation="" data-name="id"  />
    </div>
    <div id="response"></div>
    <div class="footer">
        <button type="submit" class="btn btn-lg color" ><i class="fa fa-edit"></i> Save</button>

    </div>
    <input type="reset" class="hide resetForm" >

</form>


