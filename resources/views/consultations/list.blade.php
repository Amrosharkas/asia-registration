<div class="portlet light">
    <div class="portlet-title">
        <div class="caption font-color">
            <i class="fa fa-user font-color"></i>Consultations
        </div>
    </div>
    <div class="portlet-body">
      <div class="table-toolbar">
            <div class="row">
                <div class="col-md-6">
                    <div class="btn-group">
                    
                    <form  method="post" action="/admin/consultations/init">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <?php if(isset($_GET['error'])){?>
                   <tr>
                     <td colspan="3" align="left" valign="middle"><span class="danger"><?php echo $_GET['error'];?></span></td>
                     </tr>
                   <tr>
                     <td height="10" colspan="3" align="left" valign="middle"><div></div></td>
                   </tr>
                     <?php ;}?>
                   <tr>
                     <td align="left" valign="middle">
                     
                     <select name="client_id" data-validation="" data-name="Client" class="form-control select2" style="display:inline;" >
                        <option value="" >Select Client</option>
                        @foreach($clients as $client)
                            <option  value="{{$client->id}}" >{{$client->name}} </option>
                        @endforeach
                    </select>
                    
                    </td>
                     <td width="20" align="left" valign="middle"><div></div></td>
                     <td align="left" valign="middle"><input class="btn green"  type="submit" style="display:inline;" value="New Consultation"/></td>
                     </tr>
                 </table> 
                 </form>     
                           
                    </div>
                </div>
                <div class="col-md-6"></div>
            </div>
        </div>
        <table class="table table-striped table-bordered table-hover table-dt" id="table-dt" >
            <thead>
                <tr class="tr-head">
                  <th valign="middle">
                      Client
                  </th>
                  <th valign="middle">Date &amp; Time</th>
                  <th valign="middle">Next Contact</th>
                  <th valign="middle">Call Reply</th>
                  <th valign="middle">Points</th>
                  <th valign="middle">Price</th>
                  <th valign="middle">Paid</th>
                  <th valign="middle">Remaining</th>
                  <th valign="middle">Add Payment</th>
                    <th valign="middle">
                        Action
                    </th>
                </tr>
            </thead>
            <tbody>
                @foreach($consultations as $consultation)
                <tr class="odd gradeX" id="data-row-{{$consultation->id}}">
                  <td valign="middle">
                  <?php if(isset($consultation->getClient->name)){?>
                      {{$consultation->getClient->name}}
                      <?php ;}?>
                  </td>
                  <td valign="middle"><?php echo date("l d M h:i a",strtotime($consultation->start_time));?></td>
                  <td valign="middle">
                  <span class="<?php if(strtotime(date("Y-m-d")) < strtotime($consultation->next_contact)){?>info<?php ;}else{?><?php if($consultation->call_reply == ""){?>danger<?php ;}?><?php ;}?>">
				  <?php if($consultation->next_contact != ""){echo date("l d M h:i a",strtotime($consultation->next_contact));}?>
                  </span>
                  
                  </td>
                  <td valign="middle">
                      <?php if(strtotime(date("Y-m-d")) >= strtotime($consultation->next_contact) && $consultation->next_contact != ""){?>
                      <form id="form1" name="form1" class="ajax_form" method="post" action="/admin/consultations/add_reply">
                      <input type="hidden" value="{{$consultation->id}}" name="id" />
                        <textarea name="reply" class="call_reply" placeholder = "Type the reply here">{{$consultation->call_reply}}</textarea>
                        </form>
                        <?php ;}?>
                        </td>
                  <td align="center" valign="middle"><span class="info">{{$consultation->getClient->points_balance}}</span></td>
                  <td align="center" valign="middle">{{$consultation->price}}</td>
                  <td align="center" valign="middle">
                  <?php if($consultation->payment_percentage!=""){?>
                  <span class="success">{{$consultation->payment}} ({{$consultation->payment_percentage}}%)</span>
                  <a href="{{route('admin.transactions.payment_history',['user_id'=>$consultation->client_id,'stuff_id'=>$consultation->id,'stuff_identifier' => 'consultation_id'] )}}" class="btn blue popup" ><i class="fa fa-money"></i> View History</a>
				  <?php ;}?>
                  </td>
                  <td align="center" valign="middle"><span class="danger"><?php echo ($consultation->price - $consultation->payment);?></span></td>
                  <td align="center" valign="middle">
                  <div class="payment_info"> 
                  <?php if($consultation->price != "" && $consultation->payment_percentage != 100 && $consultation->closed == 0){?>
                  <a href="#" class="add_payment btn green">Click to add +</a>
                  <?php ;}?>
                  </div>
                  <form class="ajax_form" action="/admin/consultations/add_payment" style="display:none;" method="post">
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td><input name="consultation_id" placeholder="Amount" type="hidden" value="{{$consultation->id}}" />
                  <input  name="user_id" type="hidden" value="{{$consultation->client_id}}" />
                  <input style="width:75px;" name="amount" type="text" placeholder=" Amount"  /></td>
    <td>
    <select name="payment_type">
    	<option value="Cash">Cash</option>
        <option value="Coins">Coins</option>
    </select></td>
    <td>
    <input type="text" class="datePicker" name="date" style="width:90px;" placeholder=" Date" />
    </td>
    <td><button type="submit" class="btn btn-info" ><i class="fa fa-check" aria-hidden="true"></i></button>
    
    </td>
    <td>
    <button type="button" class="btn btn-danger cancel_payment" ><i class="fa fa-remove" aria-hidden="true"></i></button>
    </td>
  </tr>
</table>
                   
                  </form>
                  {{$consultation->payment_status}}</td>
                    <td valign="middle">
 						@if(Auth::user()->role_id <= 1)
                        <a href="#" data-action="{{route('admin.consultations.delete',['id'=>$consultation->id])}}"  class="btn red delete_single" ><i class="fa fa-remove"></i> Delete</a> 
                        @endif
                        
                        <a href="{{route('admin.consultations.edit',['id'=>$consultation->id])}}" class="btn green" ><i class="fa fa-edit"></i> Edit</a> 
                        @can('super_user')
                        <?php if($consultation->closed == 0){?>
                        <a href="#" data-action="{{route('admin.consultations.end_transactions',['consultation_id'=>$consultation->id])}}"  class="btn color confirm_action" ><i class="fa fa-check"></i> Close payments</a> 
                        <?php ;}?>
                        @endcan
                        @can('super_user')
                        <?php if($consultation->closed == 1){?>
                        <a href="#" data-action="{{route('admin.consultations.reopen_transactions',['consultation_id'=>$consultation->id])}}"  class="btn color confirm_action" ><i class="fa fa-check"></i> Re-open payments</a> 
                        <?php ;}?>
                        @endcan
                        
                        
                        
                        
                    </td>
                </tr>
                @endforeach

        </table>
    </div>
</div>