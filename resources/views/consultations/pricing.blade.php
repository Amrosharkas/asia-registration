<form action="{{route('admin.consultations.save_pricing')}}" method="post" class="ajax_form j-forms larajsval form" id="main-form" novalidate>
    <div class="header">
        <p>
            <i class="fa fa-money"></i> Pricing 
        </p>
    </div>

    <div class="content">
        
            
           <div class="unit">
                <label class="label">45 minutes</label>
                <label class="input">
                    <input id="pricing_45" name="pricing_45" data-validation="required,number" data-name="Price" class="form-control" value="{{$pricing->pricing_45}}" />
                    
                    <span class="error_message"></span>
                    <i></i>
                </label>
            </div>
            <div class="unit">
                <label class="label">More than 45 minutes</label>
                <label class="input">
                    <input id="pricing_1hour" name="pricing_1hour" data-validation="required,number" data-name="Price" class="form-control" value="{{$pricing->pricing_1hour}}" />
                    
                    <span class="error_message"></span>
                    <i></i>
                </label>
            </div>
            <div class="unit">
                <label class="label">Points gained by the client</label>
                <label class="input">
                    <input id="points_gained" name="points_gained" data-validation="required,number" data-name="This field" class="form-control" value="{{$pricing->points_gained}}" />
                    
                    <span class="error_message"></span>
                    <i></i>
                </label>
            </div>
            <div class="unit">
                <label class="label">Points gained by the referral</label>
                <label class="input">
                    <input id="referral_points_gained" name="referral_points_gained" data-validation="required,number" data-name="This Field" class="form-control" value="{{$pricing->referral_points_gained}}" />
                    
                    <span class="error_message"></span>
                    <i></i>
                </label>
            </div>
            
            
            
            
            
            
            
            
            <input type="hidden" name="id" id="id" value="{{$pricing->id}}"  data-validation="" data-name="id"  />
    </div>
    <div id="response"></div>
    <div class="footer">
        <button type="submit" class="btn btn-lg color" ><i class="fa fa-edit"></i> Save</button>

    </div>
    <input type="reset" class="hide resetForm" >

</form>


