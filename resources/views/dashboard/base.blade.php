<?php 
	$i ="dashboard" ; $j = "";
 ?>
@extends('admin.master')
@section('plugins_css')
<style>
#chartdiv {
  width: 100%;
  height: 590px;
}
#chartdiv2 {
  width: 100%;
  height: 330px;
}
</style>
@stop

@section('plugins_js')

@stop

@section('page_js')
<script type="text/javascript" src="{{asset('assets/admin/pages/scripts/balance.js')}}"></script>
@if($partialView =='dashboard.hr')
    <script>
        var chart = AmCharts.makeChart("chartdiv", {
            "theme": "light",
            "type": "serial",
            "dataProvider": [
                    @foreach($weekly_sign_ins as $days)
                {
                    "country": "{{ Carbon\Carbon::parse($days->date)->format('d,M') }}",
                    "year2004": "{{$days->days}}"
                },
                    @endforeach

            ],
            "valueAxes": [{
                "stackType": "3d",
                "unit": " Employees",
                "position": "left",
                "title": "Number Of Employees",
            }],
            "startDuration": 1,
            "graphs": [{
                "balloonText": " <b>[[category]]:</b> [[value]] Employees",
                "fillAlphas": 1,
                "lineAlpha": 1,
                "title": "2004",
                "type": "column",
                "valueField": "year2004"
            }],
            "plotAreaFillAlphas": 0.1,
            "depth3D": 60,
            "angle": 30,
            "categoryField": "country",
            "categoryAxis": {
                "gridPosition": "start"
            }
        });
    </script>

    <script>
        var chart = AmCharts.makeChart( "chartdiv2", {
            "type": "pie",
            "theme": "light",
            "dataProvider": [
                    @foreach($weekly_sign_ins_statuses as $status)
                {
                    "country": "{{$status->status}}",
                    "value": "{{$status->count}}"
                },
                @endforeach
            ],
            "valueField": "value",
            "titleField": "country",
            "outlineAlpha": 1,
            "depth3D": 15,
            "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
            "angle": 30,
            "export": {
                "enabled": true
            }
        } );
    </script>

    @else

    <script>
        var chart = AmCharts.makeChart("chartdiv", {
            "type": "serial",
            "theme": "light",
            "categoryField": "year",
            "rotate": true,
            "startDuration": 1,
            "categoryAxis": {
                "gridPosition": "start",
                "position": "left"
            },
            "trendLines": [],
            "graphs": [
                {
                    "balloonText": "Income:[[value]]",
                    "fillAlphas": 1,
                    "id": "AmGraph-1",
                    "lineAlpha": 1,
                    "title": "Income",
                    "type": "column",
                    "valueField": "income"
                },
                {
                    "balloonText": "Penalties:[[value]]",
                    "fillAlphas": 1,
                    "id": "AmGraph-2",
                    "lineAlpha": 1,
                    "title": "Expenses",
                    "type": "column",
                    "valueField": "expenses"
                }
            ],
            "guides": [],
            "valueAxes": [
                {
                    "id": "ValueAxis-1",
                    "position": "top",
                    "axisAlpha": 0
                }
            ],
            "allLabels": [],
            "balloon": {},
            "titles": [],
            "dataProvider": [
                    @foreach($penalties as $penalty)
                {
                    "year": "{{ $penalty->penalty_reason }}",
                    "expenses": "{{ $penalty->count }}",
                         },
                    @endforeach

            ],
            "export": {
                "enabled": false
            }

        });
    </script>
@endif
@endSection

@section('add_inits')

@stop

@section('title')
Dashboard
@stop

@section('page_title')

@stop

@section('page_title_small')

@stop

@section('content')
@include($partialView)
@stop

