<div class="row">
    <div class="col-md-6 col-sm-6">
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-share hide"></i>
                    <span class="caption-subject bold uppercase font-color">Request History</span>
                </div>
               
            </div>
            <div class="portlet-body">

                <div class="scroller" style="height: 300px;" data-always-visible="1" data-rail-visible="0">
                    <ul class="feeds">
                        @foreach($reqs as $req)
                            <li>
                                <div class="col1">
                                    <div class="cont">
                                        <div class="cont-col1">
                                            @if($req->status ==2)
                                                <div class="label label-sm label-warning">
                                                    <i class="fa fa-clock-o"></i>
                                                </div>
                                            @elseif($req->status ==1)
                                                <div class="label label-sm label-success">
                                                    <i class="fa fa-check"></i>
                                                </div>
                                            @elseif( $req->status ==0)
                                                <div class="label label-sm label-danger">
                                                    <i class="fa fa-remove"></i>
                                                </div>
                                            @else
                                                <div class="label label-sm label-info">
                                                    <i class="fa fa-check"></i>
                                                </div>
                                            @endif

                                        </div>
                                        <div class="cont-col2">
                                            <div class="desc">
                                                <?php $req_type = str_replace("ves","ve",$req->type);
                                                $req_type= str_replace("ns","n",$req_type);
                                                echo str_replace("Home","Work From Home",$req_type);
                                                ?> in
                                                <span class="label label-sm label-warning ">
                                                        <?php if($type == "Permissions"  || $type == "All" || $type == "Half Day"  || $type == "Home" || $type == "Overtime"){?>
                                                    <b>Day: </b>{{$req->req_day}}
                                                    <?php ;}?>

                                                    <?php if($req->type == "Emergency Leaves" || $req->type == "Normal Leaves"){?>
                                                    -<b>From: </b>   <?php echo date("Y-m-d",strtotime($req->time_from));?>
                                                    <?php ;}else{?>
                                                    {{$req->time_from}}
                                                    <?php ;}?>

                                                    <?php if($req->type == "Emergency Leaves" || $req->type == "Normal Leaves"){?>
                                                    -<b>To: </b>     <?php echo date("Y-m-d",strtotime($req->time_to));?>
                                                    <?php ;}else{?>
                                                    {{$req->time_to}}
                                                    <?php ;}?>
                                                    </span>
                                                <?php if($req->type == "Half Day" || $req->type == "Home" ){?>
                                                <span class="label label-sm label-success">
                                                        {{$req->add_info}}
                                                    </span>
                                                <?php ;}?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col2">
                                    <div class="mt-action-datetime ">
                                            <span class="mt-action-date username username-hide-on-mobile">
                                                {{ Carbon\Carbon::parse($req->created_at)->format('d, M Y') }}
                                            </span>
                                        <span class="mt-action-dot bg-green"></span>
                                        <span class="mt=action-time">
                                                {{ Carbon\Carbon::parse($req->created_at)->format(' h:i:s A') }}
                                            </span>
                                    </div>
                                </div>
                            </li>
                        @endforeach
                    </ul>
              </div>
                <div class="scroller-footer">
                    <div class="btn-arrow-link pull-right">
                        <a href="/admin/reqs/user_requests/{{Auth::user()->id}}/2/0">Pending Requests</a>
                        <i class="icon-arrow-right"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-sm-6">
        <div class="portlet light bordered">
            <div class="portlet-title tabbable-line">
                <div class="caption">
                    <i class=" icon-social-twitter hide"></i>
                    <span class="caption-subject bold uppercase font-color">Penalty History</span>
                </div>
            </div>
            <div class="portlet-body">
                <div id="chartdiv"></div>
            </div>
        </div>

    </div>

</div>
<div class="row">
    <div class="col-md-12 col-sm-12">

        <div class="portlet light">
            <div class="portlet-title">
                <div class="caption capitalize font-color">
                    <i class="fa fa-globe font-color"></i>Attendance
                </div>
                <div class="actions">
                    <div class="btn-group">
                        <a class="btn color btn-outline btn-circle btn-sm" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" aria-expanded="true"> Actions
                            <i class="fa fa-angle-down"></i>
                        </a>
                        <ul class="dropdown-menu pull-right">
                            @if(Auth::user()->getProfile->sign_in_req ==1)
                                <li class="nav-item <?php if($i=='user_reports' && $j =='attendance'){?>active open<?php ;}?>">
                                    <a href="/admin/user_reports/attendance" class="nav-link ">
                                        <i class="fa fa-user font-color"></i>
                                        <span class="title font-color">Attendance</span>
                                    </a>
                                </li>
                            @endif
                            <li class="nav-item <?php if($i=='user_reports' && $j =='penalties'){?>active open<?php ;}?>">
                                <a href="/admin/user_reports/penalties" class="nav-link">
                                    <i class="fa fa-minus-circle "></i>
                                    <span class="title">Penalties</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="portlet-body">

                <table class="table table-striped table-bordered table-hover table-dt" id="table-dt" >
                    <thead>
                    <tr class="tr-head font-color">
                        <th valign="middle">Date</th>
                        <th valign="middle">Sign in time</th>
                        <th valign="middle">Sign out time</th>
                        <th valign="middle">Status </th>
                        <th valign="middle">From Home</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($sign_ins as $sign_in)
                        <tr class="odd gradeX" id="data-row-{{$sign_in->id}}">
                            <td valign="middle">

                                {{ Carbon\Carbon::parse($sign_in->date)->format('l d, M Y') }}

                            </td>
                            <td valign="middle">

                                <?php if(isset($sign_in->sign_in_time)){?>
                                {{ Carbon\Carbon::parse($sign_in->sign_in_time)->format('l h:i:s A') }}
                                <?php ;}?>
                            </td>
                            <td valign="middle">

                                <?php if(isset($sign_in->sign_out_time)){?>
                                {{ Carbon\Carbon::parse($sign_in->sign_out_time)->format('l h:i:s A') }}
                                <?php ;}?>
                            </td>
                            <td valign="middle">
                                {{$sign_in->status}}
                            </td>
                            <td valign="middle">
                                <?php if($sign_in->from_home == 1){?>
                                Yes
                                <?php ;}else{?>
                                No
                                <?php ;}?>

                            </td>
                        </tr>
                    @endforeach

                </table>




            </div>
        </div>
    </div>
</div>