<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="portlet light bordered">
            <div class="portlet-title tabbable-line">
                <div class="caption">
                    <i class=" icon-social-twitter font-color hide"></i>
                    <span class="caption-subject font-color bold uppercase">Today Attendance</span>
                </div>
                <ul class="nav nav-tabs">
                    <li class="active">
                        <a href="#tab_actions_pending" data-toggle="tab"> {{$sign_in_count}} </a>
                    </li>

                </ul>
            </div>
            <div class="portlet-body">
                <div class="tab-content">
                    <div class="tab-pane active" id="tab_actions_pending">
                        <!-- BEGIN: Actions -->
                        <div class="mt-actions">
                            @foreach($sign_ins as $sign_in)
                                <div class="mt-action">
                                    <div class="mt-action-img">
                                        <img src="/images/user.png" width="45"> </div>
                                    <div class="mt-action-body">
                                        <div class="mt-action-row">
                                            <div class="mt-action-info ">
                                                <div class="mt-action-icon ">
                                                    <i class="icon-user hide"></i>
                                                </div>
                                                <div class="mt-action-details ">
                                                    <span class="mt-action-author">{{$sign_in->name}}</span>
                                                    <p class="mt-action-desc">{{$sign_in->position}}</p>
                                                </div>
                                            </div>
                                            <div class="mt-action-datetime ">
                                                                <span class="mt-action-date">
                                                                     {{ Carbon\Carbon::parse($sign_in->date)->format('d, M') }}
                                                                </span>
                                                <span class="mt-action-dot bg-green"></span>
                                                <span class="mt=action-time">
                                                                    {{ Carbon\Carbon::parse($sign_in->sign_in_time)->format(' h:i:s A') }}
                                                    -{{ Carbon\Carbon::parse($sign_in->sign_out_time)->format(' h:i:s A') }}
                                                                    </span>
                                            </div>
                                            <div class="mt-action-buttons ">
                                                <div class="btn-group btn-group-circle">
                                                    <div class="mt-comment-details">
                                                        @if($sign_in->status != "Ontime")
                                                            <span class="mt-comment-status mt-comment-status-rejected red">{{$sign_in->status}}</span>
                                                        @else
                                                            <span class="mt-comment-status">{{$sign_in->status}}</span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        @endforeach
                        <!-- END: Completed -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12 col-sm-12">
        <div class="portlet light bordered">
            <div class="portlet-title tabbable-line">
                <div class="caption">
                    <i class=" icon-social-twitter font-color hide"></i>
                    <span class="caption-subject font-color bold uppercase">Weekly Attendance</span>
                </div>
            </div>
            <div class="portlet-body">
                <div id="chartdiv"></div>
            </div>
        </div>

    </div>

</div>

<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="portlet light bordered">
            <div class="portlet-title tabbable-line">
                <div class="caption">
                    <i class=" icon-social-twitter font-color hide"></i>
                    <span class="caption-subject font-color bold uppercase">Weekly Sign In Status</span>
                </div>

            </div>
            <div class="portlet-body">
                <div id="chartdiv2"></div>
            </div>
        </div>

    </div>
    <div class="col-md-12 col-sm-12">
        <div class="portlet light bordered">
            <div class="portlet-title tabbable-line">
                <div class="caption">
                    <i class=" icon-social-twitter font-color hide"></i>
                    <span class="caption-subject font-color bold uppercase">Today No Sign Out</span>
                </div>
                <ul class="nav nav-tabs">
                    <li class="active">
                        <a href="#tab_actions_pending" data-toggle="tab"> {{$no_sign_out_count}} </a>
                    </li>

                </ul>
            </div>
            <div class="portlet-body">
                <table class="table table-striped table-bordered table-hover table-dt" id="table-dt" >
                    <thead>
                    <tr class="tr-head font-color">
                        <th valign="middle">Employee</th>
                        {{--<th valign="middle">Date</th>--}}
                        <th valign="middle">Sign in time</th>
                        <th valign="middle">
                            Status</th>
                        <th valign="middle">From home </th>
                        <th valign="middle">Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($no_sign_outs as $no_sign_out)
                        <tr class="odd gradeX" id="data-row-{{$no_sign_out->id}}">
                            <td valign="middle">{{$no_sign_out->name}}</td>
                            {{--<td valign="middle">--}}
                                {{--{{ Carbon\Carbon::parse($no_sign_out->date)->format('l d, M Y') }}--}}
                            {{--</td>--}}
                            <td valign="middle">
                                <?php if(isset($no_sign_out->sign_in_time)){?>
                                {{ Carbon\Carbon::parse($no_sign_out->sign_in_time)->format('l h:i:s A') }}
                                <?php ;}?>
                            </td>
                            <td valign="middle">
                                {{$no_sign_out->status}}
                            </td>
                            <td valign="middle"><?php if($no_sign_out->from_home == 1){?>
                                Yes
                                <?php ;}else{?>
                                No
                                <?php ;}?></td>
                            <td valign="middle">@if((Auth::user()->role_id == 2 || Auth::user()->role_id == 1) && $no_sign_out->status != "Leave")<a href="{{route('admin.sign_ins.edit',['id'=> $no_sign_out->id])}}" class="btn btn-edit pjax-link" ><i class="fa fa-edit"></i> Edit</a> @endif    </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>


        </div>

    </div>


</div>
<div class="row">
  <div class="col-md-12 col-sm-12">
      <div class="portlet light bordered">
          <div class="portlet-title tabbable-line">
              <div class="caption">
                  <i class=" icon-social-twitter font-color hide"></i>
                  <span class="caption-subject font-color bold uppercase">Pending Requests</span>
              </div>
              <ul class="nav nav-tabs">
                  <li class="active">
                      <a href="#tab_actions_pending" data-toggle="tab"> Pending </a>
                  </li>

              </ul>
          </div>
          <div class="portlet-body">
              <table class="table table-striped table-bordered table-hover table-dt" id="table-dt" >
                  <thead>
                  <tr class="tr-head font-color">
                      <th valign="middle">Employee</th>
                      <th valign="middle">Type</th>
                      <?php if($type == "Permissions"  || $type == "All" || $type == "Half Day" || $type == "Home" || $type == "Overtime"){?>
                      <th valign="middle">Day</th>
                      <?php ;}?>
                      <?php if($type == "Permissions"  || $type == "All" || $type == "Overtime" || $type == "Normal Leaves" || $type == "Emergency Leaves"){?>
                      <th valign="middle">From </th>
                      <th valign="middle">To </th>
                      <th valign="middle">Interval</th>
                      <?php ;}?>

                      <?php if($type == "Half Day"  || $type == "All" || $type == "Home"){?>
                      <th valign="middle">
                          @if($type == "Half Day")
                              Half
                          @endif
                          @if($type == "Home")
                              Exchanged day
                          @endif
                          @if($type == "All")
                              Notes
                          @endif
                      </th>
                      <?php ;}?>
                      <th valign="middle">
                          Requested at
                      </th>
                      <th valign="middle">Status</th>
                      <th valign="middle">
                          Action
                      </th>
                  </tr>
                  </thead>
                  <tbody>
                  @foreach($reqs as $req)
                      <tr class="odd gradeX" id="data-row-{{$req->id}}">
                          <td valign="middle">
                              {{$req->name}}

                          </td>
                          <td valign="middle">
                              <?php $req_type = str_replace("ves","ve",$req->type);
                              $req_type= str_replace("ns","n",$req_type);
                              echo str_replace("Home","Work From Home",$req_type);
                              ?>
                          </td>
                          <?php if($type == "Permissions"  || $type == "All" || $type == "Half Day"  || $type == "Home" || $type == "Overtime"){?>
                          <td valign="middle">{{$req->req_day}}</td>
                          <?php ;}?>
                          <?php if($type == "Permissions"  || $type == "All" || $type == "Overtime" || $type == "Normal Leaves" || $type == "Emergency Leaves"){?>
                          <td valign="middle">
                              <?php if($req->type == "Emergency Leaves" || $req->type == "Normal Leaves"){?>
                  <?php echo date("Y-m-d",strtotime($req->time_from));?>
                  <?php ;}else{?>
                              {{$req->time_from}}
                              <?php ;}?>

                          </td>
                          <td valign="middle">
                              <?php if($req->type == "Emergency Leaves" || $req->type == "Normal Leaves"){?>
                  <?php echo date("Y-m-d",strtotime($req->time_to));?>
                  <?php ;}else{?>
                              {{$req->time_to}}
                              <?php ;}?>


                          </td>
                          <td valign="middle">
                              <?php if(isset($req->interval_in_days)){?>
                              {{$req->interval_in_days}} Day(s)
                              <?php ;}?>
                              <?php if(isset($req->interval_in_time)){?>
                              {{$req->interval_in_time}}
                              <?php ;}?>
                              <?php if($req->type == "Overtime" && $req->status == 2){?>
                              <form action="{{route('admin.reqs.edit_signout_interval')}}" method="post" class="ajax_form">
                                  <input type="hidden" name="id" value="{{$req->id}}" />
                                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                      <tr>
                                          <td align="left" valign="middle"><input name="interval_in_time" class="form-control timepicker timepicker-24" data-validation="required" data-name="From" value="{{$req->interval_in_time}}" >
                                              <span class="error_message"></span></td>
                                          <td width="50" align="left" valign="middle"><button type="submit" class="btn btn-lg color"  ><i class="fa fa-check"></i></button></td>
                                      </tr>
                                  </table>



                              </form>
                              <?php ;}?>

                          </td>
                          <?php ;}?>

                          <?php if($type == "Half Day" || $type == "Home" || $type == "All"){?>
                          <td valign="middle">{{$req->add_info}}</td>
                          <?php ;}?>
                          <td valign="middle">
                              {{$req->created_at}}
                          </td>
                          <td valign="middle">
                              <?php $req_status = str_replace("1","Accepted", $req->status);
                              $req_status = str_replace("2","Pending", $req_status);
                              echo str_replace("0","Rejected", $req_status);
                              ?>

                          </td>
                          <td valign="middle">
                              @if($req->status == 2 /*&& $type != "All"*/)
                                 {{--<div class="btn-group btn-group-circle">--}}
                                      <a href="#" data-action="{{route('admin.reqs.accept',['id'=>$req->id])}}" class="btn btn-outline color btn-sm" ><i class="fa fa-check"></i> Accept</a>
                                      <a href="#" data-action="{{route('admin.reqs.reject',['id'=>$req->id])}}" class="btn btn-outline btn-delete btn-sm" ><i class="fa fa-remove"></i> Reject</a>
                                  {{--</div>--}}
                              @endif
                          </td>
                      </tr>
                  @endforeach

                  </tbody>
              </table>
          </div>
      </div>
  </div>
</div>