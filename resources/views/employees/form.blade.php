<form action="{{route('admin.employees.save')}}" method="post" class="ajax_form j-forms larajsval form" id="main-form" novalidate>
    <div class="header">
        <p>
             @if(empty($employee->name))<i class="fa fa-user"></i> New employee @else <i class="fa fa-user"></i> {{$employee->name}} @endif
        </p>
    </div>

    <div class="content">
        
            
            
            
            
            <div class="unit">
                <label class="label">Employee Type</label>
                <label class="input select">
                    <select name="role_id" data-validation="required" data-name="Type" class="form-control" >
                        <option value="">Select Type</option>
                        
                            <option  value="1" <?php if( $employee->role_id == 1){?>selected<?php ;}?>>Super admin</option>
                            <option  value="2" <?php if( $employee->role_id == 2){?>selected<?php ;}?>>Normal employee</option>
                    </select>
                    <span class="error_message"></span>
                    <i></i>
                </label>
            </div>
            <div class="unit">
                <label class="label">Gender</label>
                <label class="input select">
                    <select name="gender" data-validation="required" data-name="Gender" class="form-control" >
                        <option value="">Select Gender</option>
                        
                            <option  value="Male" <?php if( $employee->gender == "Male"){?>selected<?php ;}?>>Male</option>
                            <option  value="Female" <?php if( $employee->gender == "Female"){?>selected<?php ;}?>>Female</option>
                    </select>
                    <span class="error_message"></span>
                    <i></i>
                </label>
            </div>
            
            <div class="unit">
                <label class="label">Name</label>
                <div class="input">
                    <label class="icon-right">
                        <i class="fa fa-anchor"></i>
                    </label>
                    <input name="name" type="text" class="form-control" data-validation="required" data-name="Name" value="{{$employee->name}}"  >
                    <span class="error_message"></span>
                </div>
            </div>
            <div class="unit">
                <label class="label">Email</label>
                <div class="input">
                    <label class="icon-right">
                        <i class="fa fa-envelope"></i>
                    </label>
                    <input name="email" type="text" data-validation="required,email" data-name="Email"  class="form-control" value="{{$employee->email}}"  >
                    <span class="error_message"></span>
                </div>
            </div>
            <div class="unit">
                <label class="label">Phone</label>
                <div class="input">
                    <label class="icon-right">
                        <i class="fa fa-phone"></i>
                    </label>
                    <input name="phone" type="text" data-validation="required,number" data-name="Phone" class="form-control" value="{{$employee->phone}}"  >
                    <span class="error_message"></span>
                </div>
            </div>
            <div class="unit">
                <label class="label">Fax</label>
                <div class="input">
                    <label class="icon-right">
                        <i class="fa fa-phone"></i>
                    </label>
                    <input name="fax" type="text" data-validation="number" data-name="Fax" class="form-control" value="{{$employee->fax}}"  >
                    <span class="error_message"></span>
                </div>
            </div>
            <div class="unit">
                <label class="label">Date of birth</label>
                <div class="input">
                    <label class="icon-right">
                        <i class="fa fa-calendar"></i>
                    </label>
                    <input name="date_of_birth" type="text" data-validation="" data-name="Date of birth" class="form-control datePicker" value="<?php if($employee->date_of_birth!= ""){echo date("m/d/Y",strtotime($employee->date_of_birth)) ;}?>"  >
                    <span class="error_message"></span>
                </div>
            </div>
            <div class="unit">
                <label class="label">Joining Date</label>
                <div class="input">
                    <label class="icon-right">
                        <i class="fa fa-calendar"></i>
                    </label>
                    <input name="joining_date" type="text" data-validation="required" data-name="Joining Date" class="form-control datePicker" value="<?php if($employee->joining_date!= ""){echo date("m/d/Y",strtotime($employee->joining_date)) ;}?>"  >
                    <span class="error_message"></span>
                </div>
            </div>
            <div class="unit">
                <label class="label">password</label>
                <div class="input">
                    <label class="icon-right">
                        <i class="fa fa-lock"></i>
                    </label>
                    <input name="password" data-validation="required" data-name="password" type="password" class="form-control" value="{{$employee->dec_password}}"  >
                    <span class="error_message"></span>
                </div>
            </div>
            <div class="unit">
                <label class="label">Retype Password</label>
                <div class="input">
                    <label class="icon-right">
                        <i class="fa fa-lock"></i>
                    </label>
                    <input name="password2" data-validation="required,match.password" data-name="password" type="password" class="form-control" value="{{$employee->dec_password}}" >
                    <span class="error_message"></span>
                </div>
            </div>
            <input type="hidden" name="id" id="id" value="{{$employee->id}}"  data-validation="" data-name="id"  />
    </div>
    <div id="response"></div>
    <div class="footer">
        <button type="submit" class="btn btn-lg color" ><i class="fa fa-edit"></i> Save</button>

    </div>
    <input type="reset" class="hide resetForm" >

</form>


