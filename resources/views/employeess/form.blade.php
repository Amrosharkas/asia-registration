<form action="{{route('admin.employees.save')}}" method="post" class="ajax_form j-forms larajsval form" id="main-form" novalidate>
    <div class="header">
        <p>
             @if(empty($employee->name))<i class="fa fa-user"></i> New Employee @else <i class="fa fa-user"></i> {{$employee->name}} @endif
        </p>
    </div>

    <div class="content">
        @if(Auth::user()->role_id == 1)
            <div class="unit">
                <label class="label">Office</label>
                <label class="input select">
                    <select name="office_id" data-validation="required" data-name="Office" class="form-control">
                        <option value="" @if(empty($employee->office_id)) selected @endif >Select Office</option>
                        @foreach($offices as $office)
                            <option value="{{$office->id}}" <?php if($office->id == $employee->office_id){?>selected<?php ;}?>>{{$office->name}}</option>
                        @endforeach
                    </select>
                    <span class="error_message"></span>
                    <i></i>
                </label>
            </div>
        @endif
        @if(Auth::user()->role_id == 2)
                <input type="hidden" name="office_id" id="office_id" value="{{Auth::user()->office_id}}"  data-validation="" data-name="office_id"  />
            @endif
            <div class="unit">
                <label class="label">Department</label>
                <label class="input select">
                    <select name="department_id" data-validation="required" data-name="Department" class="form-control options_dependent" data-depending-on="office_id">
                        <option value="" @if(empty($employee->department_id)) selected @endif >Select Department</option>
                        @foreach($departments as $department)
                            <option   data-depending-value="{{$department->office_id}}" value="{{$department->id}}" <?php if($department->id == $employee->department_id){?>selected<?php ;}?>>{{$department->name}}</option>
                        @endforeach
                    </select>
                    <span class="error_message"></span>
                    <i></i>
                </label>
            </div>
            <div class="unit">
                <label class="label">Role</label>
                <label class="input select">
                    <select name="role_id" data-validation="required" data-name="Role" class="form-control">
                        <option value="" @if(empty($employee->role_id)) selected @endif>Select Role</option>
                        @foreach($roles as $role)
                            <option  value="{{$role->id}}" <?php if($role->id == $employee->role_id){?>selected<?php ;}?>>{{$role->name}}</option>
                        @endforeach
                    </select>
                    <span class="error_message"></span>
                    <i></i>
                </label>
            </div>
            <div class="unit">
                <label class="label">Position</label>
                <label class="input select">
                    <select name="position_id" data-validation="required" data-name="Position" class="form-control options_dependent" data-depending-on="office_id">
                        <option value="" @if(empty($position->id)) selected @endif >Select Position</option>
                        @foreach($positions as $position)
                            <option data-depending-value="{{$position->office_id}}" value="{{$position->id}}" <?php if($position->id == $employee->position_id){?>selected<?php ;}?>>{{$position->name}}</option>
                        @endforeach
                    </select>
                    <span class="error_message"></span>
                    <i></i>
                </label>
            </div>
            <div class="unit">
                <label class="label">Attendance Profile</label>
                <label class="input select">
                    <select name="att_profile_id" data-validation="required" data-name="Profile" class="form-control options_dependent" data-depending-on="office_id">
                        <option value="" @if(empty($profile->id)) selected @endif >Select Profile</option>
                        @foreach($profiles as $profile)
                            <option data-depending-value="{{$profile->office_id}}"  value="{{$profile->id}}" <?php if($profile->id == $employee->att_profile_id){?>selected<?php ;}?>>{{$profile->name}}</option>
                        @endforeach
                    </select>
                    <span class="error_message"></span>
                    <i></i>
                </label>
            </div>
            <div class="unit">
                <label class="label">Direct Manager</label>
                <label class="input select">
                    <select name="manager_id" data-validation="required" data-name="Manager" class="form-control options_dependent" data-depending-on="office_id">
                        <option value="" @if(empty($manager->id)) selected @endif>Select Manager</option>
                        @foreach($managers as $manager)
                            <option data-depending-value="<?php if($manager->office_id == 0){?>force_show<?php ;}else{?>{{$manager->office_id}}<?php ;}?>" value="{{$manager->id}}" <?php if($manager->id == $employee->manager_id){?>selected<?php ;}?>>{{$manager->name}}</option>
                        @endforeach
                    </select>
                    <span class="error_message"></span>
                    <i></i>
                </label>
            </div>
            <div class="unit">
                <label class="label">Copied Managers</label>
                <label class="input select">
                    <select name="copied_managers[]" data-validation="required" data-name="Manager" class="form-control  select2-multiple select2 copied_managers options_dependent" data-depending-on="office_id" multiple>
                        @foreach($managers as $manager)
                            <option data-depending-value="<?php if($manager->office_id == 0){?>force_show<?php ;}else{?>{{$manager->office_id}}<?php ;}?>" value="{{$manager->id}}" <?php if(in_array($manager->id, $entries) ){?>selected="selected"<?php ;}?>>{{$manager->name}}</option>
                        @endforeach
                    </select>
                    <span class="error_message"></span>
                    <i></i>
                </label>
            </div>
            <div class="unit">
                <label class="label">Name</label>
                <div class="input">
                    <label class="icon-right">
                        <i class="fa fa-anchor"></i>
                    </label>
                    <input name="name" type="text" class="form-control" data-validation="required" data-name="Name" value="{{$employee->name}}"  >
                    <span class="error_message"></span>
                </div>
            </div>
            <div class="unit">
                <label class="label">Email</label>
                <div class="input">
                    <label class="icon-right">
                        <i class="fa fa-envelope"></i>
                    </label>
                    <input name="email" type="text" data-validation="required,email" data-name="Email"  class="form-control" value="{{$employee->email}}"  >
                    <span class="error_message"></span>
                </div>
            </div>
            <div class="unit">
                <label class="label">Phone</label>
                <div class="input">
                    <label class="icon-right">
                        <i class="fa fa-phone"></i>
                    </label>
                    <input name="phone" type="text" data-validation="required,number" data-name="Phone" class="form-control" value="{{$employee->phone}}"  >
                    <span class="error_message"></span>
                </div>
            </div>
            <div class="unit">
                <label class="label">password</label>
                <div class="input">
                    <label class="icon-right">
                        <i class="fa fa-lock"></i>
                    </label>
                    <input name="password" data-validation="required" data-name="password" type="password" class="form-control" value="{{$employee->dec_password}}"  >
                    <span class="error_message"></span>
                </div>
            </div>
            <div class="unit">
                <label class="label">Retype Password</label>
                <div class="input">
                    <label class="icon-right">
                        <i class="fa fa-lock"></i>
                    </label>
                    <input name="password2" data-validation="required,match.password" data-name="password" type="password" class="form-control" value="{{$employee->dec_password}}" >
                    <span class="error_message"></span>
                </div>
            </div>
            <div class="unit">
                <label class="label">Start calculating absence for this user from</label>
                <div class="input">
                    <label class="icon-right">
                        <i class="fa fa-calendar"></i>
                    </label>
                    <input name="starts_at" data-validation="required" data-name="Thi field" type="text" class="form-control datePicker" value="<?php if(isset($employee->starts_at)){echo date("m/d/Y",strtotime($employee->starts_at));}?>" >
                    <span class="error_message"></span>
                </div>
            </div>
            <div class="divider-text gap-top-20 gap-bottom-45">
                <span>Work From Home Policy</span>
            </div>
            <div class="unit">
                <label class="label">Can Work From Home</label>
                <label class="input select">
                    <select name="can_work_home" data-validation="required" data-name="This field" class="form-control">
                        <option value="">Select Option</option>
                        <option value="1" <?php if(1 == $employee->can_work_home){?>selected<?php ;}?>>Yes</option>
                        <option value="0" <?php if(0 == $employee->can_work_home){?>selected<?php ;}?>>No</option>
                    </select>
                    <span class="error_message"></span>
                    <i></i>
                </label>
            </div>
            <div class="form-group dependent" data-depending-on= "can_work_home" data-depending-value="1">
                <div class="unit">
                    <label class="label">Maximum Days Per Week</label>
                    <div class="input">
                        <label class="icon-right">
                            <i class="fa fa-home"></i>
                        </label>
                        <input name="max_homeDays_per_week" type="text" class="form-control" data-validation="required,number" data-name="This Field" value="{{$employee->max_homeDays_per_week}}" >
                        <span class="error_message"></span>
                    </div>
                </div>
                <div class="unit">
                    <label class="label">Allow Flexible Work From Home System</label>
                    <label class="input select">
                        <select name="flexible_home" data-validation="required" data-name="This field" class="form-control">
                            <option value="">Select Option</option>
                            <option value="1" <?php if(1 == $employee->flexible_home){?>selected<?php ;}?>>Yes</option>
                            <option value="0" <?php if(0 == $employee->flexible_home){?>selected<?php ;}?>>No</option>
                        </select>
                        <span class="error_message"></span>
                        <i></i>
                    </label>
                </div>
            </div>
            <div class="form-group dependent" data-depending-on= "flexible_home" data-depending-value="0">
                <div class="unit">
                    <label class="label">Can Exchange From Home Days</label>
                    <label class="input select">
                        <select name="can_ex_days" data-validation="required" data-name="This field" class="form-control">
                            <option value="">Select Option</option>
                            <option value="1" <?php if(1 == $employee->can_ex_days){?>selected<?php ;}?>>Yes</option>
                            <option value="0" <?php if(0 == $employee->can_ex_days){?>selected<?php ;}?>>No</option>
                        </select>
                        <span class="error_message"></span>
                        <i></i>
                    </label>
                </div>
            </div>
            <div class="form-group dependent" data-depending-on= "flexible_home" data-depending-value="0">
                <div class="unit">
                    <label class="label">Default Work from home days</label>
                    <label class="input select">
                        <select id="home_days" name="home_days[]" data-validation="required" data-name="Home Days" class="form-control select2-multiple select2" multiple>
                            <optgroup label="Weekends">
                                <option value="Saturday" <?php  if (strpos($employee->home_days, 'Saturday') !== false) {?>selected="selected"<?php ;}?>>Saturday</option>
                                <option value="Sunday" <?php  if (strpos($employee->home_days, 'Sunday') !== false) {?>selected="selected"<?php ;}?>>Sunday</option>
                                <option value="Monday" <?php  if (strpos($employee->home_days, 'Monday') !== false) {?>selected="selected"<?php ;}?>>Monday</option>
                                <option value="Tuesday" <?php  if (strpos($employee->home_days, 'Tuesday') !== false) {?>selected="selected"<?php ;}?>>Tuesday</option>
                                <option value="Wednesday" <?php  if (strpos($employee->home_days, 'Wednesday') !== false) {?>selected="selected"<?php ;}?>>Wednesday</option>
                                <option value="Thursday" <?php  if (strpos($employee->home_days, 'Thursday') !== false) {?>selected="selected"<?php ;}?>>Thursday</option>
                                <option value="Friday" <?php  if (strpos($employee->home_days, 'Friday') !== false) {?>selected="selected"<?php ;}?>>Friday</option>
                            </optgroup>
                        </select>
                        <span class="error_message"></span>
                        <i></i>
                    </label>
                </div>
            </div>
            <input type="hidden" name="id" id="id" value="{{$employee->id}}"  data-validation="" data-name="id"  />
            <input type="hidden" name="company_id" id="company_id" value="{{Auth::user()->company_id}}"  data-validation="" data-name="company_id"  />
    </div>
    <div id="response"></div>
    <div class="footer">
        <button type="submit" class="btn btn-lg color" ><i class="fa fa-edit"></i> Save</button>

    </div>
    <input type="reset" class="hide resetForm" >

</form>


