<div class="portlet box grey-cascade">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-globe"></i>Employees
        </div>
        <div class="tools">
            <a href="javascript:;" class="collapse">
            </a>
        </div>
    </div>
    <div class="portlet-body">
      <div class="table-toolbar">
            <div class="row">
                <div class="col-md-6">
                    <div class="btn-group">
                    <a href="{{route('admin.employees.init')}}" class="pjax-link" >
                        <button class="btn green" id="add_new" >

                            Add New <i class="fa fa-plus"></i>
                        </button>      
                      </a>                  
                    </div>
                </div>
                <div class="col-md-6"></div>
            </div>
        </div>
        <table class="table table-striped table-bordered table-hover table-dt" id="table-dt" >
            <thead>
                <tr class="tr-head">
                  <th valign="middle">
                      Name
                  </th>
                  <th valign="middle">Office</th>
                  <th valign="middle">Manager</th>
                  <th valign="middle">Position</th>
                  <th valign="middle">Role</th>
                  <th valign="middle">Department</th>
                  <th valign="middle">Email</th>
                  <th valign="middle">Phone</th>
                    <th valign="middle">
                        Created at
                    </th>
                    <th valign="middle">
                        Action
                    </th>
                </tr>
            </thead>
            <tbody>
                @foreach($employees as $employee)
                <tr class="odd gradeX" id="data-row-{{$employee->id}}">
                  <td valign="middle">
                      {{$employee->name}}
                    </td>
                  <td valign="middle"><?php if(isset($employee->getOffice->name)){?>{{$employee->getOffice->name}}<?php ;}?></td>
                  <td valign="middle"><?php if(isset($employee->getManager->name)){?>{{$employee->getManager->name}}<?php ;}?></td>
                  <td valign="middle"><?php if(isset($employee->getPosition->name)){?>{{$employee->getPosition->name}}<?php ;}?></td>
                  <td valign="middle"><?php if(isset($employee->getRole->name)){?>{{$employee->getRole->name}}<?php ;}?></td>
                  <td valign="middle"><?php if(isset($employee->getDepartment->name)){?>{{$employee->getDepartment->name}}<?php ;}?></td>
                  <td valign="middle">{{$employee->email}}</td>
                  <td valign="middle">{{$employee->phone}}</td>
                    <td valign="middle">
                        {{$employee->created_at}}
                    </td>
                    <td valign="middle">
                    	<a href="{{route('admin.employees.profile',['id'=>$employee->id])}}" class="btn blue pjax-link" ><i class="fa fa-eye"></i> Profile</a> 
                        <a href="{{route('admin.employees.edit',['id'=>$employee->id])}}" class="btn green pjax-link" ><i class="fa fa-edit"></i> Edit</a> 
                        @if(Auth::user()->role_id < $employee->role_id)
                        <a href="#" data-action="{{route('admin.employees.delete',['id'=>$employee->id])}}"  class="btn red delete_single" ><i class="fa fa-remove"></i> Delete</a> 
                        @endif
                    </td>
                </tr>
                @endforeach

        </table>
    </div>
</div>