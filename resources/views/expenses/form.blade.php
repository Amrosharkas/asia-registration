<form action="{{route('admin.expenses.save')}}" method="post" class="ajax_form j-forms larajsval form" id="main-form" novalidate>
    <div class="header">
        <p>
             @if(empty($expense->name))<i class="fa fa-user"></i> New expense @else <i class="fa fa-user"></i> {{$expense->name}} @endif
        </p>
    </div>

    <div class="content">
        
            
            
            
            @can('super_user')
            <div class="unit">
                <label class="label">Privacy</label>
                <label class="input select">
                    <select name="view_type" data-validation="" data-name="Privacy" class="form-control" >
                        
                        
                            <option  value="public" <?php if( $expense->view_type == "public"){?>selected<?php ;}?>>Public</option>
                            <option  value="private" <?php if( $expense->view_type == "private"){?>selected<?php ;}?>>Private</option>
                            
                            
                            
                    </select>
                    <input name="new_type" type="text" data-validation="" data-name="New type" class="form-control new_type" value=""  style="display:none;"  >
                    <span class="error_message"></span>
                    <i></i>
                </label>
            </div>
            @endcan
            <div class="unit">
                <label class="label">Type</label>
                <label class="input select">
                    <select name="type" data-validation="" data-name="Type" class="form-control" >
                        <option value="">Select Type</option>
                        
                            <option  value="Trainers & Consultants Payment" <?php if( $expense->type == "Trainers & Consultants Payment"){?>selected<?php ;}?>>Trainers & Consultants Payment</option>
                            
                            <option  value="Training Expenses" <?php if( $expense->type == "Training Expenses"){?>selected<?php ;}?>>Training Expenses</option>
                            <option  value="Others" <?php if( $expense->type == "Others"){?>selected<?php ;}?>>Others</option>
                            @foreach($types as $type)
                            <option  value="{{$type->type}}" <?php if( $expense->type == $type->type){?>selected<?php ;}?>>{{$type->type}}</option>
                            @endforeach
                            <option value="New type">New Type</option>
                            
                    </select>
                    <input name="new_type" type="text" data-validation="" data-name="New type" class="form-control new_type" value=""  style="display:none;"  >
                    <span class="error_message"></span>
                    <i></i>
                </label>
            </div>
            <div class="unit dependent" data-depending-on= "type" data-depending-value="Trainers & Consultants Payment">
                <label class="label">Going For</label>
                <label class="input select">
                    <select name="trainer_id" data-validation="" data-name="This field" class="form-control select2" >
                        <option value="" >Select One</option>
                        @foreach($trainers as $trainer)
                            <option  value="{{$trainer->id}}" <?php if($trainer->id == $expense->trainer_id){?>selected<?php ;}?>>{{$trainer->name}}</option>
                        @endforeach
                    </select>
                    <span class="error_message"></span>
                    <i></i>
                </label>
            </div>
            
            <div class="unit dependent" data-depending-on= "type" data-depending-value="Trainers & Consultants Payment,Training Expenses">
                <label class="label">Event</label>
                <label class="input select">
                    <select name="schedule_id" data-validation="" data-name="This field" class="form-control select2" >
                        <option value="" >Select One</option>
                        @foreach($schedules as $schedule)
                            <option  value="{{$schedule->id}}" <?php if($schedule->id == $expense->schedule_id){?>selected<?php ;}?>><?php if(isset($schedule->getCourse->name)){?>{{$schedule->getCourse->name}} <?php ;}?> ( <?php echo date("d M Y",strtotime($schedule->start_date));?> )</option>
                        @endforeach
                    </select>
                    <span class="error_message"></span>
                    <i></i>
                </label>
            </div>
            
            
            
            
            <div class="unit">
                <label class="label">Amount</label>
                <div class="input">
                    <label class="icon-right">
                        <i class="fa fa-money"></i>
                    </label>
                    <input name="amount" type="text" data-validation="required,number" data-name="Amount" class="form-control" value="{{$expense->amount}}"  >
                    <span class="error_message"></span>
                </div>
            </div>
            <div class="unit">
                <label class="label">Date</label>
                <div class="input">
                    <label class="icon-right">
                        <i class="fa fa-calendar"></i>
                    </label>
                    <input name="expense_date" type="text" data-validation="required" data-name="Date" class="form-control datePicker" value="<?php if($expense->expense_date!=""){ echo date("m/d/Y",strtotime($expense->expense_date));}?>"  >
                    <span class="error_message"></span>
                </div>
            </div>
            <div class="unit">
                <label class="label">Description</label>
                <div class="input">
                    <label class="icon-right">
                        <i class="fa fa-edit"></i>
                    </label>
                    <textarea name="description"  data-validation="" data-name="Description" class="form-control"   >{{$expense->description}}</textarea>
                    <span class="error_message"></span>
                </div>
            </div>
            
            
            
            
            
            
            
            
            <input type="hidden" name="id" id="id" value="{{$expense->id}}"  data-validation="" data-name="id"  />
    </div>
    <div id="response"></div>
    <div class="footer">
        <button type="submit" class="btn btn-lg color" ><i class="fa fa-edit"></i> Save</button>

    </div>
    <input type="reset" class="hide resetForm" >

</form>


