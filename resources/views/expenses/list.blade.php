<div class="portlet light">
    <div class="portlet-title">
        <div class="caption font-color">
            <i class="fa fa-user font-color"></i>Expenses
        </div>
        <div class="btn-group pull-right">
                    
                      
                    <a href="{{route('admin.expenses.init')}}" class="pjax-link"  >
                        <button type="button" class="btn color" id="add_new" >
                            Add New <i class="fa fa-plus"></i>
                        </button>      
                      </a> 
                      
                                       
                    </div>
    </div>
    <div class="portlet-body">
    <form class="ajax_form" method="post" action="/admin/schedules/recommend">
      <div class="table-toolbar" >
            <div class="row" >
                <div class="col-md-5 hide" >
                	<div class="btn-group">
                    
                    
                              
                      
                      
                    
                      
                                       
                  </div>    
                </div>
                <div class="col-md-5" >
                    <div class="btn-group"></div>
                </div>
                
            </div>
        </div>
        <table class="table table-striped table-bordered table-hover table-dt" id="table-dt" >
            <thead>
                <tr class="tr-head">
                  <th valign="middle">
                      Date
                  </th>
                  <th valign="middle">Type</th>
                  <th valign="middle">Amount</th>
                    <th valign="middle">
                        Action
                    </th>
                </tr>
            </thead>
            <tbody>
                @foreach($expenses as $expense)
                <tr class="odd gradeX" id="data-row-{{$expense->id}}">
                  <td valign="middle">
                      
                      <?php echo date("l d M Y",strtotime($expense->expense_date));?>
                    </td>
                    <td valign="middle">{{$expense->type}}</td>
                  <td valign="middle">{{$expense->amount}}</td>
                    <td valign="middle">
                    
 						@if(Auth::user()->role_id <= 1)
                        <a href="#" data-action="{{route('admin.expenses.delete',['id'=>$expense->id])}}"  class="btn red delete_single" ><i class="fa fa-remove"></i> Delete</a> 
                        @endif
                        
                        <a href="{{route('admin.expenses.edit',['id'=>$expense->id])}}" class="btn green pjax-link" ><i class="fa fa-edit"></i> Edit</a> 
                        
                    </td>
                </tr>
                @endforeach

        </table>
        </form>
    </div>
</div>