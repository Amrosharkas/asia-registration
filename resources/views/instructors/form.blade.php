<?php 
$nationalitites = array("Afghanistan", "Albania", "Algeria", "American Samoa", "Andorra", "Angola", "Anguilla", "Antarctica", "Antigua and Barbuda", "Argentina", "Armenia", "Aruba", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia", "Bosnia and Herzegowina", "Botswana", "Bouvet Island", "Brazil", "British Indian Ocean Territory", "Brunei Darussalam", "Bulgaria", "Burkina Faso", "Burundi", "Cambodia", "Cameroon", "Canada", "Cape Verde", "Cayman Islands", "Central African Republic", "Chad", "Chile", "China", "Christmas Island", "Cocos (Keeling) Islands", "Colombia", "Comoros", "Congo", "Congo, the Democratic Republic of the", "Cook Islands", "Costa Rica", "Cote d'Ivoire", "Croatia (Hrvatska)", "Cuba", "Cyprus", "Czech Republic", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "East Timor", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia", "Falkland Islands (Malvinas)", "Faroe Islands", "Fiji", "Finland", "France", "France Metropolitan", "French Guiana", "French Polynesia", "French Southern Territories", "Gabon", "Gambia", "Georgia", "Germany", "Ghana", "Gibraltar", "Greece", "Greenland", "Grenada", "Guadeloupe", "Guam", "Guatemala", "Guinea", "Guinea-Bissau", "Guyana", "Haiti", "Heard and Mc Donald Islands", "Holy See (Vatican City State)", "Honduras", "Hong Kong", "Hungary", "Iceland", "India", "Indonesia", "Iran (Islamic Republic of)", "Iraq", "Ireland", "Israel", "Italy", "Jamaica", "Japan", "Jordan", "Kazakhstan", "Kenya", "Kiribati", "Korea, Democratic People's Republic of", "Korea, Republic of", "Kuwait", "Kyrgyzstan", "Lao, People's Democratic Republic", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libyan Arab Jamahiriya", "Liechtenstein", "Lithuania", "Luxembourg", "Macau", "Macedonia, The Former Yugoslav Republic of", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Marshall Islands", "Martinique", "Mauritania", "Mauritius", "Mayotte", "Mexico", "Micronesia, Federated States of", "Moldova, Republic of", "Monaco", "Mongolia", "Montserrat", "Morocco", "Mozambique", "Myanmar", "Namibia", "Nauru", "Nepal", "Netherlands", "Netherlands Antilles", "New Caledonia", "New Zealand", "Nicaragua", "Niger", "Nigeria", "Niue", "Norfolk Island", "Northern Mariana Islands", "Norway", "Oman", "Pakistan", "Palau", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Pitcairn", "Poland", "Portugal", "Puerto Rico", "Qatar", "Reunion", "Romania", "Russian Federation", "Rwanda", "Saint Kitts and Nevis", "Saint Lucia", "Saint Vincent and the Grenadines", "Samoa", "San Marino", "Sao Tome and Principe", "Saudi Arabia", "Senegal", "Seychelles", "Sierra Leone", "Singapore", "Slovakia (Slovak Republic)", "Slovenia", "Solomon Islands", "Somalia", "South Africa", "South Georgia and the South Sandwich Islands", "Spain", "Sri Lanka", "St. Helena", "St. Pierre and Miquelon", "Sudan", "Suriname", "Svalbard and Jan Mayen Islands", "Swaziland", "Sweden", "Switzerland", "Syrian Arab Republic", "Taiwan, Province of China", "Tajikistan", "Tanzania, United Republic of", "Thailand", "Togo", "Tokelau", "Tonga", "Trinidad and Tobago", "Tunisia", "Turkey", "Turkmenistan", "Turks and Caicos Islands", "Tuvalu", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "United States", "United States Minor Outlying Islands", "Uruguay", "Uzbekistan", "Vanuatu", "Venezuela", "Vietnam", "Virgin Islands (British)", "Virgin Islands (U.S.)", "Wallis and Futuna Islands", "Western Sahara", "Yemen", "Yugoslavia", "Zambia", "Zimbabwe");

?>
<form action="{{route('admin.instructors.save')}}" method="post" class="ajax_form j-forms larajsval form" id="main-form" novalidate>
    <div class="header">
        <p>
             @if(empty($instructor->name))<i class="fa fa-user"></i> New instructor @else <i class="fa fa-user"></i> {{$instructor->name}} @endif
        </p>
    </div>

    <div class="content">
        
            
            <div class="unit">
                <label class="label">Gender</label>
                <label class="input select">
                    <select name="gender" data-validation="required" data-name="Gender" class="form-control" >
                        <option value="">Select Gender</option>
                        
                            <option  value="Male" <?php if( $instructor->gender == "Male"){?>selected<?php ;}?>>Male</option>
                            <option  value="Female" <?php if( $instructor->gender == "Female"){?>selected<?php ;}?>>Female</option>
                    </select>
                    <span class="error_message"></span>
                    <i></i>
                </label>
            </div>
            
            <div class="unit">
                <label class="label">Name</label>
                <div class="input">
                    <label class="icon-right">
                        <i class="fa fa-anchor"></i>
                    </label>
                    <input name="name" type="text" class="form-control" data-validation="required" data-name="Name" value="{{$instructor->name}}"  >
                    <span class="error_message"></span>
                </div>
            </div>
            <div class="unit">
                <label class="label">Email</label>
                <div class="input">
                    <label class="icon-right">
                        <i class="fa fa-envelope"></i>
                    </label>
                    <input name="email" type="text" data-validation="required,email" data-name="Email"  class="form-control" value="{{$instructor->email}}"  >
                    <span class="error_message"></span>
                </div>
            </div>
            <div class="unit">
                <label class="label">Phone</label>
                <div class="input">
                    <label class="icon-right">
                        <i class="fa fa-phone"></i>
                    </label>
                    <input name="phone" type="text" data-validation="required,number" data-name="Phone" class="form-control" value="{{$instructor->phone}}"  >
                    <span class="error_message"></span>
                </div>
            </div>
            <div class="unit">
                <label class="label">Nationality</label>
                <div class="input">
                    <label class="icon-right">
                        <i class="fa fa-flag"></i>
                    </label>
                    <select name="nationality">
  <option value="">Nationality</option>
  @foreach($nationalitites as $nationality)
  	<option value="{{$nationality}}" <?php if($instructor->nationality == $nationality){?> selected="selected"<?php ;}?>>{{$nationality}}</option>
  @endforeach
</select>
                    <span class="error_message"></span>
                </div>
            </div>
            <div class="unit">
                <label class="label">Speciality</label>
                <div class="input">
                    <label class="icon-right">
                        <i class="fa fa-user"></i>
                    </label>
                    <select name="speciality" data-validation="" data-name="speciality" class="form-control" >
                        <option value="">Select Type</option>
                        
                            @foreach($specs as $spec)
                            <option  value="{{$spec->speciality}}" <?php if( $instructor->speciality == $spec->speciality){?>selected<?php ;}?>>{{$spec->speciality}}</option>
                            @endforeach
                            <option value="New Speciality">New Speciality</option>
                            
                    </select>
                    <input name="new_spec" type="text" data-validation="" data-name="New type" class="form-control new_spec" value=""  style="display:none;"  >
                    <span class="error_message"></span>
                </div>
            </div>
            <div class="unit">
                <label class="label">Living Country</label>
                <div class="input">
                    <label class="icon-right">
                        <i class="fa fa-flag"></i>
                    </label>
                    <select name="living_country">
  <option value="">Select Country</option>
  @foreach($nationalitites as $nationality)
  	<option value="{{$nationality}}" <?php if($instructor->living_country == $nationality){?> selected="selected"<?php ;}?>>{{$nationality}}</option>
  @endforeach
</select>
                    <span class="error_message"></span>
                </div>
            </div>
            <div class="unit">
                <label class="label">Fax</label>
                <div class="input">
                    <label class="icon-right">
                        <i class="fa fa-phone"></i>
                    </label>
                    <input name="fax" type="text" data-validation="number" data-name="Fax" class="form-control" value="{{$instructor->fax}}"  >
                    <span class="error_message"></span>
                </div>
            </div>
            <div class="unit">
                <label class="label">Date of birth</label>
                <div class="input">
                    <label class="icon-right">
                        <i class="fa fa-calendar"></i>
                    </label>
                    <input name="date_of_birth" type="text" data-validation="" data-name="Date of birth" class="form-control datePicker" value="<?php if($instructor->date_of_birth!= ""){echo date("m/d/Y",strtotime($instructor->date_of_birth)) ;}?>"  >
                    <span class="error_message"></span>
                </div>
            </div>
            <div class="unit">
                <label class="label">Joining Date</label>
                <div class="input">
                    <label class="icon-right">
                        <i class="fa fa-calendar"></i>
                    </label>
                    <input name="joining_date" type="text" data-validation="required" data-name="Joining Date" class="form-control datePicker" value="<?php if($instructor->joining_date!= ""){echo date("m/d/Y",strtotime($instructor->joining_date)) ;}?>"  >
                    <span class="error_message"></span>
                </div>
            </div>
            <div class="divider-text gap-top-20 gap-bottom-45">
                <span>Documents</span>
            </div>
            <div id="files">
            @foreach($documents as $document)
            <div class="portlet box blue-hoki file" data-id="{{$document->id}}">
                                <div class="portlet-title">
                                    <div class="caption">
                                    {{$document->document_name}}
                                         </div>
                                    <div class="actions">
                                        
                                        <a href="javascript:;" class="btn btn-default btn-sm">
                                            <i class="fa fa-remove"></i>  </a>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                <div class="j-row">
                                <div class="span4 unit">
                <label class="label">Name</label>
                                    <div class="input">
                    <label class="icon-right">
                        <i class="fa fa-file"></i>
                    </label>
                    <input name="document_name[]" type="text" class="form-control" data-validation="required" data-name="Name" value="{{$document->document_name}}" placeholder="Name"  >
                    <span class="error_message"></span>
                </div>
                </div>
                <div class="span4 unit">
                <label class="label">File</label>
                                    <div class="input">
                    
                    @if(isset($document->getFile->file))<a style="margin-bottom:20px;" target="_blank" href="/uploads/{{$document->getFile->hash}}/{{$document->getFile->file}}">{{$document->getFile->file}}</a>@endif
                    <div class="row">&nbsp;</div>
                    <div id="filePicker_{{$document->id}}" class="filepicker"></div>
                    <span class="error_message"></span>
                </div>
                </div>
                                
                				
                                
                                <div class="span4 unit">
                <label class="label">Expires at</label>
                                    <div class="input">
                    <label class="icon-right">
                        <i class="fa fa-calendar"></i>
                    </label>
                    <input name="expire_date[]" type="text" class="form-control datePicker" autocomplete="off" data-validation="" data-name="Expire date" value="<?php if($document->expire_date != "0000-00-00"){echo date("m/d/Y",strtotime($document->expire_date));}?>" placeholder="Expiration Date"  >
                    <input name="document_id[]" type="hidden" class="form-control document_id"  data-validation="" data-name="Expire date" value="{{$document->id}}"   >
                    <span class="error_message"></span>
                </div>
                </div>
                </div>
                				
                                </div>
                            </div>
            @endforeach
            </div>
            <button class="btn btn-info add_new_file pull-left" type="button" >Add new</button>


            <div class="hide unit">
                <label class="label">password</label>
                <div class="input">
                    <label class="icon-right">
                        <i class="fa fa-lock"></i>
                    </label>
                    <input name="password" data-validation="" data-name="password" type="password" class="form-control" value="{{$instructor->dec_password}}"  >
                    <span class="error_message"></span>
                </div>
            </div>
            <div class="hide unit">
                <label class="label">Retype Password</label>
                <div class="input">
                    <label class="icon-right">
                        <i class="fa fa-lock"></i>
                    </label>
                    <input name="password2" data-validation="" data-name="password" type="password" class="form-control" value="{{$instructor->dec_password}}" >
                    <span class="error_message"></span>
                </div>
            </div>

            
            
            
            
            
            
            <input type="hidden" name="id" id="id" value="{{$instructor->id}}" class="user_id"  data-validation="" data-name="id"  />
    </div>
    <div id="response"></div>
    <div class="footer">
        <button type="submit" class="btn btn-lg color" ><i class="fa fa-edit"></i> Save</button>

    </div>
    <input type="reset" class="hide resetForm" >

</form>
<div class="hide">
<div class="portlet box blue-hoki clonable file">
                                <div class="portlet-title">
                                    <div class="caption">
                                         </div>
                                    <div class="actions">
                                        
                                        <a href="javascript:;" class="btn btn-default btn-sm">
                                            <i class="fa fa-remove"></i>  </a>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                <div class="j-row">
                                <div class="span4 unit">
                <label class="label">Name</label>
                                    <div class="input">
                    <label class="icon-right">
                        <i class="fa fa-file"></i>
                    </label>
                    <input name="document_name[]" type="text" class="form-control" data-validation="required" data-name="Name" value="" placeholder="Name"  >
                    <span class="error_message"></span>
                </div>
                </div>
                <div class="span4 unit">
                <label class="label">File</label>
                                    <div class="input">
                    <label class="icon-right">
                        <i class="fa fa-file"></i>
                    </label>
                    <div class="filepicker"></div>
                    <span class="error_message"></span>
                </div>
                </div>
                                
                				
                                
                                <div class="span4 unit">
                <label class="label">Expires at</label>
                                    <div class="input">
                    <label class="icon-right">
                        <i class="fa fa-calendar"></i>
                    </label>
                    <input name="expire_date[]" type="text" class="form-control datePicker" autocomplete="off" data-validation="" data-name="Expire date" value="" placeholder="Expiration Date"  >
                    <input name="document_id[]" type="hidden" class="form-control document_id"  data-validation="" data-name="Expire date" value=""   >
                    <span class="error_message"></span>
                </div>
                </div>
                </div>
                				
                                </div>
                            </div>
                            </div>


