<div class="portlet light">
									<div class="portlet-title tabbable-line">
										<div class="caption caption-md">
											<i class="icon-globe theme-font hide"></i>
											<span class="caption-subject font-blue-madison bold uppercase">Instructors</span>
										</div>
										<ul class="nav nav-tabs">
											<li class="active">
												<a href="#tab_1_1" data-toggle="tab">
												Instructors </a>
											</li>
											<li>
												<a href="#tab_1_2" data-toggle="tab">
												Expiring Documents </a>
											</li>
										</ul>
									</div>
									<div class="portlet-body">
										<!--BEGIN TABS-->
										<div class="tab-content">
											<div class="tab-pane active" id="tab_1_1">
											  <div class="table-toolbar">
            <div class="row">
                <div class="col-md-6">
                    <div class="btn-group">
                    <a href="{{route('admin.instructors.init')}}" class="pjax-link" >
                        <button class="btn color" id="add_new" >
                            Add New <i class="fa fa-plus"></i>
                        </button>      
                      </a>                  
                    </div>
                </div>
                <div class="col-md-6"></div>
            </div>
        </div>
        <table class="table table-striped table-bordered table-hover table-dt" id="table-dt" >
            <thead>
                <tr class="tr-head">
                  <th valign="middle">
                      Name
                  </th>
                  <th valign="middle">Phone</th>
                  <th valign="middle">Email</th>
                    <th valign="middle">
                        Created at
                    </th>
                    <th valign="middle">
                        Action
                    </th>
                </tr>
            </thead>
            <tbody>
                @foreach($instructors as $instructor)
                <tr class="odd gradeX" id="data-row-{{$instructor->id}}">
                  <td valign="middle">
                      {{$instructor->name}}
                  </td>
                    <td valign="middle">{{$instructor->phone}}</td>
                  <td valign="middle">{{$instructor->email}}</td>
                    <td valign="middle">
                        {{$instructor->created_at}}
                    </td>
                    <td valign="middle">
 						@if(Auth::user()->role_id <= 1)
                        <a href="#" data-action="{{route('admin.instructors.delete',['id'=>$instructor->id])}}"  class="btn red delete_single" ><i class="fa fa-remove"></i> Delete</a> 
                        @endif
                        <a href="{{route('admin.instructors.edit',['id'=>$instructor->id])}}" class="btn green pjax-link" ><i class="fa fa-edit"></i> Edit</a> 
                        
                    </td>
                </tr>
                @endforeach

        </table>
											</div>
											<div class="tab-pane" id="tab_1_2">
												<table class="table table-striped table-bordered table-hover table-dt" id="table-dt" >
            <thead>
                <tr class="tr-head">
                  <th valign="middle">
                      Trainer
                  </th>
                  <th valign="middle">Document type</th>
                  <th valign="middle">File</th>
                    <th valign="middle">Expiration date</th>
                </tr>
            </thead>
            <tbody>
                @foreach($expiring_documents as $document)
                <tr class="odd gradeX" id="data-row-{{$document->id}}">
                  <td valign="middle">
                      {{$document->name}}
                    </td>
                    <td valign="middle">{{$document->document_name}}</td>
                  <td valign="middle"><a target="_blank" href="/uploads/{{$document->hash}}/{{$document->file}}">{{$document->file}}</a></td>
                    <td valign="middle">
                        <?php echo date("d M Y",strtotime($document->expire_date));?>
                    </td>
                    </tr>
                @endforeach

        </table>
											</div>
										</div>
										<!--END TABS-->
									</div>
								</div>
