<div class="portlet box grey-cascade">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-globe"></i>Test Details
        </div>
        <div class="tools">
            <a href="javascript:;" class="collapse">
            </a>
        </div>
    </div>
    <div class="portlet-body">
        <div class="table-toolbar">
            <div class="row">
                <div class="col-md-6">
                    <div class="btn-group">
                           <a href="{{route('admin.candidates.add')}}" class="pjax-link"><button class="btn green" >

                            Add New Candidate <i class="fa fa-plus"></i>
                        </button>
                        </a>                  
                    </div>
                </div>
                <div class="col-md-6"></div>
            </div>
        </div>
        <table class="table table-striped table-bordered table-hover table-dt" id="table-dt" >
            <thead>
                <tr class="tr-head">
                    <th valign="middle">Question</th>
                    <th valign="middle">Student Degree</th>
                    <th valign="middle">Max degree</th>
                    <th valign="middle">Start time</th>
                    <th valign="middle">End time</th>
                </tr>
            </thead>
            <tbody>
                @foreach($answers as $answer)
                <tr class="odd gradeX" id="data-row-{{$answer->id}}" data-user_id = "{{$answer->id}}">
                    <td valign="middle">
                    {{$answer->getQuestion->content}}
                        
                    </td>
                    <td valign="middle">{{$answer->degree}}</td>
                    <td valign="middle">{{$answer->getQuestion->weight}}</td>
                    <td valign="middle">{{$answer->getEntry->start_time}}</td>
                    <td valign="middle">{{$answer->getEntry->end_time}}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>