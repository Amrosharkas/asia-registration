<form action="{{route('admin.places.save')}}" method="post" class="ajax_form j-forms larajsval form" id="main-form" novalidate>
    <div class="header">
        <p>
             @if(empty($place->name))<i class="fa fa-user"></i> New place @else <i class="fa fa-user"></i> {{$place->name}} @endif
        </p>
    </div>

    <div class="content">
        
            
           <div class="unit">
                <label class="label">Country</label>
                <label class="input select">
                    <select id="country" name="country" data-validation="required" data-name="City" class="form-control" >
                    </select>
                    <span class="error_message"></span>
                    <i></i>
                </label>
            </div>
            <div class="unit">
                <label class="label">City</label>
                <label class="input select">
                    <select id="city" name="city" data-validation="required" data-name="City" class="form-control" >
                    </select>
                    <span class="error_message"></span>
                    <i></i>
                </label>
            </div>
            <div class="unit">
                <label class="label">Name</label>
                <div class="input">
                    <label class="icon-right">
                        <i class="fa fa-anchor"></i>
                    </label>
                    <input name="name" type="text" class="form-control" data-validation="required" data-name="Name" value="{{$place->name}}"  >
                    <span class="error_message"></span>
                </div>
            </div>
            
            
            
            
            
            
            <input type="hidden" name="id" id="id" value="{{$place->id}}"  data-validation="" data-name="id"  />
    </div>
    <div id="response"></div>
    <div class="footer">
        <button type="submit" class="btn btn-lg color" ><i class="fa fa-edit"></i> Save</button>

    </div>
    <input type="reset" class="hide resetForm" >

</form>


