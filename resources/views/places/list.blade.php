<div class="portlet light">
    <div class="portlet-title">
        <div class="caption font-color">
            <i class="fa fa-user font-color"></i>places
        </div>
    </div>
    <div class="portlet-body">
      <div class="table-toolbar">
            <div class="row">
                <div class="col-md-6">
                    <div class="btn-group">
                    <a href="{{route('admin.places.init')}}" class="pjax-link" >
                        <button class="btn color" id="add_new" >
                            Add New <i class="fa fa-plus"></i>
                        </button>      
                      </a>                  
                    </div>
                </div>
                <div class="col-md-6"></div>
            </div>
        </div>
        <table class="table table-striped table-bordered table-hover table-dt" id="table-dt" >
            <thead>
                <tr class="tr-head">
                  <th valign="middle">
                      Name
                  </th>
                  <th valign="middle">Country</th>
                  <th valign="middle">City</th>
                    <th valign="middle">
                        Created at
                    </th>
                    <th valign="middle">
                        Action
                    </th>
                </tr>
            </thead>
            <tbody>
                @foreach($places as $place)
                <tr class="odd gradeX" id="data-row-{{$place->id}}">
                  <td valign="middle">
                      {{$place->name}}
                    </td>
                  <td valign="middle">{{$place->country}}</td>
                  <td valign="middle">{{$place->city}}</td>
                    <td valign="middle">
                        {{$place->created_at}}
                    </td>
                    <td valign="middle">
 						@if(Auth::user()->role_id <= 1)
                        <a href="#" data-action="{{route('admin.places.delete',['id'=>$place->id])}}"  class="btn red delete_single" ><i class="fa fa-remove"></i> Delete</a> 
                        @endif
                        <a href="{{route('admin.places.edit',['id'=>$place->id])}}" class="hide btn green pjax-link" ><i class="fa fa-edit"></i> Edit</a> 
                        
                    </td>
                </tr>
                @endforeach

        </table>
    </div>
</div>