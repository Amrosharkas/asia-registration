<!-- Amcharts -->
<script src="/js/amcharts/amcharts.js"></script>
<script src="/js/amcharts/serial.js"></script>
<script src="/js/amcharts/pie.js"></script>
<script src="/js/amcharts/themes/light.js"></script>
<style>
#chartdiv,#chartExpenses,#chartIncome {
  width: 100%;
  height: 500px;
}										
</style>
<?php $tt = array();?>

<div class="portlet box green">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-calendar"></i>Date Range
						  </div>
							<div class="tools">
								<a href="javascript:;" class="collapse" data-original-title="" title="">
								</a>
								
							</div>
						</div>
						<div class="portlet-body display-hide" style="display: block;">
							<p>
						    </p>
							<form id="form2" name="form2" method="get" action="">
                            <div class="row">
                            
                            	<div class="col-md-2">
                                	<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>From</td>
    <td><label for="textfield"></label>
      <input type="text" name="date1" id="textfield" class="datePicker" <?php if(isset($_GET['date1'])){?>value="<?php echo $_GET['date1'];?>"<?php ;}?> /></td>
  </tr>
</table>

                                </div>
                                <div class="col-md-2">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>To</td>
    <td><label for="textfield2"></label>
      <input type="text" name="date2" id="textfield2" class="datePicker" <?php if(isset($_GET['date2'])){?>value="<?php echo $_GET['date2'];?>"<?php ;}?> /></td>
  </tr>
</table>
                                	
                                </div>
                                <div class="col-md-2">
                                <button type="submit" class="btn btn-primary pull-left"> Search </button>
                                </div>
                            </div>
						  </form>
							<p></p>
						</div>
					</div>
 
<div class="portlet light">
    <div class="portlet-title">
        <div class="caption font-color">
            <i class="fa fa-line-chart font-color"></i>Income VS Expenses (AED)
        </div>
        <div class="btn-group pull-right">
                    
                      
                         
                     
                      
                                       
                    </div>
    </div>
    <div class="portlet-body">
    <div id="chartdiv"></div>
    </div>
</div> 
<div class="portlet light">
    <div class="portlet-title">
        <div class="caption font-color">
            <i class="fa fa-line-chart font-color"></i>Expenses Breakdown
        </div>
        <div class="btn-group pull-right">
                    
                      
                         
                     
                      
                                       
                    </div>
    </div>
    <div class="portlet-body">
    <div id="chartExpenses"></div>
    </div>
</div> 
<div class="portlet light">
    <div class="portlet-title">
        <div class="caption font-color">
            <i class="fa fa-line-chart font-color"></i>Income Breakdown
        </div>
        <div class="btn-group pull-right">
                    
                      
                         
                     
                      
                                       
                    </div>
    </div>
    <div class="portlet-body">
    <div id="chartIncome"></div>
    </div>
</div>                
<div class="portlet light">
    <div class="portlet-title">
        <div class="caption font-color">
            <i class="fa fa-money font-color"></i>Income
        </div>
        <div class="btn-group pull-right">
                    
                      
                         
                     
                      
                                       
                    </div>
    </div>
    <div class="portlet-body">
    <form class="ajax_form" method="post" action="/admin/schedules/recommend">
      <div class="table-toolbar" >
            
        </div>
        <table class="table table-striped table-bordered table-hover"  >
            <thead>
                <tr class="tr-head">
                  <th width="300" valign="middle">Type</th>
                <?php foreach($dates as $date){?>
                  <th valign="middle">
                      <?php 
					  
					  echo date("M Y",strtotime("15-".$date));?> 
                  </th>
                  
                  <?php ;}?>
                  <th valign="middle">Total</th>
                </tr>
            </thead>
            <tbody>
                
                <tr class="odd gradeX" id="data-row">
                  <td width="300" valign="middle">Consultations Income</td>
                <?php
				$total_consultations = 0 ;
				 foreach($dates as $date){
					$month = explode("-",$date)[0];
					$year = explode("-",$date)[1];
					?>
                  <td valign="middle">
                      <?php 
					  if(isset($consultations[$month][$year])){
					  	echo $consultations[$month][$year];
						$total_consultations = $total_consultations + $consultations[$month][$year];
					  }
					  ?>
                    </td>
                    <?php ;}?>
                  <td valign="middle">
				  <span class="success" >
				  <?php echo $total_consultations;?>
                  </span>
                  </td>
                    
                </tr>
                <tr class="odd gradeX">
                  <td width="300" valign="middle">Training Income</td>
                  <?php 
				  $total_training = 0 ;
				  foreach($dates as $date){
					$month = explode("-",$date)[0];
					$year = explode("-",$date)[1];
					?>
                  <td valign="middle">
                      <?php 
					  if(isset($events[$month][$year])){
					  	echo $events[$month][$year];
						$total_training = $total_training + $events[$month][$year];
					  }
					  ?>
                    </td>
                    <?php ;}?>
                  <td valign="middle">
				  <span class="success" >
				  <?php echo $total_training;?>
                  </span>
                  </td>
                    
                </tr>
                <tr class="odd gradeX">
                  <td width="300" valign="middle">Total</td>
                  <?php foreach($dates as $date){
					  $month = explode("-",$date)[0];
					$year = explode("-",$date)[1];
					  ?>
                  <td valign="middle"><span class="success" >
                  <?php 
					  if(isset($events[$month][$year]) ){
					  	$evs =  $events[$month][$year];
						 
					  }else{
						  $evs = 0;
					  }
					  if(isset($consultations[$month][$year]) ){
					  	$cns =  $consultations[$month][$year];
						 
					  }else{
						  $cns = 0;
					  }
					  $total_i[$month][$year] = $evs + $cns;
					  echo ($evs + $cns);
					  ?>
                      </span>
                  </td>
                  <?php ;}?>
                  <td valign="middle">
				  <span class="success" style="font-weight:bold;">
				  <?php $total1 = $total_consultations+ $total_training;
				  echo $total1;
				  ?>
                  </span></td>
                </tr>
                

        </table>
        </form>
    </div>
</div>

<div class="portlet light">
    <div class="portlet-title">
        <div class="caption font-color">
            <i class="fa fa-money font-color"></i>Expenses
        </div>
        <div class="btn-group pull-right">
                    
                      
                         
                     
                      
                                       
                    </div>
    </div>
    <div class="portlet-body">
    <form class="ajax_form" method="post" action="/admin/schedules/recommend">
      <div class="table-toolbar" >
            
        </div>
        <table class="table table-striped table-bordered table-hover"  >
            <thead>
                <tr class="tr-head">
                  <th width="300" valign="middle">Type</th>
                <?php foreach($dates as $date){?>
                  <th valign="middle">
                      <?php 
					  
					  echo date("M Y",strtotime("15-".$date));?> 
                  </th>
                  
                  <?php ;}?>
                  <th valign="middle">Total</th>
                </tr>
            </thead>
            <tbody>
                <?php 
				$total = 0;
				foreach($types as $type){?>
                <tr class="odd gradeX" id="data-row">
                  <td width="300" valign="middle"><?php echo $type;?></td>
                <?php
				$total_type = 0 ;
				 foreach($dates as $date){
					$month = explode("-",$date)[0];
					$year = explode("-",$date)[1];
					?>
                  <td valign="middle">
                      <?php 
					  if(isset($expenses[$month][$year][$type])){
					  	echo $expenses[$month][$year][$type];
						if(!isset($tt[$month][$year])){
							$tt[$month][$year] = 0;
						}
						$tt[$month][$year] += $expenses[$month][$year][$type];
						$total_type = $total_type + $expenses[$month][$year][$type];
						
					  }
					  ?>
                  </td>
                    <?php 
					
					;}?>
                  <td valign="middle">
				  <span class="danger" style="font-weight:bold;">
				  <?php echo $total_type;
				  $total = $total + $total_type;
				  ?>
                  </span>
                  </td>
                    
              </tr>
                <?php ;}?>
                
              <tr class="odd gradeX">
                  <td width="300" valign="middle">Total</td>
                  <?php foreach($dates as $date){
					  $month = explode("-",$date)[0];
					$year = explode("-",$date)[1];
					  ?>
                <td valign="middle">
                <span class="danger">
				<?php
				if(isset($tt[$month][$year])){
				echo $tt[$month][$year];
				}
				?>
                </span>
                </td>
                  <?php ;}?>
                  <td valign="middle">
				  <span class="danger" style="font-weight:bold;">
				  <?php //$total = $total_consultations+ $total_training;
				  echo $total;
				  ?>
                  </span></td>
                </tr>
                

        </table>
      </form>
    </div>
</div>
<div class="pull-right" style="font-size:35px; font-weight:bold;">Net Profit: <?php echo ($total1 - $total);?></div>

<script>
var chart = AmCharts.makeChart("chartdiv", {
    "theme": "light",
    "type": "serial",
    "dataProvider": [
	<?php foreach($dates as $date){
					  $month = explode("-",$date)[0];
					$year = explode("-",$date)[1];
					  ?>
	{
        "country": "<?php echo date("M Y",strtotime("15-".$date));?>",
        "Expenses": <?php
				if(isset($tt[$month][$year])){
				echo $tt[$month][$year];
				}else{
					echo 0;
				}
				?>
		,
        "Income": <?php echo $total_i[$month][$year];?>
    },
	<?php ;}?>
	{
        "country": "Total",
        "Expenses": <?php 
				  echo $total;
				  ?>
		,
        "Income": <?php 
				  echo $total1;
				  ?>
    }
	
	],
    "valueAxes": [{
        
        "unit": " AED",
        "position": "left",
        "title": "Income VS Expenses",
    }],
    "startDuration": 1,
    "graphs": [
	{
        "balloonText": "Income [[category]] : <b>[[value]]</b>",
        "fillAlphas": 0.9,
        "lineAlpha": 0.2,
        "title": "Income",
        "type": "column",
        "valueField": "Income",
		"fillColors":"#79a121",
		"lineColor" : "#79a121"
    },
	{
        "balloonText": "Expenses [[category]] : <b>[[value]]</b>",
        "fillAlphas": 0.9,
        "lineAlpha": 1,
        "title": "Expenses",
        "type": "column",
        "valueField": "Expenses",
		"fillColors" : "#e7505a",
		"lineColor" : "#e7505a"
    } 
	],
    "plotAreaFillAlphas": 0.1,
    "depth3D": 60,
    "angle": 30,
    "categoryField": "country",
    "categoryAxis": {
        "gridPosition": "start"
    },
    "export": {
    	"enabled": true
     }
});
jQuery('.chart-input').off().on('input change',function() {
	var property	= jQuery(this).data('property');
	var target		= chart;
	chart.startDuration = 0;

	if ( property == 'topRadius') {
		target = chart.graphs[0];
      	if ( this.value == 0 ) {
          this.value = undefined;
      	}
	}

	target[property] = this.value;
	chart.validateNow();
});
</script>

<script>
var chart = AmCharts.makeChart( "chartExpenses", {
  "type": "pie",
  "theme": "light",
  "dataProvider": [ 
  <?php foreach($expenses_breakdown as $type){?>
  {
    "country": "<?php echo $type->type;?>",
    "value": <?php echo $type->amount;?>
  },
  <?php ;}?>
  
   ],
  "valueField": "value",
  "titleField": "country",
  "outlineAlpha": 0.4,
  "depth3D": 15,
  "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
  "angle": 30,
  "export": {
    "enabled": true
  }
} );
</script>
<script>
var chart = AmCharts.makeChart( "chartIncome", {
  "type": "pie",
  "theme": "light",
  "dataProvider": [ 
  <?php foreach($income_breakdown as $type){?>
  {
    "country": "<?php echo $type->type;?>",
    "value": <?php 
	if($type->amount){
	echo $type->amount;
	}else{
		echo 0;	
	}
		?>
  },
  <?php ;}?>
  
   ],
  "valueField": "value",
  "titleField": "country",
  "outlineAlpha": 0.4,
  "depth3D": 15,
  "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
  "angle": 30,
  "export": {
    "enabled": true
  }
} );
</script>