<div class="row">
				<div class="col-md-12">
					<!-- BEGIN PROFILE SIDEBAR -->
					<div class="profile-sidebar">
						<!-- PORTLET MAIN -->
					  <div class="portlet light profile-sidebar-portlet">
							<!-- SIDEBAR USERPIC -->
							<div class="profile-userpic">
								@if($client->gender == "Male")
                                	<img src="/images/male.png" class="img-responsive" alt="">
                                @else
                                	<img src="/images/female.png" class="img-responsive" alt="">
                                @endif
							</div>
							<!-- END SIDEBAR USERPIC -->
							<!-- SIDEBAR USER TITLE -->
					    <div class="profile-usertitle">
								<div class="profile-usertitle-name">
									 {{$client->name}} 
								</div>
								<div class="profile-usertitle-job">
									 {{$client->phone}} 
								</div>
                                <div class="profile-usertitle-job" style="text-align:center;">
									 <img src="/images/coins.png" style="margin:0 auto;" width="30" class="img-responsive" alt="">
								</div>
                                <div class="profile-usertitle-job">
									 {{$client->points_balance}} 
								</div>
                                <div class="row">&nbsp;</div>
						  </div>
						  <!-- END SIDEBAR USER TITLE -->
						  <!-- SIDEBAR BUTTONS --><!-- END SIDEBAR BUTTONS -->
						  <!-- SIDEBAR MENU --><!-- END MENU -->
						</div>
						<!-- END PORTLET MAIN -->
						<!-- PORTLET MAIN -->
						<div class="portlet light">
							
							<div>
								<h4 class="profile-desc-title">Details</h4>
								
								<div class="margin-top-20 profile-desc-link">
									<i class="fa fa-envelope-o"></i>
									<a href="mailto:{{$client->email}}">{{$client->email}}</a>
								</div>
								<div class="margin-top-20 profile-desc-link">
									<i class="fa fa-phone"></i>
									<span href="#">{{$client->phone}}</span>
								</div>
                                @if($client->date_of_birth != "")
								<div class="margin-top-20 profile-desc-link">
                                <span  class="tooltips" data-container="body" data-placement="top" data-original-title="Date of Birth" >
									<i class="fa fa-calendar"></i>
									<?php echo date("d M Y",strtotime($client->date_of_birth));?></span>
								</div>
                                @endif
                                @if($client->joining_date != "")
								<div class="margin-top-20 profile-desc-link">
                                <span  class="tooltips" data-container="body" data-placement="top" data-original-title="Joining Date" >
									<i class="fa fa-calendar"></i>
                                    
									<?php echo date("d M Y",strtotime($client->joining_date));?></span>
								</div>
                                @endif
							</div>
						</div>
                        <div class="portlet light">
									<div class="portlet-title tabbable-line">
										<div class="caption caption-md">
											<i class="icon-globe theme-font hide"></i>
											<span class="caption-subject font-blue-madison bold uppercase">Family</span>
										</div>
										<ul class="nav nav-tabs">
											<li class="active">
												<a href="#tab_1_1" data-toggle="tab">
												Parents </a>
											</li>
											<li>
												<a href="#tab_1_2" data-toggle="tab">
												Children </a>
											</li>
										</ul>
									</div>
									<div class="portlet-body">
										<!--BEGIN TABS-->
										<div class="tab-content">
											<div class="tab-pane active" id="tab_1_1">
												<div  style="position: relative; overflow: hidden; width: auto; height: auto;"><div    >
													<ul class="feeds">
                                                    <?php if(isset($client->getFather)){?>
														<li>
															<div class="col1">
																<div class="cont">
																	<div class="cont-col1">
																		<div class="label label-sm label-male">
																			<i class="fa fa-user"></i>
																		</div>
																	</div>
																	<div class="cont-col2">
																		<div class="desc">
																			 <?php if(isset($client->getFather)){?><a href="/admin/clients/{{$client->getFather->id}}/profile">{{$client->getFather->name}}</a><?php ;}?> 
																		</div>
																	</div>
																</div>
															</div>
															<div class="col2">
																
															</div>
														</li>
                                                        <?php ;}?>
                                                        <?php if(isset($client->getMother)){?>
                                                        <li>
															<div class="col1">
																<div class="cont">
																	<div class="cont-col1">
																		<div class="label label-sm label-female">
																			<i class="fa fa-user"></i>
																		</div>
																	</div>
																	<div class="cont-col2">
																		<div class="desc">
																			 <?php if(isset($client->getMother)){?>{<a href="/admin/clients/{{$client->getMother->id}}/profile">{$client->getMother->name}}</a><?php ;}?> 
																		</div>
																	</div>
																</div>
															</div>
															<div class="col2">
																
															</div>
														</li>
                                                        <?php ;}?>
													</ul>
												</div><div  style="background: rgb(215, 220, 226); width: 7px; position: absolute; top: 0px; opacity: 0.4; display: block; border-radius: 7px; z-index: 99; right: 1px; height: 169.256px;"></div><div  style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(234, 234, 234); opacity: 0.2; z-index: 90; right: 1px;"></div></div>
											</div>
											<div class="tab-pane" id="tab_1_2">
												<div  style="position: relative;  width: auto; height: auto;"><div style="height: auto;  width: auto;" >
													<ul class="feeds">
                                                    @foreach($children as $child)
														<li>
															<a href="/admin/clients/{{$child->id}}/profile">
															<div class="col1">
																<div class="cont">
																	<div class="cont-col1">
																		<div class="label label-sm <?php if($child->gender == "Male"){?>label-male<?php ;}else{?>label-female<?php ;}?>">
																			<i class="fa fa-user"></i>
																		</div>
																	</div>
																	<div class="cont-col2">
																		<div class="desc">
																			 {{$child->name}}
																		</div>
																	</div>
																</div>
															</div>
															<div class="col2">
																
															</div>
															</a>
														</li>
                                                        @endforeach
													</ul>
												</div><div class="slimScrollBar" style="background: rgb(215, 220, 226); width: 7px; position: absolute; top: 0px; opacity: 0.4; display: block; border-radius: 7px; z-index: 99; right: 1px;"></div><div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(234, 234, 234); opacity: 0.2; z-index: 90; right: 1px;"></div></div>
											</div>
										</div>
										<!--END TABS-->
									</div>
								</div>
						<!-- END PORTLET MAIN -->
					</div>
					<!-- END BEGIN PROFILE SIDEBAR -->
					<!-- BEGIN PROFILE CONTENT -->
					<div class="profile-content">
						<div class="row">
							
							<div class="col-md-12">
								<!-- BEGIN PORTLET -->
								<div class="portlet light">
									<div class="portlet-title tabbable-line">
										<div class="caption caption-md">
											<i class="icon-globe theme-font hide"></i>
											<span class="caption-subject font-blue-madison bold uppercase">Activities</span>
										</div>
										<ul class="nav nav-tabs">
											<li class="active">
												<a href="#tab_1_12" data-toggle="tab">
												Consultations </a>
											</li>
											<li>
												<a href="#tab_1_22" data-toggle="tab">
												Events </a>
											</li>
										</ul>
									</div>
									<div class="portlet-body">
										<!--BEGIN TABS-->
										<div class="tab-content">
										  <div class="tab-pane active" id="tab_1_12">
												<div  style="position: relative; width:auto;  ">
													<table class="table table-striped table-bordered table-hover table-dt" id="table-dt" >
            <thead>
                <tr class="tr-head">
                  <th valign="middle">Date &amp; Time</th>
                  <th valign="middle">Comment</th>
                  <th valign="middle">Voice Note</th>
                  <th valign="middle">Remaining Payment</th>
                  </tr>
            </thead>
            <tbody>
                @foreach($consultations as $consultation)
                <tr class="odd gradeX" id="data-row-{{$consultation->id}}">
                  <td valign="middle"><?php echo date("l d M h:i a",strtotime($consultation->created_at));?></td>
                  <td align="center" valign="middle">{{$consultation->comment}}</td>
                  <td align="center" valign="middle"><?php if($consultation->audio_file!= ""){?><audio src="/uploads/{{$consultation->audio_file}}" controls=""></audio><?php ;}?></td>
                  <td align="center" valign="middle"><span class="danger"><?php echo ($consultation->price - $consultation->payment);?></span></td>
                  </tr>
                @endforeach

        </table>
											</div>
											</div>
										  <div class="tab-pane" id="tab_1_22">
												<div  style="position: relative;   height: auto;">
                                                <table class="table table-striped table-bordered table-hover table-dt" id="table-dt" >
            <thead>
                <tr class="tr-head">
                  <th valign="middle">
                    Event
                  </th>
                  <th valign="middle">
                    Interest
                  </th>
                  <th align="center" valign="middle">Remaining Payment</th>
                  </tr>
            </thead>
            <tbody>
                @foreach($clients as $client)
                <tr class="odd gradeX" id="data-row-{{$client->id}}" data-id = "{{$client->id}}">
                  <td valign="middle">
                    {{$client->name}} (<?php echo date("d M Y",strtotime($client->start_date));?>)
                    
                   ({{$client->place}})</td>
                  <td valign="middle">
                    
                    {{$client->interest_status}}</td>
                  <td align="center" valign="middle" > <span class="danger"> <?php echo $client->price - $client->payment;?></span></td>
                  </tr>
                @endforeach

        </table>
													
											  </div>
											</div>
										</div>
										<!--END TABS-->
									</div>
								</div>
								<!-- END PORTLET -->
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<!-- BEGIN PORTLET --><!-- END PORTLET -->
							</div>
							<div class="col-md-6">
								<!-- BEGIN PORTLET --><!-- END PORTLET -->
							</div>
						</div>
					</div>
					<!-- END PROFILE CONTENT -->
				</div>
			</div>