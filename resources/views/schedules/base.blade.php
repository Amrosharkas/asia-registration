<?php 
	$i ="schedules" ; $j = "";
 ?>
@extends('admin.master')
@section('plugins_css')
<style>
.portlet.box.blue-hoki > .portlet-title {
    background-color: #79A121 !important;
}
.clonable{
	display:none;
}
</style>
@stop

@section('plugins_js')

@stop

@section('page_js')

<script type="text/javascript" src="{{asset('assets/admin/pages/scripts/schedule.js')}}"></script>

@endSection

@section('add_inits')

@stop

@section('title')
Events
@stop

@section('page_title')

@stop

@section('page_title_small')

@stop

@section('content')
@include($partialView)
@stop

