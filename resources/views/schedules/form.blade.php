<form action="{{route('admin.schedules.save')}}" method="post" class="ajax_form j-forms larajsval form" id="main-form" novalidate>
    <div class="header">
        <p>
             @if(empty($schedule->name))<i class="fa fa-user"></i> New schedule @else <i class="fa fa-user"></i> {{$schedule->name}} @endif
        </p>
    </div>

    <div class="content">
        
            
           
            <div class="unit">
                <label class="label">Course</label>
                <label class="input select">
                    <select name="course_id" data-validation="Required" data-name="Course" class="form-control select2" >
                        <option value="" >Select Course</option>
                        @foreach($courses as $course)
                            <option  value="{{$course->id}}" <?php if($course->id == $schedule->course_id){?>selected<?php ;}?>>{{$course->name}}</option>
                        @endforeach
                    </select>
                    <span class="error_message"></span>
                    <i></i>
                </label>
            </div>
            <div class="unit">
                <label class="label">Place</label>
                <label class="input select">
                    <select name="place_id" data-validation="Required" data-name="Place" class="form-control select2" >
                    <option value="">Select Place</option>
                        <optgroup label="Places">
                        
                        @foreach($places as $place)
                            <option  value="{{$place->id}}" <?php  if ($schedule->place_id == $place->id) {?>selected<?php ;}?>>{{$place->name}}</option>
                            </optgroup>
                        @endforeach
                    </select>
                    <span class="error_message"></span>
                    <i></i>
                </label>
            </div>
            <div class="unit">
                <label class="label">Instructors</label>
                <label class="input select">
                    <select name="instructor_id[]" data-validation="Required" data-name="Instructor" class="form-control select2 select2-multiple" multiple>
                        <optgroup label="Instructors">
                        @foreach($instructors as $instructor)
                            <option  value="{{$instructor->name}}" <?php  if (strpos($schedule->instructor_id, $instructor->name) !== false) {?>selected<?php ;}?>>{{$instructor->name}}</option>
                            </optgroup>
                        @endforeach
                    </select>
                    <span class="error_message"></span>
                    <i></i>
                </label>
            </div>
            <div class="unit">
                <label class="label">Start Date</label>
                <div class="input">
                    <label class="icon-right">
                        <i class="fa fa-calendar"></i>
                    </label>
                    <input name="start_date" type="text" data-validation="" data-name="Date of birth" class="form-control datePicker" value="<?php 
					if($schedule->start_date !=""){
					echo date("m/d/Y",strtotime($schedule->start_date));
					}
					?>"  >
                    <span class="error_message"></span>
                </div>
            </div>
            <div class="unit">
                <label class="label">Price </label>
                <div class="input">
                    <label class="icon-right">
                        <i class="fa fa-money"></i>
                    </label>
                    <input name="price_cash" type="text" class="form-control" data-validation="required,number" data-name="Price (cash)" value="{{$schedule->price_cash}}"  >
                    <span class="error_message"></span>
                </div>
            </div>
            <div class="hide unit">
                <label class="label">Price (Coins)</label>
                <div class="input">
                    <label class="icon-right">
                        <i class="fa fa-money"></i>
                    </label>
                    <input name="price_coins" type="text" class="form-control" data-validation="number" data-name="Price (coins)" value="{{$schedule->price_coins}}"  >
                    <span class="error_message"></span>
                </div>
            </div>
            <div class="unit">
                <label class="label">Discounted Price </label>
                <div class="input">
                    <label class="icon-right">
                        <i class="fa fa-money"></i>
                    </label>
                    <input name="disc_price_cash" type="text" class="form-control" data-validation="required,number" data-name="Discounted Price (cash)" value="{{$schedule->disc_price_cash}}"  >
                    <span class="error_message"></span>
                </div>
            </div>
            <div class="hide unit">
                <label class="label">Discounted Price (Coins)</label>
                <div class="input">
                    <label class="icon-right">
                        <i class="fa fa-money"></i>
                    </label>
                    <input name="disc_price_coins" type="text" class="form-control" data-validation="number" data-name="Discounted Price (Coins)" value="{{$schedule->disc_price_coins}}"  >
                    <span class="error_message"></span>
                </div>
            </div>
            <div class="unit">
                <label class="label">Discounted Cutt off Time (In days)</label>
                <div class="input">
                    <label class="icon-right">
                        <i class="fa fa-clock-o"></i>
                    </label>
                    <input name="disc_cuttoff_time" type="text" class="form-control" data-validation="required,number" data-name="Discounted Time" value="{{$schedule->disc_cuttoff_time}}"  >
                    <span class="error_message"></span>
                </div>
            </div>
            <div class="divider-text gap-top-20 gap-bottom-45">
                <span>Cancelation Policy</span>
            </div>
            <div class="unit">
                <label class="label">100% Cutt off Time (In days)</label>
                <div class="input">
                    <label class="icon-right">
                        <i class="fa fa-clock-o"></i>
                    </label>
                    <input name="canc_100_cuttoff_time" type="text" class="form-control" data-validation="required,number" data-name="Time" value="{{$schedule->canc_100_cuttoff_time}}"  >
                    <span class="error_message"></span>
                </div>
            </div>
            <div class="unit">
                <label class="label">50% Cutt off Time (In days)</label>
                <div class="input">
                    <label class="icon-right">
                        <i class="fa fa-clock-o"></i>
                    </label>
                    <input name="canc_50_cuttoff_time" type="text" class="form-control" data-validation="required,number" data-name="Time" value="{{$schedule->canc_50_cuttoff_time}}"  >
                    <span class="error_message"></span>
                </div>
            </div>
            <div class="divider-text gap-top-20 gap-bottom-45">
                <span>Rewards</span>
            </div>
            
            <div class="unit">
                <label class="label">Points Gained (The points the client will gain after joining this schedule)</label>
                <div class="input">
                    <label class="icon-right">
                        <i class="fa fa-money"></i>
                    </label>
                    <input name="points_gained" type="text" class="form-control" data-validation="required,number" data-name="Points Gained" value="{{$schedule->points_gained}}"  >
                    <span class="error_message"></span>
                </div>
            </div>
            
            <div class="unit">
                <label class="label">Points Gained by the referral (The points the referral will gain after a client joins this schedule)</label>
                <div class="input">
                    <label class="icon-right">
                        <i class="fa fa-money"></i>
                    </label>
                    <input name="referral_points_gained" type="text" class="form-control" data-validation="required,number" data-name="Points Gained" value="{{$schedule->referral_points_gained}}"  >
                    <span class="error_message"></span>
                </div>
            </div>
            
            <div class="divider-text gap-top-20 gap-bottom-45">
                <span>Schedule details</span>
            </div>
            <div id="days">
            <?php  $i =1;?>
            @foreach($schedule_days as $day)
            <div class="portlet box blue-hoki day" data-id="{{$day->id}}">
                                <div class="portlet-title">
                                    <div class="caption">
                                    Day <?php echo $i;?>
                                         </div>
                                    <div class="actions">
                                        
                                        <a href="javascript:;" class="btn btn-default btn-sm">
                                            <i class="fa fa-remove"></i>  </a>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                <div class="j-row">
                                <div class="span4 unit">
                <label class="label">Day</label>
                                    <div class="input">
                    <label class="icon-right">
                        <i class="fa fa-clock-o"></i>
                    </label>
                    <input name="day[]" type="text" class="form-control datePicker" data-validation="required" data-name="Day" value="<?php echo date("Y-m-d",strtotime($day->from));?>" placeholder="Day"  >
                    <span class="error_message"></span>
                </div>
                </div>
                                <div class="span4 unit">
                <label class="label">From</label>
                                    <div class="input">
                    <label class="icon-right">
                        <i class="fa fa-clock-o"></i>
                    </label>
                    <input name="from[]" type="text" class="form-control timepicker timepicker-24" data-validation="required" data-name="Time" value="<?php echo date("H:i:s",strtotime($day->from));?>" placeholder="From"  >
                    <span class="error_message"></span>
                </div>
                </div>
                				
                                
                                <div class="span4 unit">
                <label class="label">To</label>
                                    <div class="input">
                    <label class="icon-right">
                        <i class="fa fa-clock-o"></i>
                    </label>
                    <input name="to[]" type="text" class="form-control timepicker timepicker-24" data-validation="required" data-name="Time" value="<?php echo date("H:i:s",strtotime($day->to));?>" placeholder="To"  >
                    <span class="error_message"></span>
                </div>
                </div>
                </div>
                				
                                </div>
                            </div>
                            <?php $i++;?>
            @endforeach
            
                            </div>
                            
                            <button class="btn btn-info add_new_day pull-left" type="button" >Add new</button>
            
            
            
            
            
            
            <input type="hidden" name="id" id="id" value="{{$schedule->id}}"  data-validation="" data-name="id"  />
    </div>
    <div id="response"></div>
    <div class="footer">
        <button type="submit" class="btn btn-lg color" ><i class="fa fa-edit"></i> Save</button>

    </div>
    <input type="reset" class="hide resetForm" >

</form>

<div class="portlet box blue-hoki clonable day">
                                <div class="portlet-title">
                                    <div class="caption">
                                         </div>
                                    <div class="actions">
                                        
                                        <a href="javascript:;" class="btn btn-default btn-sm">
                                            <i class="fa fa-remove"></i>  </a>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                <div class="j-row">
                                <div class="span4 unit">
                <label class="label">Day</label>
                                    <div class="input">
                    <label class="icon-right">
                        <i class="fa fa-clock-o"></i>
                    </label>
                    <input name="day[]" type="text" class="form-control datePicker" data-validation="required" data-name="Time" value="" placeholder="Day"  >
                    <span class="error_message"></span>
                </div>
                </div>
                                <div class="span4 unit">
                <label class="label">From</label>
                                    <div class="input">
                    <label class="icon-right">
                        <i class="fa fa-clock-o"></i>
                    </label>
                    <input name="from[]" type="text" class="form-control timepicker timepicker-24" data-validation="required" data-name="Time" value="Choose Time" placeholder="From"  >
                    <span class="error_message"></span>
                </div>
                </div>
                				
                                
                                <div class="span4 unit">
                <label class="label">To</label>
                                    <div class="input">
                    <label class="icon-right">
                        <i class="fa fa-clock-o"></i>
                    </label>
                    <input name="to[]" type="text" class="form-control timepicker timepicker-24" data-validation="required" data-name="Time" value="Choose Time" placeholder="To"  >
                    <span class="error_message"></span>
                </div>
                </div>
                </div>
                				
                                </div>
                            </div>


