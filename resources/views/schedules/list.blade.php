<div class="portlet light">
    <div class="portlet-title">
        <div class="caption font-color">
            <i class="fa fa-user font-color"></i>Events
        </div>
    </div>
    <div class="portlet-body">
      <div class="table-toolbar">
            <div class="row">
                <div class="col-md-6">
                    <div class="btn-group">
                    <a href="{{route('admin.schedules.init')}}" class="pjax-link" >
                        <button class="btn color" id="add_new" >
                            Add New <i class="fa fa-plus"></i>
                        </button>      
                      </a>                  
                    </div>
                </div>
                <div class="col-md-6"></div>
            </div>
        </div>
        <table class="table table-striped table-bordered table-hover table-dt" id="table-dt" >
            <thead>
                <tr class="tr-head">
                  <th valign="middle">
                      Course
                  </th>
                  <th valign="middle">Start Date</th>
                  <th valign="middle">Place</th>
                  <th valign="middle">
                      Created at
                  </th>
                  <th valign="middle">&nbsp;</th>
                </tr>
            </thead>
            <tbody>
                @foreach($schedules as $schedule)
                <tr class="odd gradeX" id="data-row-{{$schedule->id}}">
                  <td valign="middle">
                  <?php if(isset($schedule->getCourse->name)){?>
                      {{$schedule->getCourse->name}}
                      <?php ;}?>
                    </td>
                  <td valign="middle">
                  <?php echo date("l d M Y",strtotime($schedule->start_date));?>
                  </td>
                  <td valign="middle">
                  <?php if(isset($schedule->getPlace->name)){?>
                  {{$schedule->getPlace->name}}
                  <?php ;}?>
                  </td>
                  <td valign="middle">
                      {{$schedule->created_at}}
                    </td>
                  <td valign="middle">
                  <a href="{{route('admin.schedules.manage',['id'=>$schedule->id])}}" class="btn color pjax-link" ><i class="fa fa-gears"></i> Manage</a>
                  	<a href="{{route('admin.schedules.edit',['id'=>$schedule->id])}}" class="btn green pjax-link" ><i class="fa fa-edit"></i> Edit</a> 
                  </td>
                </tr>
                @endforeach

        </table>
    </div>
</div>