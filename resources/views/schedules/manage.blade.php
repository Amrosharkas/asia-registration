<style>
.portlet.box.green {
    border: 1px solid #79a121 !important;
}

</style>
<div class="portlet box green">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-search"></i>Search
						  </div>
							<div class="tools">
								<a href="javascript:;" class="collapse" data-original-title="" title="">
								</a>
								
							</div>
						</div>
						<div class="portlet-body display-hide" style="display: block;">
							<p>
						    </p>
							<form id="form2" name="form2" method="get" action="">
                            <div class="row">
                            
                            	<div class="col-md-2">
                                	<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>Interested</td>
    <td><select name="interest_status" >
                        <option <?php if($interest_status == ""){?> selected="selected" <?php ;}?> value="">All</option>
                          <option <?php if($interest_status == "Interested"){?> selected="selected" <?php ;}?> value="Interested">Interested</option>
                          <option <?php if($interest_status == "Rejected"){?> selected="selected" <?php ;}?> value="Rejected">Rejected</option>
                          <option <?php if($interest_status == "No Reply"){?> selected="selected" <?php ;}?> value="No Reply">No Reply</option>
                          <option <?php if($interest_status == "Pending"){?> selected="selected" <?php ;}?> value="Pending">Pending</option>
                          <option <?php if($interest_status == "Confirmed"){?> selected="selected" <?php ;}?> value="Confirmed">Confirmed</option>
                        </select></td>
  </tr>
</table>

                                </div>
                                <div class="col-md-2">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>Payment</td>
    <td><select name="payment_status" >
    <option <?php if($payment_status == ""){?> selected="selected" <?php ;}?> value="">All</option>
                        <option <?php if($payment_status == "No"){?> selected="selected" <?php ;}?> value="No">No Payment</option>
                          <option <?php if($payment_status == "Half"){?> selected="selected" <?php ;}?> value="Half">Half Payment</option>
                          <option <?php if($payment_status == "Full"){?> selected="selected" <?php ;}?> value="Full">Fully Paid</option>
                          
                        </select></td>
  </tr>
</table>
                                	
                                </div>
                                <div class="col-md-2">
                                <button type="submit" class="btn btn-primary pull-left"> Search </button>
                                </div>
                            </div>
						  </form>
							<p></p>
						</div>
					</div>

<div class="portlet light">
    <div class="portlet-title">
        <div class="caption font-color">
            <i class="fa fa-user font-color"></i>
            <?php if(isset($schedule->getCourse->name)){?>
            {{$schedule->getCourse->name}}
            <?php ;}?>
        </div>
        <div class="pull-right">
        @if($schedule->closed == 0)
        @can("super_user")
        	<a data-action="{{route('admin.schedules.end_transactions',['schedule_id'=>$schedule->id])}}" class="btn color confirm_action" >
            	<i class="fa fa-check"></i> Close Schedule Payments
            </a>
            @endcan
            @endif
            @if($schedule->closed == 1)
        @can("super_user")
        	<a data-action="{{route('admin.schedules.reopen_transactions',['schedule_id'=>$schedule->id])}}" class="btn color confirm_action" >
            	<i class="fa fa-check"></i> Re-open Schedule Payments
            </a>
            @endcan
            @endif
        </div>
    </div>
    <div class="portlet-body">
      <div class="hide table-toolbar">
            <div class="row">
                <div class="col-md-6">
                    <div class="btn-group">
                    <a href="{{route('admin.schedules.init')}}" class="pjax-link" >
                        <button class="btn color" id="add_new" >
                            Add New <i class="fa fa-plus"></i>
                        </button>      
                      </a>                  
                    </div>
                </div>
                <div class="col-md-6"></div>
            </div>
        </div>
        <table class="table table-striped table-bordered table-hover table-dt" id="table-dt" >
            <thead>
                <tr class="tr-head">
                  <th valign="middle">
                      Client
                  </th>
                  <th valign="middle">Email</th>
                  <th valign="middle">Phone</th>
                  
                  <th valign="middle">
                      Interest
                  </th>
                  <th valign="middle">Credit</th>
                  
                  <th align="center" valign="middle">Course Price</th>
                  <th align="center" valign="middle">Paid</th>
                  <th align="center" valign="middle">Remaining</th>
                  <th align="center" valign="middle">Add Payment</th>
                  <th valign="middle">
                      Action
                  </th>
                </tr>
            </thead>
            <tbody>
                @foreach($clients as $client)
                <tr class="odd gradeX" id="data-row-{{$client->id}}" data-id = "{{$client->id}}">
                  <td valign="middle">
                  {{$client->name}}
                     
                    </td>
                  <td valign="middle">{{$client->email}}</td>
                  <td valign="middle">{{$client->phone}}</td>
                  
                  <td valign="middle">
                      
                      <form id="form1" name="form1" method="post" action="">
                        <label for="select"></label>
                        <select name="in_status" class="interested_status">
                        <option <?php if($client->interest_status == ""){?> selected="selected" <?php ;}?> value="No Contact">No contact</option>
                          <option <?php if($client->interest_status == "Interested"){?> selected="selected" <?php ;}?> value="Interested">Interested</option>
                          <option <?php if($client->interest_status == "Rejected"){?> selected="selected" <?php ;}?> value="Rejected">Rejected</option>
                          <option <?php if($client->interest_status == "No Reply"){?> selected="selected" <?php ;}?> value="No Reply">No Reply</option>
                          <option <?php if($client->interest_status == "Pending"){?> selected="selected" <?php ;}?> value="Pending">Pending</option>
                          <option <?php if($client->interest_status == "Confirmed"){?> selected="selected" <?php ;}?> value="Confirmed">Confirmed</option>
                        </select>
                    </form></td>
                    <td valign="middle" ><span class="info">{{$client->points_balance}}</span></td>
                  
                  <td align="center" valign="middle">{{$client->price}}</td>
                  <td align="center" valign="middle" ><?php if($client->payment_percentage != ""){?><span class="success">{{$client->payment}} ({{$client->payment_percentage}}%) </span>
                  <a href="{{route('admin.transactions.payment_history',['user_id'=>$client->user_id,'stuff_id'=>$client->schedule_id,'stuff_identifier' => 'schedule_id'] )}}" class="btn blue popup" ><i class="fa fa-money"></i> View History</a>
                  <?php ;}?>
                  </td>
                  <td align="center" valign="middle" > <span class="danger"> <?php echo $client->price - $client->payment;?></span></td>
                  <td align="center" valign="middle">
                  <div class="payment_info"> 
                  @if($schedule->closed == 0)
                  <a href="#" class="add_payment btn green">Click to add +</a>
                  @endif
                  </div>
                  <form class="ajax_form" action="/admin/schedules/add_payment" style="display:none;" method="post">
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td><input name="schedule_id" placeholder="Amount" type="hidden" value="{{$client->schedule_id}}" />
                  <input  name="user_id" type="hidden" value="{{$client->user_id}}" />
                  <input style="width:75px;" name="amount" type="text" placeholder=" Amount"  /></td>
    <td>
    <select name="payment_type">
    	<option value="Cash">Cash</option>
        <option value="Coins">Coins</option>
    </select></td>
    <td>
    <input type="text" class="datePicker" name="date" style="width:90px;" placeholder=" Date" />
    </td>
    <td><button type="submit" class="btn btn-info" ><i class="fa fa-check" aria-hidden="true"></i></button>
    
    </td>
    <td>
    <button type="button" class="btn btn-danger cancel_payment" ><i class="fa fa-remove" aria-hidden="true"></i></button>
    </td>
  </tr>
</table>
                   
                  </form>
                  {{$client->payment_status}}</td>
                  <td valign="middle">
					  @can('super_user')
                      @if($schedule->closed == 0)
                      <a href="{{route('admin.schedules.cancel',['user_schedule_id'=>$client->user_schedule_id])}}" class="btn red pjax-link" ><i class="fa fa-remove"></i> Cancel</a> 
                      @endif
                      @endcan
                        
                    </td>
                </tr>
                @endforeach

        </table>
    </div>
</div>