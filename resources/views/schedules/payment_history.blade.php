<style>
.page-header,.page-footer-inner {
	display:none !important;
}
</style>
<div class="portlet light">
    <div class="portlet-title">
        <div class="caption font-color">
            <i class="fa fa-user font-color"></i>History
        </div>
    </div>
    <div class="portlet-body">
      <div class="table-toolbar">
            <div class="row">
                <div class="col-md-6">
                    <div class="btn-group">
                                  
                  </div>
                </div>
                <div class="col-md-6"></div>
            </div>
        </div>
        <table class="table table-striped table-bordered table-hover table-dt" id="table-dt" >
            <thead>
                <tr class="tr-head">
                  <th valign="middle">
                      Date
                  </th>
                  <th valign="middle">Amount</th>
                  <th valign="middle">Type</th>
                  <th valign="middle">Actions</th>
                </tr>
            </thead>
            <tbody>
                @foreach($transactions as $transaction)
                <tr class="odd gradeX" id="data-row-{{$transaction->id}}">
                  <td valign="middle">
                      <?php echo date("d M Y",strtotime($transaction->transaction_date));?>
                      </td>
                  <td valign="middle">
                  {{$transaction->amount}}
                  </td>
                  <td valign="middle">
                  
                  {{$transaction->type}}
                  
                  </td>
                  <td valign="middle">
                  <a href="#" data-action="{{route('admin.schedules.delete_transaction',['id'=>$transaction->id])}}"  class="btn red delete_single" ><i class="fa fa-remove"></i> Delete</a>
                  </td>
                </tr>
                @endforeach

        </table>
    </div>
</div>