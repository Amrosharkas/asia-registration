<?php 
	$i ="" ; $j = "";
 ?>
@extends('admin.master')
@section('plugins_css')
@stop

@section('plugins_js')

@stop

@section('page_js')
<script type="text/javascript" src="{{asset('assets/admin/pages/scripts/candidates.js')}}"></script>
@endSection

@section('add_inits')

@stop

@section('title')
Candidates
@stop

@section('page_title')
Candidates
@stop

@section('page_title_small')

@stop

@section('content')
@include($partialView)
@stop

