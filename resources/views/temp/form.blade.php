<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-gift"></i> New  </div>
        <div class="tools">
            <a href="javascript:;" class="collapse"> </a>
        </div>
    </div>
    <div class="portlet-body form">
        <!-- BEGIN FORM-->
        <form action="{{route('admin.temp.save')}}" method="post" class="ajax_form">
        <div  id="main-form" novalidate method="POST" class="larajsval form"> 
            <div class="form-body">
            <div class="mt-element-ribbon bg-grey-steel">
                                                <div class="ribbon ribbon-border-hor ribbon-clip ribbon-color-primary uppercase">
                                                    <div class="ribbon-sub ribbon-clip"></div> Main Info </div>
                                                <p class="ribbon-content">
                                                <div class="form-group">
                                                  <div class="form-group">
                    <label>Name</label>
                    <input name="name" class="form-control" data-validation="required" data-name="Name"  >
                    <span class="error_message"></span>
                </div>
                <div class="form-group">
                    <label>Email</label>
                    <input name="email" type="text" data-validation="required,email" data-name="Email"  class="form-control" >
                    <span class="error_message"></span>
                </div>
                <div class="form-group">
                    <label>mobile</label>
                    <input name="mobile" data-validation="required,number" data-name="Mobile" class="form-control" >
                    <span class="error_message"></span>
                </div>
                <div class="form-group">
                    <label>password</label>
                    <input name="password" data-validation="required" data-name="password" type="password" class="form-control" >
                    <span class="error_message"></span>
                </div>
                <div class="form-group">
                    <label>Retype field</label>
                    <input name="password2" data-validation="required,match.password" data-name="password" type="password" class="form-control" >
                    <span class="error_message"></span>
                </div>
                <div class="form-group">
                    <label>Date</label>
                    <input name="date" data-validation="required" data-name="Date" type="text" class="form-control datePicker" >
                    <span class="error_message"></span>
                </div>
                <div class="form-group">
                    <label>Date & Time</label>
                    <input name="dateTime" data-validation="required" data-name="Date & Time" type="text" class="form-control timepicker" >
                    <span class="error_message"></span>
                </div>
                <div class="form-group">
                    <label>Dropdown</label>
                    <select name="dropdown" data-validation="required" data-name="Dropdown" class="form-control">
                    	<option value="">Select Option</option>
                    	<option value="1">1</option>
                    </select>
                    <span class="error_message"></span>
                </div>
                
                
                
                
                
                </p>
                
                                            </div>
                                            
                                            </div>
            

            </div>
            <div class="form-actions">
                <div class="btn-set pull-right">
                    <button type="submit" class="btn btn-lg green"  >Save</button>
                </div>
            </div>
        </div>
        <input type="reset" class="hide resetForm" >
        </form>
        <!-- END FORM-->
    </div>
</div>
@include('quizzes.templates')