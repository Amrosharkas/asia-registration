<div class="portlet box grey-cascade">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-globe"></i>Candidates
        </div>
        <div class="tools">
            <a href="javascript:;" class="collapse">
            </a>
        </div>
    </div>
    <div class="portlet-body">
        <div class="table-toolbar">
            <div class="row">
                <div class="col-md-6">
                    <div class="btn-group">
                           <a href="{{route('admin.temp.add')}}" class="pjax-link"><button class="btn green" >

                            Add New Candidate <i class="fa fa-plus"></i>
                        </button>
                        </a>                  
                    </div>
                </div>
                <div class="col-md-6"></div>
            </div>
        </div>
        <table class="table table-striped table-bordered table-hover table-dt" id="table-dt" >
            <thead>
                <tr class="tr-head">
                    <th valign="middle">
                        Title
                    </th>
                    <th valign="middle">Email</th>
                    <th valign="middle">Mobile</th>
                    <?php if($user_phase == "testEvaluation" || $user_phase == "phoneInterview"){?> <th valign="middle">Test Degree</th> <?php ;}?>
                    <?php if($user_phase == "pilotTest"){?><th valign="middle">Pilot</th><?php ;}?>
                   
                    <?php if($user_status == "Rejected" || $user_status == "all" ){?>
                    <th valign="middle">Phase &amp; Grades</th>
                    <?php ;}?>
                    
                    <th valign="middle">
                        Action
                    </th>
                </tr>
            </thead>
            <tbody>
                @foreach($candidates as $candidate)
                <tr class="odd gradeX" id="data-row-{{$candidate->id}}" data-user_id = "{{$candidate->id}}">
                    <td valign="middle">
                        {{$candidate->name}}
                    </td>
                    <td valign="middle">{{$candidate->email}}</td>
                    <td valign="middle">{{$candidate->phone}}</td>
                    <?php if($candidate->user_phase == "testEvaluation" || $user_phase == "phoneInterview"){?><td valign="middle">{{$candidate->getGrades->quiz}}</td><?php ;}?>
                    <?php if($user_phase == "pilotTest"){?><td valign="middle"><?php if($candidate->pilot_ready == 1){?>Ready<?php ;}else{?>Not ready<?php ;}?></td><?php ;}?>
                    
                    <?php if($user_status == "Rejected" || $user_status == "all" ){?><td valign="middle">
                      <table width="100%" border="0" cellspacing="0" cellpadding="0" class="grades">
                        <tr>
                        <th align="center" scope="col">Phase</th>
                        <th align="center" scope="col">Status</th>
                          <th align="center" scope="col">Quiz</th>
                          <th align="center" scope="col">Fluency</th>
                          <th align="center" scope="col">Vocab</th>
                          <th align="center" scope="col">Grammar</th>
                          <th align="center" scope="col">Accent</th>
                          <th align="center" scope="col">Pilot</th>
                        </tr>
                        <tr>
                        <td align="center">{{$candidate->user_phase}}</td>
                        <td align="center">{{$candidate->user_status}}</td>
                          <td align="center">@if(isset($candidate->getGrades->quiz)){{$candidate->getGrades->quiz}}@endif</td>
                          <td align="center">@if(isset($candidate->getGrades->quiz)){{$candidate->getGrades->fluency}}@endif</td>
                          <td align="center">@if(isset($candidate->getGrades->quiz)){{$candidate->getGrades->vocab}}@endif</td>
                          <td align="center">@if(isset($candidate->getGrades->quiz)){{$candidate->getGrades->grammar}}@endif</td>
                          <td align="center">@if(isset($candidate->getGrades->quiz)){{$candidate->getGrades->accent}}@endif</td>
                          <td align="center">@if(isset($candidate->getGrades->quiz)){{$candidate->getGrades->pilot}}@endif</td>
                        </tr>
                      </table></td><?php ;}?>
                    
                    <td valign="middle">
                    @if($candidate->user_status =="Pending" && $candidate->user_phase !="testing")
                    <a href="{{route('admin.temp.pass',['id'=>$candidate->id])}}" data-phase = "{{$candidate->user_phase}}" class="btn green candidate-change-status" ><i class="fa fa-check"></i> Pass</a>
                    <a href="#" data-action="{{route('admin.temp.delete',['id'=>$candidate->id])}}"  class="btn red delete_single" ><i class="fa fa-remove"></i> delete</a> 
                    
                    @endif
                    <?php if($user_status == "all" ){?>
                   	  <a href="{{route('admin.temp.edit',['id'=>$candidate->id])}}"  class="btn green pjax-link" ><i class="fa fa-edit"></i> edit</a> 	
                    <?php ;}?>
                    <a href="{{route('admin.temp.delete',['id'=>$candidate->id])}}"  class="btn blue delete_single" ><i class="fa fa-edit"></i> test details</a> 	
                    
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>