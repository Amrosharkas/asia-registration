<style>
.page-header,.clearfix,.page-footer,.page-title{
	display:none !important;
}
.page-container{
	margin:0 !important;
	padding:0 !important;
}
</style>
<div class="portlet light">
    <div class="portlet-title">
        <div class="caption font-color">
            <i class="fa fa-user font-color"></i>History
        </div>
    </div>
    <div class="portlet-body">
      <div class="table-toolbar">
            <div class="row">
                <div class="col-md-6">
                    <div class="btn-group">
                                  
                  </div>
                </div>
                <div class="col-md-6"></div>
            </div>
        </div>
        <table class="table table-striped table-bordered table-hover table-dt" id="table-dt" >
            <thead>
                <tr class="tr-head">
                  <th valign="middle">
                      Date
                  </th>
                  <th valign="middle">Amount</th>
                  <th valign="middle">Type</th>
                  <th valign="middle">Actions</th>
                </tr>
            </thead>
            <tbody>
                @foreach($transactions as $transaction)
                <tr class="odd gradeX" id="data-row-{{$transaction->id}}">
                  <td valign="middle">
                      <?php echo date("d M Y",strtotime($transaction->transaction_date));?>
                      </td>
                  <td valign="middle">
                  {{$transaction->amount}}
                  </td>
                  <td valign="middle">
                  
                  {{$transaction->type}}
                  
                  </td>
                  <td valign="middle">
                  @if($transaction->editable == 1)
                  <a href="#" data-action="{{route('admin.transactions.delete',['id'=>$transaction->id])}}"  class="btn red delete_single" ><i class="fa fa-remove"></i> Delete</a>
                  @endif
                  </td>
                </tr>
                @endforeach

        </table>
    </div>
</div>