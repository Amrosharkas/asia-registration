<script>

function runUploader(selector,multiple,maximum_file_size,youtube_videos,encrypt_files,model,field,entity_id,model_id){
	


        
    

    var errorHandler = function(id, fileName, reason) {
            return qq.log("id: " + id + ", fileName: " + fileName + ", reason: " + reason);
        },
         validatingUploader;

    validatingUploader = new qq.FineUploader({
        element: document.getElementById(selector),
        multiple: multiple,
        request: {
            endpoint: "{{route('files.endpoint')}}",
			params:{
				"_token" : "{{ csrf_token() }}"
				}
        },
		deleteFile: {
            enabled: true,
            endpoint: "/files/endpoint",
            forceConfirm: true,
			params:{
				"_token" : "{{ csrf_token() }}"
				}
        },
        debug: false,
        validation: {
            sizeLimit: maximum_file_size,
            minSizeLimit: 0
        },
        text: {
            uploadButton: "Click Or Drop"
        },
        display: {
            fileSizeOnSubmit: true
        },
		
        chunking: {
            enabled: true,
            concurrent: {
                enabled: false
            },
            success: {
                endpoint: "{{route('files.endpoint')}}?done"
            }
        },
        resume: {
            enabled: true
        },
        retry: {
            enableAuto: true
        },
		scaling: {
			sendOriginal: false,
			sizes: [
				{name: "original", maxSize: 900}
				
			]
		},
        callbacks: {
            onError: errorHandler,
			onComplete: function (id, filename,responseJSON,xhr) {
				var uuid = this.getUuid(id);
				$.ajax({
					url: "{{route('files.save_file')}}",
					method: "post",
					data:{
						uuid:uuid,
						name:filename,
						youtube_videos : youtube_videos,
						encrypt_files: encrypt_files,
                        multiple: multiple,
                        model: model,
                        field: field,
                        entity_id:entity_id,
						model_id:model_id
					}
					
				})
                console.log(uuid);

            }
        }
    });
	
}
	
       
	
    </script>